<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TestScoreRequest extends FormRequest
{
    /**
     * Determine if the userLogin is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'studentId' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'testScore.required' => __('language.Null'),
            'studentId.required' => __('language.Null'),
            'testScore.*.numeric' => __('language.Null'),
        ];
    }
}
