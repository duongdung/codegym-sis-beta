<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CenterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'center_name'   =>  'required|unique:centers,name|min:3|max:100',
            'center_code'   =>  'required|unique:centers,center_code|min:3|max:50',
            'center_phone'  =>  'required|regex:/^(0)\\d{9,10}$/',
            'center_email'  =>  'required|email|unique:centers,email',
            'center_address'=>  'required',
        ];
    }
    public function messages()
    {
        return [
            'center_name.required'  =>  __('language.Center_name_request'),
            'center_name.unique'    =>  __('language.Center_name_request_name'),
            'center_name.min'       =>  __('language.Center_name_request_min'),
            'center_name.max'       =>  __('language.Center_name_request_max'),
            'center_code.required'  =>  __('language.Center_code_request'),
            'center_code.unique'    =>  __('language.Center_request_code'),
            'center_code.min'       =>  __('language.Center_code_request_min'),
            'center_code.max'       =>  __('language.Center_code_request_max'),
            'center_phone.required' =>  __('language.Center_phone_request'),
            'center_phone.regex'    =>  __('language.Center_request_phone'),
            'center_phone.numeric'  =>  __('language.Center_request_phone'),
            'center_email.email'    =>  __('language.Center_request_email'),
            'center_email.unique'   =>  __('language.Center_request_email_unique'),
            'center_address.required'        =>  __('language.text_required')
        ];

    }
}
