<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseRequest extends FormRequest
{
    /**
     * Determine if the userLogin is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'course_code'   =>'required|min:3|max:10|unique:courses,course_code',
            'course_name'   =>'required|min:3|max:100',
        ];

    }
    public function messages(){
        return [
            'course_code.required'   => __('language.course_code_required') ,
            'course_code.min'        => __('language.course_code_min') ,
            'course_code.max'        => __('language.course_code_max') ,
            'course_code.unique'     => __('language.course_code_unique') ,

            'course_name.required'   => __('language.course_name_required') ,
            'course_name.min'        => __('language.course_name_min') ,
            'course_name.max'        => __('language.course_name_max') ,

        ];
    }
}
