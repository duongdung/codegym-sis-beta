<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AttendanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_attedance'    => 'required|date|before:tomorrow',
            'attend'            => 'required',
        ];
    }
    public function messages()
    {
        return [
            'date_attedance.required'   => __('language.text_required'),
            'date_attedance.date'       => __('language.date_attedance_date'),
            'date_attedance.before'      => __('language.date_attedance_before'),
            'attend.required'           => __('language.attend_required'),
        ];
    }
}
