<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GroupScoreRequest extends FormRequest
{
    /**
     * Determine if the userLogin is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'exam_date'  =>  'date|before:tomorrow',
            'score'      =>  'regex:/^[0-9]{1,2}$/',
        ];
    }
    public function messages()
    {
        return [
            'exam_date.date'   => __('language.exam_date') ,
            'exam_date.before' => __('language.exam_date_before_current') ,
            'score.regex'      => __('language.Test_Score_Number') ,
        ];
    }
}
