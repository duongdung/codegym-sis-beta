<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'    =>'required|min:1|max:30',
            'last_name'     =>'required|min:1|max:30',
            'email'         =>'required|unique:users,email',
            'password'      =>  'required|min:6',
            'password_confim'  =>  'required|same:password',
            'roles'         =>  'required',

        ];
    }
    public function messages(){
        return [
            'first_name.required'   => __('language.full_name_required') ,
            'first_name.min'        => __('language.full_name_min') ,
            'first_name.max'        => __('language.full_name_max') ,

            'last_name.required'    => __('language.full_name_required') ,
            'last_name.min'         => __('language.full_name_min') ,
            'last_name.max'         => __('language.full_name_max') ,

            'email.required'            => __('language.email_required') ,
            'email.unique'              => __('language.email_unique') ,

            'password.required'			=>	__('language.text_required'),
            'password.min'				=>	__('language.password_min'),

            'password_confim.required'	=>	__('language.text_required'),
            'password_confim.same'	    =>	__('language.password_confim_confirmed'),

            'roles.required'	        =>	__('language.roles_required'),
        ];
    }
}
