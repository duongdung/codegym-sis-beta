<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pass_old'      =>  'required|min:6',
            'pass_new'      =>  'required|min:6',
            'pass_confim'   =>  'required|min:6|same:pass_new',
        ];
    }
    public function messages()
    {
        return [
            'pass_old.required'     =>__('language.text_required'),
            'pass_old.min'          =>__('language.password_min'),
            'pass_new.required'     =>__('language.text_required'),
            'pass_new.min'          =>__('language.password_min'),

            'pass_confim.required'  =>__('language.text_required'),
            'pass_confim.min'       =>__('language.password_min'),
            'pass_confim.same'      =>__('language.password_confim_confirmed'),


        ];
    }
}
