<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AttendanceDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_to'           => 'required|date|before_or_equal:tomorrow',
            'date_from'         =>  'required|date|after:date_to|before_or_equal:tomorrow',
        ];
    }
    public function messages()
    {
        return [
            'date_to.required'              =>  __('language.text_required'),
            'date_to.date'                  =>  __('language.date_attedance_date'),
            'date_to.before_or_equal'       =>  __('language.date_attdendancedetail_before'),
            'date_from.required'            =>  __('language.text_required'),
            'date_from.date'                =>  __('language.date_attedance_date'),
            'date_from.after'               =>  __('language.attendancedetail_after'),
            'date_from.before_or_equal'     =>  __('language.date_attdendancedetail_before'),
        ];
    }
}
