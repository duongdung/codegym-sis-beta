<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_id'    =>  'required|unique:students,student_id|min:3|max:9',
            'first_name'    =>  'required|min:1|max:50',
            'last_name'     =>  'required|min:1|max:50',
            'birthday'      =>  'nullable|before:today',
            'startdate_id'  =>  'nullable|after:birthday|before:today',
            'identify_card' =>  'nullable|unique:students,identify_card|regex:/^\\d{9,12}$/',
            'phone'         =>  'required|regex:/^(0)\\d{9,10}$/',
            'email'         =>  'required|unique:users,email',
            'image'         =>  'image|mimes:jpeg,bmp,png',

            'start_school'  =>  'nullable|before_or_equal:today',
            'expected_end_date'=>  'nullable|after:start_school',
            'password'         =>  'required|min:6',
            'password_confim'  =>  'required|same:password',
        ];
    }
    public function messages()
    {
        return [
            'student_id.required'   => __('language.student_id_required') ,
            'student_id.unique'     => __('language.student_id_unique') ,
            'student_id.min'        => __('language.student_id_min') ,
            'student_id.max'        => __('language.student_id_max') ,

            'first_name.required'   => __('language.full_name_required') ,
            'first_name.min'        => __('language.full_name_min') ,
            'first_name.max'        => __('language.full_name_max') ,

            'last_name.required'    => __('language.full_name_required') ,
            'last_name.min'         => __('language.full_name_min') ,
            'last_name.max'         => __('language.full_name_max') ,

            'birthday.date'         =>  __('language.birthday_date'),
            'birthday.before'       =>  __('language.birthday_before'),

            'startdate_id.after'    =>__('language.startdate_id_after'),
            'startdate_id.before'   =>__('language.startdate_id_before'),

            'identify_card.unique'      => __('language.identify_card_unique') ,
            'identify_card.regex'       => __('language.identify_card_numeric') ,

            'phone.required'            => __('language.phone_required') ,
            'phone.regex'               => __('language.phone_regex') ,

            'email.required'            => __('language.email_required') ,
            'email.unique'              => __('language.email_unique') ,

            'image.image'               => __('language.image'),
            'image.mimes'               => __('language.image'),

            'start_school.before_or_equal'     =>  __('language.start_school_before_or_equal'),

            'expected_end_date.after'   => __('language.enddate_group_date'),

            'password.required'			=>	__('language.text_required'),
            'password.min'				=>	__('language.password_min'),

            'password_confim.required'	=>	__('language.text_required'),
            'password_confim.same'	    =>	__('language.password_confim_confirmed'),
        ];
    }


}
