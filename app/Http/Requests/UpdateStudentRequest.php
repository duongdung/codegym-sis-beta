<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'    =>  'required|min:1|max:50',
            'last_name'     =>  'required|min:1|max:50',
            'birthday'      =>  'nullable|before:today',
            'startdate_id'  =>  'nullable|after:birthday|before:today',
            'start_school'  =>  'nullable|before_or_equal:today',
            'phone'         =>  'required|regex:/^(0)\\d{9,10}$/',
            'image'         =>  'image|mimes:jpeg,bmp,png',
            'expected_end_date'=>  'nullable|after:start_school',
        ];
    }
    public function messages()
    {
        return [

            'first_name.required'   => __('language.full_name_required') ,
            'first_name.min'        => __('language.full_name_min') ,
            'first_name.max'        => __('language.full_name_max') ,

            'last_name.required'    => __('language.full_name_required') ,
            'last_name.min'         => __('language.full_name_min') ,
            'last_name.max'         => __('language.full_name_max') ,

            'birthday.date'         =>  __('language.birthday_date'),
            'birthday.before'       =>  __('language.birthday_before'),

            'startdate_id.after'    =>__('language.startdate_id_after'),
            'startdate_id.before'   =>__('language.startdate_id_before'),

            'start_school.before_or_equal'     =>  __('language.start_school_before_or_equal'),

            'image.image'               => __('language.image'),
            'image.mimes'               => __('language.image'),

            'expected_end_date.after'   => __('language.enddate_group_date'),
        ];
    }
}
