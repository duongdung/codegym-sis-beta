<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudyProgramRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_study_program'        =>'required|min:3|max:100',
            'length_of_program'         =>'required|min:2|max:100',
        ];
    }
    public function messages()
    {
        return [
            'name_study_program.required'   => __('language.text_required'),
            'name_study_program.min'        => __('language.name_study_program_min'),
            'name_study_program.max'        => __('language.name_study_program_max'),
            'length_of_program.required'    => __('language.text_required'),
            'length_of_program.min'          => __('language.length_of_program_min'),
            'length_of_program.max'          => __('language.length_of_program_max'),
        ];
    }
}
