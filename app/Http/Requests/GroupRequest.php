<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'group_id'                  =>'required|unique:groups,group_id|min:3|max:50',
            'startdate_group'           =>'required|date',
            'expected_end_date_group'   =>'required|date|after:startdate_group',
        ];
    }
    public function messages()
    {
        return [
            'group_id.required'             => __('language.group_id_required'),
            'group_id.unique'               => __('language.group_id_name'),
            'group_id.min'                  => __('language.group_id_min'),
            'group_id.max'                  => __('language.group_id_max'),
            'expected_end_date_group.after' => __('language.enddate_group_date'),

        ];
    }
}
