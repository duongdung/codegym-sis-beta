<?php

namespace App\Http\Middleware;

use App\Http\Controllers\RoleConstants;
use App\Student;
use Closure;
use Illuminate\Support\Facades\Auth;

class StudentsLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ($user->hasRole(RoleConstants::ROLE_STUDENT)) {
            $student = Student::where('email', '=', $user->email)->first();
            return redirect()->route('student.details', [$student->id, $student->group_id]);
        }
        return $next($request);
    }
}
