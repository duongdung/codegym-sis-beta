<?php

namespace App\Http\Middleware;

use App\Center;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CenterId
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $user = Auth::user();
        if (!empty($user->center_id)) {
            Session::put('centerId', $user->center_id);
        } elseif (!Session::has('centerId')) {
            Session::put('centerId', Center::first()->id);
        }
        return $response;
    }
}
