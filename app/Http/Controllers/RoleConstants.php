<?php
/**
 * Created by PhpStorm.
 * User: luanp
 * Date: 19/09/2017
 * Time: 3:58 PM
 */

namespace App\Http\Controllers;


interface RoleConstants
{
     const ROLE_ADMIN     = 1;
     const ROLE_HQMANAGER = 2;
     const ROLE_HQAAO     = 3;
     const ROLE_CENTERMANAGER  = 4;
     const ROLE_AAO       = 5;
     const ROLE_COACH     = 6;
     const ROLE_TUTOR     = 7;
     const ROLE_STUDENT   = 8;

}