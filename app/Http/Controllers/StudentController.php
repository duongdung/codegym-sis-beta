<?php

namespace App\Http\Controllers;


use App\Criteria;
use App\Course;
use App\EvaluateStudent;
use App\Exam;
use App\Group;
use App\Http\Requests\ExamRequest;
use App\Http\Requests\LogRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Report;
use App\StudentLog;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use App\Http\Requests\StudentRequest;
use App\Student;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;


class StudentController extends Controller
{
    public function create()
    {
        if ($this->userCan('curd-student', Session::get('centerId'))) {
            $groups = Group::where('center_id', '=', Session::get('centerId'))->get();
            return view('pageAdmin.student.add', compact('groups'));
        }
        abort('403');
    }

    public function store(StudentRequest $request)
    {
        if ($this->userCan('curd-student', Session::get('centerId'))) {

            DB::beginTransaction();
            try {
                $student = new Student;
                $student->student_id = $request->student_id;
                $student->first_name = $request->first_name;
                $student->last_name = $request->last_name;
                if ($request->hasFile('image')) {
                    $avatar = $request->file('image');
                    $path = $avatar->store('images/avatars', 'public');
                    $student->image = $path;
                }
                if ($request->input('birthday')) {
                    $student->birthday = Carbon::createFromFormat('d-m-Y', $request->input('birthday'))->format('Y-m-d');
                }
                $student->identify_card = $request->identify_card;
                if ($request->input('startdate_id')) {
                    $student->startdate_id = Carbon::createFromFormat('d-m-Y', $request->input('startdate_id'))->format('Y-m-d');
                }
                $student->address_id = $request->address_id;
                $student->household = $request->household;
                $student->phone = $request->phone;
                $student->company = $request->company;
                $student->link_github = $request->link_github;
                $student->link_blog = $request->link_blog;
                $student->agilearn_id = $request->agilearn_id;
                $student->group_id = $request->group_id;
                if ($request->input('start_school')) {
                    $student->start_school = Carbon::createFromFormat('d-m-Y', $request->input('start_school'))->format('Y-m-d');
                }
                if ($request->input('expected_end_date')) {
                    $student->expected_end_date = Carbon::createFromFormat('d-m-Y', $request->input('expected_end_date'))->format('Y-m-d');
                }
                $student->email = $request->email;
                $student->password = bcrypt($request->password);
                $student->note = $request->note;
                $student->center_id = Session::get('centerId');
                $student->save();
                
                $user = new User();
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->email = $request->email;
                $user->password = bcrypt($request->password);
                $user->center_id = Session::get('centerId');
                $user->save();
                $user->assignRole(RoleConstants::ROLE_STUDENT);
                Session::flash('add-new-success', __('language.Student_Add_Success'));
            } catch (Exception $e) {
                DB::rollback();
                return $e->getMessage();
            }
            DB::commit();
            return redirect()->route('student.list');
        }
        abort('403');
    }

    public function showList()
    {
        if ($this->userCan('view-list-students', Session::get('centerId'))) {
            $students = Student::where('center_id', '=', Session::get('centerId'))->paginate(20);
            return view('pageAdmin.student.list', compact('students'));
        }
        abort('403');
    }

    public function showDetail($studentId, $groupId)
    {
        $group = Group::with('students')->find($groupId);
        if ($this->userCan('view-list-students', Session::get('centerId'))) {
            $student = Student::where('center_id', '=', Session::get('centerId'))->find($studentId);
            return view('pageAdmin.student.tab-detail', compact('student','group'));
        } elseif ($this->userCan('view-students-group', $group)) {
            $student = Student::where('center_id', '=', Session::get('centerId'))
                ->where('group_id', '=', $groupId)
                ->find($studentId);
            return view('pageAdmin.student.tab-detail', compact('student','group'));
        } elseif ($this->userCan('students', Session::get('centerId'))) {
            $student = Student::where('center_id', '=', Session::get('centerId'))->find($studentId);
            if ($student->email == Auth::user()->email) {
                return view('pageAdmin.student.tab-detail', compact('student','group'));
            }
        } else {
            abort('403');
        }

    }

    public function showGroup($id)
    {
        $student = Student::where('center_id', '=', Session::get('centerId'))
            ->with('group')
            ->find($id);
        if ($student) {
            if ($this->userCan('view-list-students', Session::get('centerId'))) {
                return view('pageAdmin.student.tab-group', compact('student'));
            } else {
                abort('403');
            }
        } else {
            abort('404');
        }
    }

    public function showLog($id)
    {
        $students = Student::where('center_id', '=', Session::get('centerId'))->find($id);
        if ($this->userCan('add-student-log', $students)) {
            $logs = StudentLog::where('student_id', $id)
                ->orderBy('created_at', 'desc')
                ->paginate(20);
            return view('pageAdmin.student.tab-log', compact(['students', 'logs']));
        } else {
            abort('403');
        }

    }

    public function showCapacity($studentId,$groupId)
    {
        $group = Group::where('center_id','=',Session::get('centerId'))->find($groupId);
        if ($group){
            if ($this->userCan('crud-capacity',$group)){
                $student = Student::with('group')
                    ->where('center_id', '=', Session::get('centerId'))
                    ->where('group_id','=',$groupId)
                    ->find($studentId);
                if ($student) {
                    $reports = Report::all();
                    $evaluates = EvaluateStudent::where('student_id', $studentId)->get();
                    return view('pageAdmin.student.tab-capacity', compact(['student', 'evaluates', 'reports','group']));
                } else {
                    abort('404');
                }
            }else{
                abort('403');
            }
        }
    }

    public function showTabScore(Request $request, $studentId)
    {
        $student = Student::where('center_id', '=', Session::get('centerId'))->find($studentId);
        if ($student) {
            if ($this->userCan('view-student-score', $student)
                || $this->userCan('students', Session::get('centerId'))) {
                $courses = Course::all();
                $selectCourseId = $request->input('selectCourse');
                $selectCourse = Course::find($selectCourseId);
                $exams = Exam::with('students')
                    ->where('course_id', '=', $selectCourseId)
                    ->whereHas('students', function ($query) use ($studentId) {
                        $query->where('students.id', '=', $studentId);
                    })->get();

                return view('pageAdmin.student.tab-score', compact(['student', 'courses', 'selectCourse', 'exams']));
            } else {
                abort('403');
            }
        } else {
            abort('404');
        }
    }

    public function delete($id)
    {
        $student = Student::where('center_id', '=', Session::get('centerId'))
            ->find($id);
        if ($student) {
            if ($this->userCan('curd-student', Session::get('centerId'))) {
                return view('pageAdmin.student.delete', compact('student'));
            }
            abort('403');
        } else {
            abort('404');
        }

    }

    public function destroy($id)
    {
        $student = Student::where('center_id', '=', Session::get('centerId'))
            ->find($id);
        if ($student) {
            if ($this->userCan('curd-student', Session::get('centerId'))) {
                $user = User::find($student->email);
                if ($user) {
                    $user->roles()->detach();
                    $user->delete();
                }
                $student->delete();
                Session::flash('success', __('language.Delete_Student_Success'));
                return redirect()->route('student.list');
            }
            http://localhost:8000/groups
            abort('403');
        } else {
            abort('404');
        }

    }

    public function edit($id)
    {
        $student = Student::where('center_id', '=', Session::get('centerId'))->find($id);
        if ($student) {
            if ($this->userCan('curd-student', Session::get('centerId'))) {
                $groups = Group::where('center_id', '=', Session::get('centerId'))->get();
                return view('pageAdmin.student.edit', compact('student', 'groups'));
            }
            abort('403');
        } else {
            abort('404');
        }

    }

    public function update(UpdateStudentRequest $request, $id)
    {
        $student = Student::where('center_id', '=', Session::get('centerId'))->find($id);
        if ($student) {
            if ($this->userCan('curd-student', Session::get('centerId'))) {

                // Validator student code
                $studentCode = $student->student_id;
                if ($request->student_id !== $studentCode) {
                    $this->validate($request, [
                        'student_id' => 'required|unique:students,student_id|min:3|max:9',
                    ], [
                        'student_id.required' => __('language.student_id_required'),
                        'student_id.unique' => __('language.student_id_unique'),
                        'student_id.min' => __('language.student_id_min'),
                        'student_id.max' => __('language.student_id_max'),
                    ]);
                    $student->student_id = $request->student_id;
                }

                // Validator indentify card
                $identifyCard = $student->identify_card;
                if ($request->identify_card !== $identifyCard) {
                    $this->validate($request, [
                        'identify_card' => 'nullable|unique:students,identify_card|regex:/^\\d{9,12}$/',
                    ], [
                        'identify_card.unique' => __('language.identify_card_unique'),
                        'identify_card.regex' => __('language.identify_card_numeric'),

                    ]);
                    $student->identify_card = $request->identify_card;
                }
                // Validator email
                $email = $student->email;
                if ($request->email !== $email) {
                    $this->validate($request, [
                        'email' => 'required|unique:users,email',
                    ], [
                        'email.required' => __('language.email_required'),
                        'email.unique' => __('language.email_unique'),
                    ]);
                    $student->email = $request->email;
                }

                $student->first_name = $request->first_name;
                $student->last_name = $request->last_name;

                if ($request->hasFile('image')) {
                    $imgStudent = $request->file('image');
                    Storage::delete('/public/' . $student->image);
                    $path = $imgStudent->store('images/avatars', 'public');
                    $student->image = $path;
                }
                if ($request->input('birthday')) {
                    $student->birthday = Carbon::createFromFormat('d-m-Y', $request->input('birthday'))->format('Y-m-d');
                }
                if ($request->input('startdate_id')) {
                    $student->startdate_id = Carbon::createFromFormat('d-m-Y', $request->input('startdate_id'))->format('Y-m-d');
                }
                $student->phone = $request->phone;
                $student->address_id = $request->address_id;
                $student->household = $request->household;
                $student->company = $request->company;
                $student->link_github = $request->link_github;
                $student->link_blog = $request->link_blog;
                $student->agilearn_id = $request->agilearn_id;
                $student->note = $request->note;
                if ($request->input('start_school')) {
                    $student->start_school = Carbon::createFromFormat('d-m-Y', $request->input('start_school'))->format('Y-m-d');
                }
                if ($request->input('expected_end_date')) {
                    $student->expected_end_date = Carbon::createFromFormat('d-m-Y', $request->input('expected_end_date'))->format('Y-m-d');
                }
                if ($request->input('actual_end_date')) {
                    $student->actual_end_date = Carbon::createFromFormat('d-m-Y', $request->input('actual_end_date'))->format('Y-m-d');
                }
                $student->group_id = $request->group_id;

                $student->save();
                Session::flash('success', __('language.Success'));
                return redirect()->route('student.details', [$student->id, $student->group_id]);
            }
            abort('403');
        } else {
            abort('404');
        }

    }

    public function searchStudent(Request $request)
    {
        $keyword = $request->searchStudent;
        if (empty($keyword)) {
            Session::flash('one-error', __('language.Null_Search'));
            return redirect()->route('student.list');
        } else {
            $students = $this->getStudentViaSearch($keyword);
            if ($students->count() > 0) {
                Session::flash('have-result', __('language.have_result_1') . $students->count() . __('language.have_result_2') . $keyword);
                return view('pageAdmin.student.list', compact('students'));
            } else {
                Session::flash('no-result', __('language.No_Search') . $keyword);
                return redirect()->route('student.list');
            }
        }
    }

    public function getStudentViaSearch($keyword)
    {
        $students = Student::where('center_id', '=', Session::get('centerId'))
            ->where(function ($query) use ($keyword) {
                $query->where('student_id', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('first_name', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('last_name', 'LIKE', '%' . $keyword . '%');
            })
            ->orderBy('last_name')
            ->paginate(20);
        return $students;
    }

    public function addLog(LogRequest $request, $student_id)
    {
        $student = Student::where('center_id', '=', Session::get('centerId'))->find($student_id);
        if ($student) {
            if ($this->userCan('add-student-log', $student)) {
                $log = new StudentLog();
                $log->user_id = Auth::id();
                $log->student_id = $student_id;
                $log->log = $request->input("log");
                $log->save();
                return redirect()->back();
            } else abort('403');
        } else {
            abort("404");
        }
    }

    public function removeLog(Request $request, $student_id, $log_id)
    {
        $student = Student::where('center_id', '=', Session::get('centerId'))->find($student_id);
        if ($student) {
            if ($this->userCan('add-student-log', $student)) {
                $log = StudentLog::find($log_id);
                if ($log) {
                    $log->delete();
                    return response()->json([]);
                } else {
                    abort("404");
                }
            } else abort('403');
        } else {
            abort("404");
        }
    }

    public function updateLog(Request $request, $student_id, $log_id)
    {
        $student = Student::where('center_id', '=', Session::get('centerId'))->find($student_id);
        if ($student) {
            if ($this->userCan('add-student-log', $student)) {
                $log = StudentLog::find($log_id);
                if ($log) {
                    $log->log = $request->input('value');
                    $log->save();
                    return response()->json([]);
                } else {
                    abort("404");
                }
            } else abort('403');
        } else {
            abort("404");
        }
    }


    public function addScore($studentId, $courseId)
    {
        $student = Student::where('center_id', '=', Session::get('centerId'))->find($studentId);
        if ($student) {
            if ($this->userCan('crud-student-score', Session::get('centerId'))) {
                $course = Course::find($courseId);
                if ($course) {
                    return view('pageAdmin.exam_score.add-score-student', compact(['student', 'course']));
                } else {
                    abort('404');
                }
            } else {
                abort('403');
            }
        } else {
            abort('404');
        }
    }

    public function createScore(ExamRequest $request, $studentId, $courseId)
    {
        $student = Student::where('center_id', '=', Session::get('centerId'))->find($studentId);
        if ($student) {
            if ($this->userCan('crud-student-score', Session::get('centerId'))) {
                $examDate = Carbon::createFromFormat('d-m-Y', $request->input('exam_date'))->format('Y-m-d');
                $hasExam = Exam::where('course_id', '=', $courseId)
                    ->where('date', '=', $examDate)
                    ->get();

//                  Kiểm tra khóa học đó vào ngày đó đã có điểm chưa
                if (count($hasExam) > 0) {
                    Session::flash('has-exam', $request->input('exam_date'));
                    return redirect()->back();
                }
                $examScore = $request->input('score');

                $exam = new Exam();
                $exam->date = $examDate;
                $exam->course_id = $courseId;
                $exam->save();
                $exam->assignStudent($student, ['score' => $examScore]);
                Session::flash('add_score_success', __('language.add_score_success'));

//                $exams = Exam::with('students')
//                    ->where('course_id', '=', $exam->course_id)
//                    ->whereHas('students', function ($query) use ($studentId) {
//                        $query->where('students.id', '=', $studentId);
//                    })->get();

//                $courses = Course::all();
//                $selectCourse = Course::find($courseId);
                return redirect()->route('student.tab.score', $student->id);
//                return view('pageAdmin.student.tab-score', compact(['student', 'courses', 'selectCourse', 'exams']));
            } else {
                abort('403');
            }
        } else {
            abort('404');
        }
    }

    public function editScore($studentId, $examId)
    {
        $view = 'pageAdmin.exam_score.edit-score-student';
        return $this->getScoreStudent($studentId, $examId, $view);
    }

    public function updateScore(Request $request, $studentId, $examId, $courseId)
    {
        $student = Student::where('center_id', '=', Session::get('centerId'))->find($studentId);
        if ($student) {
            if ($this->userCan('crud-student-score', Session::get('centerId'))) {
                $exam = Exam::with('students')
                    ->where('course_id', '=', $courseId)
                    ->find($examId);

                if ($exam) {
                    $examDate = Carbon::createFromFormat('d-m-Y', $request->input('exam_date'))->format('Y-m-d');
                    if ($exam->date !== $examDate) {
                        $this->validate($request, [
                            'exam_date' => 'date|before:today',
                        ], [
                            'exam_date.date' => __('language.exam_date'),
                            'exam_date.before' => __('language.exam_date_before_current'),
                        ]);
                        $exam->date = $examDate;
                        $exam->save();
                    }

                    $examScore = $request->input('score');
                    foreach ($exam->students as $s)
                        if ($s->pivot->score !== $examScore) {
                            $exam->removeStudent($student);
                            $exam->assignStudent($student, ['score' => $examScore]);
                        }

                    Session::flash('edit_score_success', __('language.edit_score_success'));
                    $exams = Exam::with('students')
                        ->where('course_id', '=', $exam->course_id)
                        ->whereHas('students', function ($query) use ($studentId) {
                            $query->where('students.id', '=', $studentId);
                        })->get();

                    $courses = Course::all();
                    $selectCourse = Course::find($courseId);
                    return view('pageAdmin.student.tab-score', compact(['student', 'courses', 'selectCourse', 'exams']));
                } else {
                    abort('404');
                }
            } else {
                abort('403');
            }
        } else {
            abort('404');
        }
    }

    public function deleteScore($studentId, $examId)
    {
        $view = 'pageAdmin.exam_score.delete-score-student';
        return $this->getScoreStudent($studentId, $examId, $view);
    }

    public function destroyScore($studentId, $examId)
    {
        $student = Student::where('center_id', '=', Session::get('centerId'))->find($studentId);
        if ($student) {
            if ($this->userCan('crud-student-score', Session::get('centerId'))) {
                $exam = Exam::find($examId);
                if ($exam) {
                    $exam->delete();
                    Session::flash('delete_score_success', __('language.delete_score_success'));
                    $exams = Exam::with('students')
                                ->where('course_id', '=', $exam->course_id)
                                ->whereHas('students', function ($query) use ($studentId) {
                                    $query->where('students.id', '=', $studentId);
                                })
                                ->get();
                    $courses = Course::all();
                    $selectCourse = Course::find($exam->course_id);
                    return view('pageAdmin.student.tab-score', compact(['student', 'courses', 'selectCourse', 'exams']));

                } else {
                    abort('404');
                }
            } else {
                abort('403');
            }
        } else {
            abort('403');
        }
    }

    private function getScoreStudent($studentId, $examId, $view)
    {
        $student = Student::where('center_id', '=', Session::get('centerId'))->find($studentId);
        if ($student) {
            if ($this->userCan('crud-student-score', Session::get('centerId'))) {
                $exam = Exam::with('students')->find($examId);
                if ($exam) {
                    $course = Course::find($exam->course_id);
                    if ($course) {
                        return view($view, compact(['student', 'exam', 'course']));
                    } else {
                        abort('404');
                    }
                } else {
                    abort('404');
                }
            } else {
                abort('403');
            }
        } else {
            abort('404');
        }
    }

}