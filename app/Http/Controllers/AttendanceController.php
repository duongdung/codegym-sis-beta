<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Attendancedetail;
use App\Group;
use App\Http\Requests\AttendanceRequest;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use dateTime;
class AttendanceController extends Controller
{

    public function create($id){
        $group  = Group::with('students')
            ->find($id);
        if (empty($group)) {
            abort('404');
        }
        if ($this->userCan('create-edit-view-attendance', $group)) {
            return view('pageAdmin.attendance.add', compact( 'group'));
        }
        abort('403');
    }

    public function store(AttendanceRequest $request,$id){
        $group  = Group::with('students')
            ->find($id);
        if (empty($group)) {
            abort('404');
        }
        if ($this->userCan('create-edit-view-attendance', $group)){
            $attendances = Attendance::with('group')
                ->where('group_id','=',$id)
                ->get();
            $date = $request->input('date_attedance');
            foreach ($attendances as $attendance){
                if( $date === $attendance->created_at->format('Y-m-d') ){
                    $this->validate($request,
                        [
                            'date_attedance'            => 'required|unique:attendances,created_at|date|before:tomorrow',
                        ],[
                            'date_attedance.required'   => __('language.text_required'),
                            'date_attedance.unique'     => __('language.date_attedance_unique'),
                            'date_attedance.date'       => __('language.date_attedance_date'),
                            'date_attedance.before'     => __('language.date_attedance_before'),
                        ]);
                }
            }
            $user = Auth::user();
            $attendance                = new Attendance();
            $attendance->user_id    = $user->id;
            $attendance->group_id   = $id;
            $attendance->created_at  = $date;
            $attendance->save();
            $attendanceStatus       = $request->input('attend');
            foreach ($attendanceStatus as $key => $item) {
                $attentdetail = new Attendancedetail();
                $attentdetail->attendance_id = $attendance->id;
                $attentdetail->student_id    = $key;
                $attentdetail->attendance    = $item;
                $attentdetail->created_at    = $date;
                $attentdetail->save();
            }
            Session::flash('success', __('language.Attendance_Success'));
            return redirect()->route('attendance.list', $id);
        }
        abort('403');
    }
    public function showlist($id){
        $group  = Group::with('students')
            ->find($id);
        if ($group) {
            if ($this->userCan('create-edit-view-attendance', $group)){
                $attendances = Attendance::with('group','user','attendancedetails')
                    ->withCount(['attendancedetails as absent'=>function($q){
                        $q->where('attendance',1);
                    }])
                    ->withCount(['attendancedetails as leave'=>function($q){
                        $q->where('attendance',2);
                    }])
                    ->withCount(['attendancedetails as late'=>function($q){
                        $q->where('attendance',3);
                    }])
                    ->withCount(['attendancedetails as present'=>function($q){
                        $q->where('attendance',4);
                    }])

                    ->where('group_id','=',$id)
                    ->orderBy('created_at','desc')
                    ->paginate(10);

                return view('pageAdmin.attendance.list',compact('attendances','group','attedance_count'));
            }
            abort('403');
        }
        else{
            abort('404');
        }

    }

    public function detail($groupId,$attendanceId = null)
    {
        $attendanceId = $_GET['attendance'];
        if ($attendanceId){
                $attendance = $this->hashAttendance($groupId,$attendanceId);
                if ($this->userCan('create-edit-view-attendance', $attendance->group)) {
                    return view('pageAdmin.attendance.detail', compact('attedancedetail','attendance'));
                }abort('403');
        }else{
            abort('404');
        }
    }
    public function hashAttendance($groupId ,$attendanceId){
        if ($groupId && $attendanceId){
            $attendance = Attendance::with('group','attendancedetails')
                        ->where('group_id','=',$groupId)->find($attendanceId);
            if ($attendance){
                return $attendance;
            }
            else{
                abort('404');
            }
        }else{
            abort('404');
        }
    }

    public function delete($groupId,$attendanceId = null){
        $attendanceId = $_GET['attendance'];
        if ($attendanceId){
            $attendance = $this->hashAttendance($groupId,$attendanceId);
            if ($this->userCan('delete-attendance', Session::get('centerId'))){
                return view('pageAdmin.attendance.delete',compact('attendance'));
            }abort('403');
        }else{
            abort('404');
        }

    }

    public function destroy($groupId,$attendanceId = null){
        $attendanceId = $_GET['attendance'];
        if ($attendanceId){
            $attendance = $this->hashAttendance($groupId,$attendanceId);
            if ($this->userCan('delete-attendance', Session::get('centerId'))){
                $attendance->students()->detach();
                $attendance->delete();
                Session::flash('success', __('language.Delete_Success'));
                return redirect()->route('attendance.list',$attendance->group->id);
            }
            abort('403');
        }else{
            abort('404');
        }


    }

    public function update($groupId ,$attendanceId = null){
        $attendanceId = $_GET['attendance'];
        if ($attendanceId){
            $attendance = $this->hashAttendance($groupId,$attendanceId);
            if($this->userCan('create-edit-view-attendance', $attendance->group)){
                return view('pageAdmin.attendance.edit',compact('attendance'));
            }
            abort('403');
        }else{
            abort('404');
        }
    }

    public function edit(Request $request, $groupId,$attendanceId = null){

        $attendanceId = $_GET['attendance'];
        if ($attendanceId){
            $attendance = $this->hashAttendance($groupId,$attendanceId);
            if($this->userCan('create-edit-view-attendance', $attendance->group)){
                $user = Auth::user();
                if (empty($attendance)){
                    abort('404');
                }
                $attendance->user_id        = $user->id;
                $attendance->updated_at     = date('Y-m-d');
                $attendance->save();
                $attendanceStatus           = $request->input('attend');
                foreach($attendance->students as $key => $student){
                    $attendance->students()->updateExistingPivot($student->id,['attendance'=>$attendanceStatus[$student->id]]);
                }
                Session::flash('success', __('language.Success'));
                return redirect()->route('attendance.detail',['groupId'=>$groupId,'attendance'=>$attendanceId]);
            }
            abort('403');
        }else{
            abort('404');
        }

    }

}
