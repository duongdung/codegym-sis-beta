<?php

namespace App\Http\Controllers;

use App\Center;
use App\Course;
use App\Group;
use App\Role;
use App\Student;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($this->userCan('view-statistics-all')) {
            $statistics = $this->getTotalOfAllObjects();
        }
        if ($this->userCan('view-statistics-center', Session::get('centerId'))) {
            $statistics = $this->getTotalOfObjectsCenter();
        }
        if ($this->userCan('view-statistics-for-teacher', Session::get('centerId'))) {
            $statistics = $this->getTotalForTeacher();
        }
        return view('home', compact('statistics', 'group'));
    }

    /**
     * @return array
     */
    private function getTotalOfAllObjects()
    {
        $centers = Center::all();
        $countCenter = str_pad($centers->count(), 2, '0', STR_PAD_LEFT);

        $courses = Course::all();
        $countCourse = str_pad($courses->count(), 2, '0', STR_PAD_LEFT);

        $groups = Group::all();
        $countGroup = str_pad($groups->count(), 2, '0', STR_PAD_LEFT);

        $students = Student::all();
        $countStudent = str_pad($students->count(), 2, '0', STR_PAD_LEFT);

        $teachers = User::with('roles')
            ->whereHas('roles', function ($q) {
                $q->where('role_id', '=', RoleConstants::ROLE_TUTOR)
                    ->orWhere('role_id', '=', RoleConstants::ROLE_COACH);
            })->get();
        $countTeacher = str_pad($teachers->count(), 2, '0', STR_PAD_LEFT);

        $users = User::all();;
        $countUser = str_pad($users->count(), 2, '0', STR_PAD_LEFT);
        return array('countCenter' => $countCenter,
            'countCourse' => $countCourse,
            'countGroup' => $countGroup,
            'countStudent' => $countStudent,
            'countTeacher' => $countTeacher,
            'countUser' => $countUser);
    }

    private function getTotalOfObjectsCenter()
    {
        $courses = Course::all();
        $countCourse = str_pad($courses->count(), 2, '0', STR_PAD_LEFT);

        $groups = Group::where('center_id', '=', Session::get('centerId'))->get();
        $countGroup = str_pad($groups->count(), 2, '0', STR_PAD_LEFT);

        $students = Student::where('center_id', '=', Session::get('centerId'))->get();
        $countStudent = str_pad($students->count(), 2, '0', STR_PAD_LEFT);

        $teachers = User::with('roles')
            ->where('center_id', Session::get('centerId'))
            ->whereHas('roles', function ($q) {
                $q->where('role_id', '=', RoleConstants::ROLE_TUTOR)
                    ->orWhere('role_id', '=', RoleConstants::ROLE_COACH);
            })->get();
        $countTeacher = str_pad($teachers->count(), 2, '0', STR_PAD_LEFT);

        $users = User::with('roles')
            ->where('center_id', '=', Session::get('centerId'))
            ->get();;
        $countUser = str_pad($users->count(), 2, '0', STR_PAD_LEFT);
        return array('countCourse' => $countCourse,
            'countGroup' => $countGroup,
            'countStudent' => $countStudent,
            'countTeacher' => $countTeacher,
            'countUser' => $countUser);
    }

    private function getTotalForTeacher()
    {
        $courses = Course::all();
        $countCourse = str_pad($courses->count(), 2, '0', STR_PAD_LEFT);

        $groups = Group::with('users')
            ->whereHas('users', function ($q) {
                $q->where('user_id', Auth::id());
            })->get();
        $countGroup = str_pad($groups->count(), 2, '0', STR_PAD_LEFT);

        return array('countCourse' => $countCourse, 'countGroup' => $countGroup);
    }
}
