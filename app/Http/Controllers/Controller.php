<?php

namespace App\Http\Controllers;

use App\Center;
use App\Report;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function postCenter(Request $request)
    {
        $request->session()->put("centerId", $request->center);
        return redirect()->route('home');
    }

    public function userCan($action, $option = NULL)
    {
        $user = Auth::user();
        return Gate::forUser($user)->allows($action, $option);
    }

    public function postReport(Request $request){
        $request->session()->put("reportId", $request->reportId);
        return redirect()->back();
    }

    public function formatDate($date){
        if ($date){
            $date->date_format('m-d-Y');
            return $date;
        }
    }

}
