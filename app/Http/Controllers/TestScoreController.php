<?php

namespace App\Http\Controllers;

use App\Course;
use App\Group;
use App\Http\Requests\TestScoreRequest;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TestScoreController extends Controller
{
    public function selectCourse($id)
    {
        if ($this->userCan('crud-student-score', Session::get('centerId'))) {
            $group = Group::where('center_id', '=', Session::get('centerId'))->find($id);
            $courses = Course::all();
            if (empty($group)) {
                abort('404');
            }
            return view('pageAdmin.exam_score.select-course', compact('group', 'courses'));
        }
        abort('403');
    }

    public function addScore(Request $request, $groupId, $courseName = null)
    {
        if ($this->userCan('crud-score', Session::get('centerId'))) {
            $courseName = $_GET['courseName'];
            $group = Group::with('students')
                ->where('center_id', '=', Session::get('centerId'))
                ->where('id', '=', $groupId)
                ->first();

            if (empty($group)) {
                abort('404');
            }
            return view('pageAdmin.exam_score.add-score', compact('group', 'courseName'));
        }
        abort('403');
    }

    public function store(Request $request)
    {
        if ($this->userCan('crud-score', Session::get('centerId'))) {
            $testScore = $request->testScore;
            $courseName = $request->courseName;
            $groupId = $request->groupId;

            $course = Course::where('course_name', '=', $courseName)->first();
            $group = Group::where('group_id', '=', $groupId)->first();
            if (empty($group)) {
                abort('404');
            }
            //Gán điểm vào từng sinh viên trong lớp
            foreach ($testScore as $key => $t) {
                if ((!is_numeric($t)) || ($t < 1) || ($t > 100)) {
                    Session::flash('test-score', __('language.Test_Score_Number'));
                    return redirect()->back();
                }
                $course->students()->attach($key, ['test_score' => $t, 'group' => $group->id]);
            }
            Session::flash('add-success', __('language.add_score_success'));
            return redirect()->route('test-score.select.course', $groupId);
        }
        abort('403');
    }

    public function showScore(Request $request, $groupId)
    {
        if ($this->userCan('crud-score', Session::get('centerId'))) {
            $courseId = $request->changeCourse;
            $selectedCourse = Course::where('id', '=', $courseId)->first();
            if (isset($selectedCourse)) {
                $courses  = Course::all();
                $group    = Group::find($groupId);
                $students = Student::where('group_id', '=', $groupId)
                    ->with(['courses' => function ($q) use ($courseId){
                        $q->where('courses.id', $courseId);
                    }])
                    ->get();
                return view('pageAdmin.exam_score.select-course', compact('courses', 'group', 'selectedCourse', 'students'));
            } else {
                abort('404');
            }
        } else {
            abort('403');
        }
    }

}
