<?php

namespace App\Http\Controllers;

use App\Center;
use App\Http\Requests\CenterRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CenterController extends Controller
{
    public function create()
    {
        if ($this->userCan('crud-center')) {
            return view('pageAdmin.center.add');
        }else{
            abort('403');
        }
    }

    public function store(CenterRequest $request)
    {

        if ($this->userCan('crud-center')) {
            $center = new Center();
            $center->name        = $request->input('center_name');
            $center->address     = $request->input('center_address');
            $center->center_code = $request->input('center_code');
            $center->phone       = $request->input('center_phone');
            $center->email       = $request->input('center_email');
            $center->save();
            Session::flash('success', __('language.Add_Success'));
            return redirect()->route('center.list');
        }else{
            abort('403');
        }
    }

    public function showList()
    {
        if ($this->userCan('view-center')) {
            $centers = Center::all();
            return view('pageAdmin.center.list', compact('centers'));
        } else{
            abort('403');
        }
    }


    public function delete($id){

        if ($this->userCan('crud-center')){

            $center = Center::with('groups')->find($id);
            if ($center){
                return view('pageAdmin.center.delete',compact('center'));
            }else{
                abort('404');
            }
        }else{
            abort('403');
        }

    }
    public function destroy($id){

        if ($this->userCan('crud-center')){

            $center = $this->checkCenter($id);
            $center->delete();
            Session::flash('success', __('language.Success_delete'));

            return redirect()->route('center.list');
        }else{
            abort('403');
        }

    }

    public function update($id){
        if ($this->userCan('crud-center')){
            $center = $this->checkCenter($id);
            return view('pageAdmin.center.edit',compact('center'));
        }else{
            abort('403');
        }
    }

    public function edit(Request $request,$id){
        if ($this->userCan('crud-center')){

            $center = $this->checkCenter($id);

            $nameCenter = $center->name;
            if ($request->center_name !== $nameCenter){
                $this->validate($request, [
                    'center_name'   =>  'required|unique:centers,name|min:3|max:100',
                ],[
                    'center_name.required'  =>  __('language.Center_name_request'),
                    'center_name.unique'    =>  __('language.Center_name_request_name'),
                    'center_name.min'       =>  __('language.Center_name_request_min'),
                    'center_name.max'       =>  __('language.Center_name_request_max'),
                ]);
                $center->name = $request->center_name;
            }

            $codeCenter = $center->center_code;

            if ($request->center_code !== $codeCenter){
                $this->validate($request, [
                    'center_code'   =>  'required|unique:centers,center_code|min:3|max:50',
                ],[
                    'center_code.required'  =>  __('language.Center_code_request'),
                    'center_code.unique'    =>  __('language.Center_request_code'),
                    'center_code.min'       =>  __('language.Center_code_request_min'),
                    'center_code.max'       =>  __('language.Center_code_request_max'),
                ]);
                $center->center_code = $request->center_code;
            }

            $phoneCenter = $center->phone;

            if ($request->center_phone !== $phoneCenter){
                $this->validate($request, [
                    'center_phone'  =>  'required|regex:/(0)[0-9]{9}/|numeric',
                ],[
                    'center_phone.required' =>  __('language.Center_phone_request'),
                    'center_phone.regex'    =>  __('language.Center_request_phone'),
                    'center_phone.numeric'  =>  __('language.Center_request_phone'),
                ]);
                $center->phone = $request->center_phone;
            }

            $emailCenter = $center->email;
            if ($request->center_email !== $emailCenter){
                $this->validate($request, [
                    'center_email'  =>  'required|email|unique:centers,email',
                ],[
                    'center_email.email'    =>  __('language.Center_request_email'),
                    'center_email.unique'   =>  __('language.Center_request_email_unique'),
                ]);
                $center->email = $request->center_email;
            }

            $this->validate($request, [
                'center_address'=>  'required',
            ],[
                'center_address.required'        =>  __('language.text_required')
            ]);
            $center->address = $request->center_address;

            $center->save();

            Session::flash('success', __('language.edit_success'));
            return redirect()->route('center.list');
        }else{
            abort('403');
        }
    }

    public function checkCenter($id){

        if($id){
            $center = Center::find($id);
            if ($center) {
                return $center;
            }else{
                abort('404');
            }
        }else{
            abort('404');
        }
    }


}
