<?php

namespace App\Http\Controllers;

use App\CategoryCriteria;
use App\GroupCriteria;
use App\Http\Requests\CategoryCriteriaRequest;
use Illuminate\Http\Request;


class CategoryCriteriaController extends Controller
{
    public function create($reportId,$groupCriteriaId = null){

        $groupCriteriaId = $_GET['group-criteria'];
        if ($this->userCan('crud-report')) {
            $groups_criteria = $this->hashGroupCriteria($reportId,$groupCriteriaId);
            return view('pageAdmin.category_criteria.add',compact('groups_criteria','reportId'));
        }
        abort('403');
    }

    public function store(CategoryCriteriaRequest $request,$reportId,$groupCriteriaId = null){

        $groupCriteriaId = $_GET['group-criteria'];
        if ($this->userCan('crud-report')) {
            $groups_criteria = $this->hashGroupCriteria($reportId,$groupCriteriaId);
            $categoryCriteria = new CategoryCriteria();
            $categoryCriteria->name = $request->input('name');
            $categoryCriteria->group_criteria_id = $groups_criteria->id;
            $categoryCriteria->save();
            return redirect()->route('report.detail',$reportId);
        }
        abort('403');
    }

    public function hashGroupCriteria($reportId,$groupCriteriaId){
        if ($reportId && $groupCriteriaId){
            $groupCriteria = GroupCriteria::where('report_id','=',$reportId)->find($groupCriteriaId);
            if ($groupCriteria){
                return $groupCriteria;
            }else{
                abort('404');
            }
        }else{
            abort('404');
        }

    }
    public function update(CategoryCriteriaRequest $request,$reportId,$groupCriteriaId,$categoryCriteriaId){

        if ($this->userCan('crud-report')) {
            $groups_criteria = $this->hashGroupCriteria($reportId,$groupCriteriaId);

            $categoryCriteria  = CategoryCriteria::with('groupCriteria')->where('group_criteria_id','=',$groupCriteriaId)
                                ->find($categoryCriteriaId);
            if ($categoryCriteria){

                $categoryCriteria->name = $request->input('value');
                $categoryCriteria->save();
                return response()->json([]);
            }else{
                abort('404');
            }
        }else{
            abort('403');
        }
    }

    public function destroy($reportId,$groupCriteriaId,$categoryCriteriaId){
        if ($this->userCan('crud-report')) {
            $groups_criteria = $this->hashGroupCriteria($reportId,$groupCriteriaId);

            $categoryCriteria  = CategoryCriteria::with('groupCriteria')->where('group_criteria_id','=',$groupCriteriaId)
                ->find($categoryCriteriaId);
            if ($categoryCriteria){
                $categoryCriteria->delete();
                return redirect()->back();
            }else{
                abort('404');
            }
        }else{
            abort('403');
        }
    }
}
