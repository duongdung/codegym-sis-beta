<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReportRequest;
use App\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ReportController extends Controller
{
    public  function create(){

        if ($this->userCan('crud-report')) {
            return view('pageAdmin.report.add');
        }else{
            abort('403');
        }
    }

    public function store(ReportRequest $request){

        if ($this->userCan('crud-report')) {
            $report = new Report();
            $report->name = $request->input('name');
            $report->description = $request->input('report_description');
            $report->save();
            Session::flash('success', __('language.Add_Success'));
            return redirect()->route('report.list');
        }else {
            abort('403');
        }
    }

    public function showList(){

        if ($this->userCan('crud-report')) {
            $reports =  Report::all();
            return view('pageAdmin.report.list',compact('reports'));
        }else{
            abort('403');
        }

    }

    public function delete($id){

        if ($this->userCan('crud-report')) {
            $report = Report::find($id);
            if ($report){
                return view('pageAdmin.report.delete',compact('report'));
            }else{
                abort('404');
            }
        }else{
            abort('403');
        }
    }

    public function destroy(Request $request,$id){

        if ($this->userCan('crud-report')) {
            $report = Report::find($id);
            $request->session()->forget('reportId');
           if ($report){
               $report->delete();
               Session::flash('success', __('language.Delete_Success'));
               return redirect()->route('report.list');
           }else{
               abort('404');
           }
        }else{
            abort('403');
        }
    }

    public function detail($id){

        if ($this->userCan('crud-report')) {
            $report = Report::with('groups_criteria')->find($id);
            if ($report){
                return view('pageAdmin.report.detail',compact('report'));
            }else{
                abort('404');
            }
        }
        else{
            abort('403');
        }
    }
    public function updateName(Request $request,$reportId){
        if ($this->userCan('crud-report')) {
            $report = Report::find($reportId);
            if ($report){
                $report->name = $request->input('value');
                $report->save();
                return response()->json([]);
            }else{
                abort('404');
            }
        }else{
            abort('403');
        }
    }
    public function updateDescription(Request $request,$reportId){
        if ($this->userCan('crud-report')) {
            $report = Report::find($reportId);
            if ($report){
                $report->description = $request->input('value');
                $report->save();
                return response()->json([]);
            }else{
                abort('404');
            }
        }else{
            abort('403');
        }
    }



}
