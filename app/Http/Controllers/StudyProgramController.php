<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StudyProgramRequest;
use App\Program;
use Illuminate\Support\Facades\Session;
use App\Group;

class StudyProgramController extends Controller
{
    public function create()
    {
        if ($this->userCan('manager-program')) {
            return view('pageAdmin.program.add');
        }abort('403');
    }

    public function store(StudyProgramRequest $request)
    {
        if ($this->userCan('manager-program')) {
            $program = new Program();
            $program->name_program = $request->input('name_study_program');
            $program->program_description = $request->input('study_program_description');
            $program->length_of_program = $request->input('length_of_program');
            $program->save();
            Session::flash('success', __('language.Add_Success'));
            return redirect()->route('program.list');
        }abort('403');
    }

    public function showList()
    {
        if ($this->userCan('manager-program')) {
        $programs = Program::paginate(20);
        return view('pageAdmin.program.list', compact('programs'));
        }abort('403');

    }

    public function showDetail($id)
    {
        if ($this->userCan('manager-program')) {

            $program = Program::find($id);

            $groupPrograms = Group::with('program','center')
                ->where('center_id','=',Session::get('centerId'))
                ->where('program_id','=',$id)
                ->paginate(10);

            if (empty($program)) {
                abort('404');
            } else {
                return view('pageAdmin.program.detail', compact('program', 'groupPrograms'));
            }
        }abort('403');
    }

    public function destroy($id)
    {
        if ($this->userCan('manager-program')) {

            $studyProgram = Program::find($id);
            if (empty($studyProgram)) {
                abort(404);
            }
            $studyProgram->delete();
            Session::flash('success', __('language.Success_delete'));
            return redirect()->route('program.list');
        }abort('403');
    }

    public function delete($id)
    {
        if ($this->userCan('manager-program')) {
            $studyProgram = Program::find($id);
            if (empty($studyProgram)) {
                abort(404);
            }
            return view('pageAdmin.program.delete', compact('studyProgram'));
        }abort('403');
    }

    public function edit(Request $request ,$id){

        if ($this->userCan('manager-program')) {
            $program = Program::find($id);

            if (empty($program)) {
                abort('404');
            } else {
                return view('pageAdmin.program.edit', compact('program'));
            }
        }abort('403');
    }

    public function update(Request $request, $id)
    {
        if ($this->userCan('manager-program')) {

            $program = Program::find($id);

            $nameprogram = $program->name_program;

            if ($request->name_study_program !== $nameprogram){
                    $this->validate($request, [
                        'name_study_program'        =>'required|min:3|max:100',
                    ],
                    [
                        'name_study_program.required'   => __('language.text_required'),
                        'name_study_program.min'        => __('language.name_study_program_min'),
                        'name_study_program.max'        => __('language.name_study_program_max'),
                        ]);
                $program->name_program = $request->input('name_study_program');
            }
                $this->validate($request, [
                    'length_of_program'         =>'required|min:2|max:100',
                ],
                    [
                        'length_of_program.required'    => __('language.text_required'),
                        'length_of_program.min'          => __('language.length_of_program_min'),
                        'length_of_program.max'          => __('language.length_of_program_max'),
                    ]);
            $program->program_description = $request->input('study_program_description');
            $program->length_of_program = $request->input('length_of_program');
            $program->save();
            Session::flash('success', __('language.Success'));
            return redirect()->route('program.list');
        }abort('403');
    }

}
