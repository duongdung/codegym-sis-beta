<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Attendancedetail;
use App\Group;
use App\Http\Requests\AttendanceDetailRequest;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\In;

class AttendanceDetailController extends Controller
{
    public function showfive($id)
    {
        $group=Group::find($id);
        if(!$group){
            abort('404');
        }
        if ($this->userCan('create-edit-view-attendance', $group)) {
            $attendances = Attendance::where('group_id', '=', $id)
                ->orderBy('created_at', 'desc')
                ->take(5)
                ->get();
            $attendance_ids = $attendances->map(function ($atendance){
                return $atendance->id;
            });
            $students = Student::with(['attendances'=>function($q) use ($attendance_ids){
                $q->whereIn('attendance_id', $attendance_ids);
            }])
                ->where('group_id', $id)
                ->get();

            return view('pageAdmin.attendance.result', compact(['students', 'attendances','group']));
        }
        abort('403');
    }
    public function showatteanDate(AttendanceDetailRequest $request,$id){
        $group=Group::find($id);
        if(!$group){
            abort('404');
        }
        if ($this->userCan('create-edit-view-attendance', $group)) {
            $dateTo = $request->input('date_to');
            $dateFrom = $request->input('date_from');
            $attendances = Attendance::where('group_id', '=', $id)
                    ->where('created_at','>=',$dateTo)
                    ->orWhere('created_at','=<',$dateFrom)
                    ->orderBy('created_at', 'desc')
                    ->get();
            $attendance_ids = $attendances->map(function ($atendance){
                return $atendance->id;
            });
            $students = Student::with(['attendances'=>function($q) use ($attendance_ids){
                $q->whereIn('attendance_id', $attendance_ids);
            }])
                ->where('group_id', $id)
                ->get();
            return view('pageAdmin.attendance.result', compact('students', 'attendances','group'));
        }
        abort('403');
    }
}
