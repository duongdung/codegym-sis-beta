<?php

namespace App\Http\Controllers;

use App\Course;
use App\Exam;
use App\GroupLog;
use App\Http\Requests\GroupScoreRequest;
use App\Http\Requests\LogRequest;
use App\Student;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\GroupRequest;
use App\Group;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use App\Program;


class GroupController extends Controller
{
    public function create()
    {
        if ($this->userCan('crud-group', Session::get('centerId'))) {
            $programs = Program::all();
            return view('pageAdmin.group.add', compact('programs'));
        }
        abort('403');
    }

    public function store(GroupRequest $request)
    {
        if ($this->userCan('crud-group', Session::get('centerId'))) {

            $group = new Group();
            $group->group_id = $request->group_id;
            $group->program_id = $request->program_id;
            $group->group_description = $request->description;
            $startdate_group = Carbon::createFromFormat('d-m-Y', $request->input('startdate_group'))->format('Y-m-d');
            $expected_end_date_group = Carbon::createFromFormat('d-m-Y', $request->input('expected_end_date_group'))->format('Y-m-d');
            $group->startdate_group = $startdate_group;
            $group->expected_end_date_group = $expected_end_date_group;
            $group->center_id = Session::get('centerId');
            $group->note = $request->note;
            $group->save();

            Session::flash('success', __('language.Group_Add_Success'));
            return redirect()->route('group.list');
        }
        abort('403');
    }

    public function showList()
    {
        if ($this->userCan('view-groups')) {
            if ($this->userCan('crud-group', Session::get('centerId'))) {
                $groups = Group::where('center_id', '=', Session::get('centerId'))->paginate(20);
            } else {
                $groups = Group::where('center_id', '=', Session::get('centerId'))
                    ->whereHas('users', function ($q) {
                        $q->where('users.id', Auth::user()->id);
                    })
                    ->paginate(20);
            }
            return view('pageAdmin.group.list', compact('groups'));
        }
        abort('403');
    }

    public function showDetail($groupId)
    {
        $group = Group::with(['program', 'users'])
            ->where('id', '=', $groupId)
            ->first();

        if (!empty($group)) {
            if ($this->userCan('view-group', $group)) {
                return view('pageAdmin.group.tab-detail', compact('group'));
            } else {
                abort('403');
            }
        } else {
            abort('404');
        }
    }

    public function showStudent($groupId)
    {
        $group = Group::with(['students'])
            ->where('id', '=', $groupId)
            ->first();

        if (empty($group)) {
            abort('404');
        }
        if ($this->userCan('view-group', $group)) {
            return view('pageAdmin.group.tab-student', compact(['group']));
        }
        abort('403');
    }

    public function showTabScore(Request $request, $groupId)
    {
        $group = Group::with('students')
            ->where('center_id', '=', Session::get('centerId'))->find($groupId);
        if ($group) {
            if ($this->userCan('view-group-score', $group)) {
                $courses = Course::all();
                $selectCourseId = $request->input('selectCourse');
                $selectCourse = Course::find($selectCourseId);
                $exams = Exam::with('students')
                    ->where('course_id', '=', $selectCourseId)
                    ->whereHas('students', function ($query) use ($group) {
                        $query->where('students.group_id', '=', $group->id);
                    })->get();
                return view('pageAdmin.group.tab-exam-score', compact('group', 'courses'));
            }
            abort('403');
        } else {
            abort('404');
        }
    }

    public function checkScore(Request $request, $groupId)
    {
        $group = Group::find($groupId);
        if ($group) {
            if ($this->userCan('crud-group-score', Session::get('centerId'))) {
                $selectCourseId = $request->selectCourse;
                $course = Course::where('id', '=', $selectCourseId)->first();
                if ($course) {
                    Session::flash('select_course_id', $selectCourseId);
                } else {
                    Session::flash('no_select_course_id', Session::get('Course_Null'));
                }
                return redirect()->back();
            } else {
                abort('403');
            }
        } else {
            abort('404');
        }
    }

    public function addScore(GroupScoreRequest $request, $groupId)
    {
        $group = Group::find($groupId);
        if ($group) {
            if ($this->userCan('crud-group-score', Session::get('centerId'))) {
                $courseId = $request->courseSelectId;
                $studentId = $request->studentId;
                $examScore = $request->testScore;
                $examDate = Carbon::createFromFormat('d-m-Y', $request->input('exam_date'))->format('Y-m-d');

                $course = Course::where('id', '=', $courseId)->first();
                if ($course) {
                    $exam = new Exam();
                    $exam->date = $examDate;
                    $exam->course_id = $courseId;
                    $exam->save();
                    for ($i = 0; $i < count($examScore); $i++) {
                        $student = Student::where('center_id', '=', Session::get('centerId'))->find($studentId[$i]);
                        if ($student) {
                            if ($examScore[$i] != null) {
                                $exam->assignStudent($student, ['score' => $examScore[$i]]);
                                Session::flash('add_score_success', __('language.add_score_success'));
                            }
                        } else {
                            abort('404');
                        }
                    }
                    return redirect()->back();
                } else {
                    abort('404');
                }
            } else {
                abort('403');
            }
        } else {
            abort('404');
        }
    }

    public function showScore(GroupScoreRequest $request, $groupId)
    {
        $group = Group::with('students')
            ->where('center_id', '=', Session::get('centerId'))->find($groupId);
        if ($group) {
            if ($this->userCan('view-group-score', $group)) {
                $selectCourseId = $request->input('selectCourse');
                $selectCourse = Course::find($selectCourseId);
                $courses = Course::all();
                $exams = Exam::with('students')
                    ->where('course_id', '=', $selectCourseId)
                    ->whereHas('students', function ($query) use ($groupId) {
                        $query->where('students.group_id', '=', $groupId);
                    })->get();
                return view('pageAdmin.exam_score.list-score-group', compact('group', 'courses', 'selectCourse', 'exams'));
            } else {
                abort('403');
            }
        } else {
            abort('404');
        }
    }

    public function showLog($groupId)
    {
        $group = Group::with(['students', 'program', 'users'])
            ->where('id', '=', $groupId)
            ->first();

        if (empty($group)) {
            abort('404');
        }
        if ($this->userCan('view-group', $group)) {
            $logs = GroupLog::where('group_id', $groupId)
                ->orderBy('created_at', 'desc')
                ->paginate(20);
            return view('pageAdmin.group.tab-log', compact(['group', 'logs']));
        }
        abort('403');
    }

    public function showAttendance($groupId)
    {
        $group = Group::with(['students', 'program', 'users'])
            ->where('id', '=', $groupId)
            ->first();

        if (empty($group)) {
            abort('404');
        }
        if ($this->userCan('view-group', $group)) {
            return view('pageAdmin.group.tab-log', compact('group'));
        }
        abort('403');
    }

    public function delete($id)
    {
        if ($this->userCan('crud-group', Session::get('centerId'))) {
            $group = Group::with('students')
                ->where('id', '=', $id)
                ->where('center_id', '=', Session::get('centerId'))
                ->first();
            if (empty($group)) {
                abort(404);
            }
            return view('pageAdmin.group.delete', compact('group'));

        }
        abort('403');
    }

    public function destroy($id)
    {
        if ($this->userCan('crud-group', Session::get('centerId'))) {
            $group = Group::with('students')
                ->where('id', '=', $id)
                ->where('center_id', '=', Session::get('centerId'))
                ->first();
            if (empty($group)) {
                abort(404);
            }
            $group->delete();
            Session::flash('success', __('language.Delete_Group_Success'));
            return redirect()->route('group.list');
        }
        abort('403');
    }

    public function editGroup($id)
    {

        if ($this->userCan('crud-group', Session::get('centerId'))) {

            $group = Group::where('center_id', '=', Session::get('centerId'))
                ->find($id);
            if (empty($group)) {
                abort('404');
            }
            $programs = Program::all();
            return view('pageAdmin.group.edit', compact('group', 'programs'));
        }
        abort('403');
    }

    public function update(Request $request, $id)
    {
        if ($this->userCan('crud-group', Session::get('centerId'))) {
            $group = Group::where('center_id', '=', Session::get('centerId'))
                ->find($id);

            if (empty($group)) {
                abort('404');
            }

            // Validator group id
            $group_id = $group->group_id;
            if ($request->group_id !== $group_id) {
                {
                    $this->validate($request, [
                        'group_id' => 'required|unique:groups,group_id|min:3|max:50',
                    ],
                        [
                            'group_id.required' => __('language.group_id_required'),
                            'group_id.unique' => __('language.group_id_name'),
                            'group_id.min' => __('language.group_id_min'),
                            'group_id.max' => __('language.group_id_max'),
                        ]);
                    $group->group_id = $request->group_id;
                }
            }

            // Validator expected_end_date_group
            $expectedEndDate = $group->expected_end_date_group;
            if ($request->expected_end_date_group !== $expectedEndDate) {
                {
                    $this->validate($request, [
                        'expected_end_date_group' => 'date|after:startdate_group',
                    ],
                        [
                            'enddate_group.after' => __('language.enddate_group_date'),
                        ]);
                    $group->expected_end_date_group = Carbon::createFromFormat('d-m-Y', $request->input('expected_end_date_group'))->format('Y-m-d');
                }
            }
            $group->program_id = $request->program_id;

            // Validator expected_end_date_group
            $endDate = $group->enddate_group;
            if ($request->enddate_group !== $endDate) {
                {
                    $this->validate($request, [
                        'enddate_group' => 'date|after:startdate_group',
                    ],
                        [
                            'enddate_group.after' => __('language.enddate_group_date'),
                        ]);
                    $group->enddate_group = Carbon::createFromFormat('d-m-Y', $request->input('enddate_group'))->format('Y-m-d');
                }
            }
            $group->note = $request->note;

            $group->save();

            Session::flash('success', __('language.Group_Edit_Success'));
            return redirect()->route('group.list');

        }
        abort('403');
    }

    public function searchGroup(Request $request)
    {
        if ($this->userCan('crud-group', Session::get('centerId'))) {
            $keyword = $request->searchGroup;
            if (empty($keyword)) {
                Session::flash('one-error', __('language.Null_Search'));
                return redirect()->route('group.list');
            } else {
                $groups = $groups = Group::where('group_id', 'LIKE', '%' . $keyword . '%')
                    ->where('center_id', '=', Session::get('centerId'))
                    ->paginate(20);
                if ($groups->count() > 0) {
                    Session::flash('have-result', __('language.have_result_1') . $groups->count() . __('language.have_result_2') . $keyword);
                    return view('pageAdmin.group.list', compact('groups'));
                } else {
                    Session::flash('no-result', __('language.No_Search') . $keyword);
                    return redirect()->route('group.list');
                }
            }
        }
        abort('403');
    }

    public function searchStudent($keyword)
    {
        if ($this->userCan('crud-group', Session::get('centerId'))) {
            $students = Student::with('group')
                ->where('center_id', Session::get('centerId'))
                ->Where('last_name', 'LIKE', '%' . $keyword . '%')
                ->orderBy('last_name')
                ->paginate(10);
            if ($students->count() > 0) {
                return response()->json($students);
            }
            Session::flash('one-error', __('language.No_Group'));
            return redirect()->route('group.list');
        }
        abort('403');
    }

    public function addStudent($groupId, $studentId)
    {
        if (isset($studentId)) {
            $student = Student::where('center_id', '=', Session::get('centerId'))
                ->where('id', '=', $studentId)
                ->first();
            if ($student->count() > 0) {
                $student->group_id = $groupId;
                $student->save();
            } else abort(404);

        } else abort(404);
    }

    public function addTeacher($groupId, $teacherId, $role)
    {
        if ($this->userCan('crud-group', Session::get('centerId'))) {
            if (isset($teacherId)) {
                $teacher = User::where('id', $teacherId)->first();
                $group = Group::where('id', $groupId)->first();
                if ($role == RoleConstants::ROLE_COACH) {
                    $group->users()->attach($teacher, ['role' => 1]);
                } elseif ($role == RoleConstants::ROLE_TUTOR) {
                    $group->users()->attach($teacher, ['role' => 2]);
                }
            } else abort(500);
            return redirect()->back();
        } else abort('403');
    }

    public function deleteTeacher($groupId, $teacherId)
    {
        if ($this->userCan('crud-group', Session::get('centerId'))) {
            if (isset($teacherId)) {
                $teacher = User::find($teacherId);
                $group = Group::find($groupId);
                $teacher->groups()->detach($group);
            } else abort(404);
            return redirect()->back();
        } else abort('403');
    }

    public function addLog(LogRequest $request, $group_id)
    {
        $group = Group::find($group_id);
        if ($group) {
            if ($this->userCan('add-group-log', $group)) {
                $log = new GroupLog();
                $log->user_id = Auth::id();
                $log->group_id = $group_id;
                $log->log = $request->input("log");
                $log->save();
                Session::flash('success', __('language.Group_Log_Added'));
                return redirect()->back();
            } else abort('403');
        } else {
            abort("404");
        }
    }

    public function removeLog(Request $request, $group_id, $log_id)
    {
        $group = Group::find($group_id);
        if ($group) {
            if ($this->userCan('add-group-log', $group)) {
                $log = GroupLog::find($log_id);
                if ($log) {
                    $log->delete();
                    return response()->json([]);
                } else {
                    abort("404");
                }
            } else abort('403');
        } else {
            abort("404");
        }
    }

    public function updateLog(Request $request, $group_id, $log_id)
    {
        $group = Group::find($group_id);
        if ($group) {
            if ($this->userCan('add-group-log', $group)) {
                $log = GroupLog::find($log_id);
                if ($log) {
                    $log->log = $request->input('value');
                    $log->save();
                    return response()->json([]);
                } else {
                    abort("404");
                }
            } else abort('403');
        } else {
            abort("404");
        }
    }
}
