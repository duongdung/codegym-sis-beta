<?php

namespace App\Http\Controllers;

use App\Criteria;
use App\GroupCriteria;
use App\Http\Requests\GroupCriteriaRequest;
use App\Report;
use Illuminate\Http\Request;

class GroupCriteriaController extends Controller
{
    public function create($reportId){

        if ($this->userCan('crud-report')) {
            $reports  = Report::find($reportId);
           if ($reports){
               return view('pageAdmin.criteria_group.add',compact('reports'));
           }else{
               abort('404');
           }
        }else{
            abort('403');
        }

    }
    public function store(GroupCriteriaRequest $request ,$reportId){

        if ($this->userCan('crud-report')) {

            $reports  = Report::find($reportId);
            if ($reports){

                $criteria = new GroupCriteria();
                $criteria->name = $request->input('name');
                $criteria->report_id = $reportId;
                $criteria->description = $request->input('description');
                $criteria->save();

                return redirect()->route('report.detail',$reportId);
            }else{
                abort('404');
            }
        }else{
            abort('403');
        }

    }

    public function updateName(Request $request,$reportId,$group_criteriaId){
        $group_criteria = GroupCriteria::with('report')
                        ->where('report_id','=',$reportId)
                        ->find($group_criteriaId);
        if ($group_criteria){
            if ($this->userCan('crud-report')) {
                $group_criteria->name = $request->input('value');
                $group_criteria->save();
                return response()->json([]);
            }else{
                abort('403');
            }
        }else{
            abort('404');
        }

    }
    public function updateDescription(Request $request,$reportId,$group_criteriaId){
        $group_criteria = GroupCriteria::with('report')
            ->where('report_id','=',$reportId)
            ->find($group_criteriaId);
        if ($group_criteria){
            if ($this->userCan('crud-report')) {
                $group_criteria->description = $request->input('value');
                $group_criteria->save();
                return response()->json([]);
            }else{
                abort('403');
            }
        }else{
            abort('404');
        }

    }

    public function destroy($reportId,$group_criteriaId){
        if ($this->userCan('crud-report')) {
            $group_criteria = GroupCriteria::with('report')
                ->where('report_id','=',$reportId)
                ->find($group_criteriaId);
            if ($group_criteria){
                $group_criteria->delete();
                return redirect()->back();
            }else{
                abort('404');
            }
        }else{
            abort('403');
        }

    }

}
