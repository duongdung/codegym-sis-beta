<?php

namespace App\Http\Controllers;

use App\Center;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\UserRequest;
use App\Student;
use Illuminate\Support\Facades\Auth;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use phpDocumentor\Reflection\Types\Null_;

class UserController extends Controller
{
    public function create()
    {
        if ($this->userCan('crud-user', Session::get('centerId'))) {
            return view('pageAdmin.user.add');
        }
        abort('403');
    }

    public function store(UserRequest $request)
    {
        if ($this->userCan('crud-user', Session::get('centerId'))) {

            $user = new User();
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));

            $roles = $request->input('roles');
            if (in_array(RoleConstants::ROLE_HQMANAGER, $roles) || in_array(RoleConstants::ROLE_HQAAO, $roles)) {
                $user->center_id = Null;
            } else {
                $user->center_id = Session::get('centerId');
            }
            $user->save();

            foreach ($roles as $key => $value) {
                $user->assignRole($value);
            }
            Session::flash('success', __('language.Add_Success'));
            return redirect()->route('user.list');
        }
        abort('403');
    }

    public function showList()
    {
        if ($this->userCan('crud-user', Session::get('centerId'))) {
            $usersManager = User::where('center_id', '=', null)
                ->orWhereHas('roles', function ($query) {
                    $query->where('id', '=', RoleConstants::ROLE_CENTERMANAGER)
                          ->where('center_id', '=', Session::get('centerId'));
                })
                ->get();
            $usersManagerCenter = User::whereHas('roles', function ($query) {
                $query->where('id', '=', RoleConstants::ROLE_CENTERMANAGER);
            })
                ->where('center_id', '=', Session::get('centerId'))->get();
            $usersTeacher = User::whereHas('roles', function ($query) {
                $query->where('id', '!=', RoleConstants::ROLE_STUDENT)
                      ->where('id', '!=', RoleConstants::ROLE_CENTERMANAGER);
            })
                ->where('center_id', '=', Session::get('centerId'))->get();

            if ($this->userCan('crud-all-user') || $this->userCan('crud-senior-user')) {
                return view('pageAdmin.user.list', compact('usersManager', 'usersTeacher'));
            }
            if ($this->userCan('crud-local-user') || $this->userCan('crud-user-aao', Session::get('centerId')) || $this->userCan('crud-user-teacher', Session::get('centerId'))) {
                return view('pageAdmin.user.list', compact('usersManagerCenter', 'usersTeacher'));
            }
        } else {
            abort('403');
        }
    }

    public function searchTeacher($keyword)
    {
        if ($this->userCan('crud-group', Session::get('centerId'))) {
            $teacher = User::with('roles')
                ->whereHas('roles', function ($q) {
                    $q->where('roles.id', RoleConstants::ROLE_COACH)
                        ->orwhere('roles.id', RoleConstants::ROLE_TUTOR);
                })
                ->where('center_id', '=', Session::get('centerId'))
                ->where('last_name', 'LIKE', '%' . $keyword . '%')
                ->get();
            return response()->json(['teacher' => $teacher]);
        }
        abort('403');
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user_login = Auth::user();
        if ($user) {
            if ($this->userCan('crud-user', Session::get('centerId'))) {
                $admin = 1;
                if ($user->id == $admin) {
                    abort('403');
                }
                //khong duoc xoa nguoi dang dang nhap
                if ($user->id == $user_login->id) {
                    abort('403');
                }
                if ($this->hashUser($user, $user_login)) {
                    return view('pageAdmin.user.delete', compact('user'));
                } else {
                    return view('pageAdmin.user.no-delete', compact('user'));
                }
            }
            abort('403');
        } else {
            abort('404');
        }

    }

    public function hashUser($user, $user_login)
    {
        if ($user_login->hasRole(RoleConstants::ROLE_ADMIN)) {
            return true;
        }
        if ($user_login->hasRole(RoleConstants::ROLE_HQMANAGER)) {
            if ($user->hasRole(RoleConstants::ROLE_ADMIN)
                || $user->hasRole(RoleConstants::ROLE_HQMANAGER)
            ) {
                return false;
            }
        } elseif ($user_login->hasRole(RoleConstants::ROLE_HQAAO)) {
            if ($user->hasRole(RoleConstants::ROLE_ADMIN)
                || $user->hasRole(RoleConstants::ROLE_HQMANAGER)
                || $user->hasRole(RoleConstants::ROLE_HQAAO)
            ) {
                return false;
            }
        } elseif ($user_login->hasRole(RoleConstants::ROLE_CENTERMANAGER)) {
            if ($user->hasRole(RoleConstants::ROLE_CENTERMANAGER)) {
                return false;
            }
            if ($user->center_id !== Session::get('centerId')) {
                return false;
            }
        } elseif ($user_login->hasRole(RoleConstants::ROLE_AAO)) {
            if ($user->hasRole(RoleConstants::ROLE_CENTERMANAGER)
                || $user->hasRole(RoleConstants::ROLE_AAO)
            ) {
                return false;
            }
            if ($user->center_id !== Session::get('centerId')) {
                return false;
            }
        }
        if ($user_login->hasRole(RoleConstants::ROLE_COACH) || $user_login->hasRole(RoleConstants::ROLE_STUDENT) || $user_login->hasRole(RoleConstants::ROLE_TUTOR)) {
            return false;
        }
        return true;
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user_login = Auth::user();
        if ($user) {
            if ($this->userCan('crud-user', Session::get('centerId'))) {
                $admin = 1;
                if ($user->id == $admin) {
                    abort('403');
                }
                //khong duoc xoa nguoi dang dang nhap
                if ($user->id == $user_login->id) {
                    abort('403');
                }
                $this->hashUser($user, $user_login);

                $user->groups()->detach();
                $user->roles()->detach();
                $student = Student::find($user->email);
                if ($student) {
                    $student->delete();
                }
                $user->delete();
                Session::flash('success', __('language.delete_user_Success'));
                return redirect()->route('user.list');
            }
            abort('403');
        } else {
            abort('404');
        }

    }

    public function detail($id)
    {
        $user = User::find($id);
        $user_login = Auth::user();
        if ($user) {
            if ($this->userCan('crud-user', Session::get('centerId'))) {
                $this->hashUser($user, $user_login);
                return view('pageAdmin.user.detail', compact('user'));
            } else {
                abort('403');
            }
        } else {
            abort('404');
        }
    }

    public function update($id)
    {
        $user = User::find($id);
        $user_login = Auth::user();
        if ($user) {
            if ($this->userCan('crud-user', Session::get('centerId'))) {
                $this->hashUser($user, $user_login);
                return view('pageAdmin.user.edit', compact('user'));
            } else {
                abort('403');
            }
        } else {
            abort('404');
        }
    }

    public function edit(Request $request, $id)
    {
        $user = User::find($id);
        $user_login = Auth::user();
        if ($user) {
            if ($this->userCan('crud-user', Session::get('centerId'))) {
                $this->hashUser($user, $user_login);
                if ($request->email !== $user->email) {
                    $this->validate($request, [
                        'email' => 'required|unique:users,email',
                    ], [
                        'email.required' => __('language.email_required'),
                        'email.unique' => __('language.email_unique'),
                    ]);
                    $user->email = $request->email;
                }

                $this->validate($request, [
                    'first_name' => 'required|min:1|max:30',
                    'last_name' => 'required|min:1|max:30',
                ], [
                    'first_name.required' => __('language.full_name_required'),
                    'first_name.min' => __('language.full_name_min'),
                    'first_name.max' => __('language.full_name_max'),

                    'last_name.required' => __('language.full_name_required'),
                    'last_name.min' => __('language.full_name_min'),
                    'last_name.max' => __('language.full_name_max'),
                ]);

                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->save();

                foreach ($user->roles as $role) {
                    $user->removeRole($role);
                }
                $roles = $request->input('roles');
                foreach ($roles as $key => $value) {
                    $user->assignRole($value);
                }
                Session::flash('success', __('language.edit_success'));
                return redirect()->route('user.detail', $user->id);

            } else {
                abort('403');
            }
        } else {
            abort('404');
        }
    }

}

