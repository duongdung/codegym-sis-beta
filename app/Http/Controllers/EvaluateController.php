<?php

namespace App\Http\Controllers;

use App\Criteria;
use App\EvaluateStudent;
use Illuminate\Contracts\Encryption\EncryptException;
use Illuminate\Http\Request;
use App\Group;
use App\Student;
use App\GroupCriteria;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Report;
use Mockery\Exception;

class EvaluateController extends Controller
{
    public function create($studentId = null ,$groupId = null){

        $groupId    = $_GET['group'];
        $group = Group::where('center_id', '=', Session::get('centerId'))->find($groupId);
        if ($group){
            if ($this->userCan('crud-capacity',$group)){
                $studentId  = $_GET['student'];

                $student    =  $this->checkStudent($studentId,$groupId);
                $evaluates  = EvaluateStudent::with('student')->where('student_id','=',$studentId)
                    ->get();
                if(count($evaluates) > 0){
                    $report_id =$evaluates->first()->report_id;
                    $groupsCriteria = GroupCriteria::where('report_id','=',$report_id)->get();
                }else{
                    $reports = Report::all();
                    if (count($reports) > 0){
                        if(!Session::has('reportId')){
                            Session::put('reportId',Report::first()->id);
                        }
                    }else{
                        abort('404');
                    }
                    $groupsCriteria = GroupCriteria::where('report_id','=',Session::get('reportId'))->get();
                }
                return view('pageAdmin.capacity.add',
                    compact( 'reports','groupsCriteria','student','groupId','evaluates','group'));
            }else{
                abort('403');
            }
        }



    }

    public function store(Request $request,$studentId = null ,$groupId = null,$reportId = null){

        $groupId    = $_GET['group'];
        $group = Group::where('center_id', '=', Session::get('centerId'))->find($groupId);
        if ($group){
            if ($this->userCan('crud-capacity',$group)) {
                $studentId  =   $_GET['student'];
                $reportId   =   $_GET['report'];
                $this->checkStudent($studentId, $groupId);
                $evaluate = new EvaluateStudent();
                $evaluate->student_id = $studentId;
                $evaluate->report_id  =  $reportId;
                $evaluate->user_id    = Auth::id();
                $evaluate->save();
                $evaluateStudent = EvaluateStudent::find($evaluate->id);
                $capacities = $request->input('status');
                foreach($capacities as $key => $criteria) {
                    $evaluateStudent->criterias()->attach($key, ['capacity' => $criteria]);
                }
                Session::flash('success-evaluate', __('language.create_evaluate_success'));
                return redirect()->route('student.capacity',[$studentId,$groupId]);
            }else{
                abort('403');
            }
        }else{
            abort('404');
        }



    }

    public function checkStudent($studentId , $groupId){
        if ($studentId && $groupId){
            if ($this->userCan('export-capacity',Session::get('centerId'))){
                $Group  = Group::with('students')
                    ->where('center_id', '=', Session::get('centerId'))
                    ->find($groupId);
            }else{
                $Group = Group::where('center_id', '=', Session::get('centerId'))
                    ->whereHas('users', function ($q) {
                        $q->where('users.id', Auth::id());
                    })->find($groupId);
            }
            $student = Student::with('group')
                    ->where('group_id','=',$groupId)
                    ->find($studentId);

            if ($student && $Group){
                return $student;
            }else{

                abort('404');
            }
        }else{
            abort('404');
        }

    }

    public function detail($id,$studentId = null,$groupId =null){
        $groupId = $_GET['group'];
        $group = Group::where('center_id','=',Session::get('centerId'))->find($groupId);

        if ($group){
            if ($this->userCan('crud-capacity',$group)) {
                $studentId= $_GET['student'];
                $evaluate = $this->hashEvaluation($id,$studentId);
                return view('pageAdmin.capacity.detail',compact('evaluate','group'));
            }else{
                abort('403');
            }
        }else{
            abort('404');
        }

    }

    public function update($evaluateId,$studentId = null,$groupId =null){
        $groupId = $_GET['group'];
        $group = Group::where('center_id','=',Session::get('centerId'))->find($groupId);
        if ($group){
            if ($this->userCan('crud-capacity',$group)) {
                $studentId= $_GET['student'];
                $evaluate = $this->hashEvaluation($evaluateId,$studentId);
                return view('pageAdmin.capacity.edit',compact('evaluate','group'));
            }else{
                abort('403');
            }
        }else{
            abort('404');
        }

    }

    public function edit(Request $request,$evaluateId,$studentId = null,$groupId =null){
        $groupId = $_GET['group'];
        $group = Group::where('center_id','=',Session::get('centerId'))->find($groupId);
        if ($group){
            if ($this->userCan('crud-capacity',$group)) {
                $studentId= $_GET['student'];
                $evaluate = $this->hashEvaluation($evaluateId,$studentId);
                $capacities = $request->input('status');
                foreach ($capacities as $key => $capacity){
                    $evaluate->criterias()->updateExistingPivot($key,['capacity'=>$capacity]);
                }
                Session::flash('success-evaluate', __('language.edit_evaluate_success'));
                return redirect()->route('evaluate.detail',[$evaluate->id,'student'=>$studentId,'group'=>$groupId]);
            }else{
                abort('403');
            }

        }else{
            abort('404');
        }

    }

    public function hashEvaluation($evaluateId,$studentId){

        if ($studentId && $evaluateId ){
            $evaluate = EvaluateStudent::with('student')->where('student_id','=',$studentId)->find($evaluateId);
            if ($evaluate){
                return $evaluate;
            }else{
                abort('404');
            }

        }
        abort('404');
    }

    public function showEvaluation($studentId,$groupId){

        $group = Group::where('center_id','=',Session::get('centerId'))->find($groupId);
        if ($group){
            if ($this->userCan('crud-capacity',$group)){
                $student = Student::with('center')
                    ->where('center_id','=',Session::get('centerId'))
                    ->find($studentId);
                if ($student){
                    $evaluations    = EvaluateStudent::with('student')
                        ->where('student_id','=',$studentId)
                        ->get();
                    if (count($evaluations) > 0){
                        $evaluation_ids = $evaluations->map(function ($evaluate){
                            return $evaluate->id;
                        });
                        $criterias = Criteria::with(['evaluates'=>function($q) use ($evaluation_ids){
                            $q->whereIn('evaluate_id',$evaluation_ids);
                        }])->get();
                        $evaluation_count = $evaluations->count();
                        return view('pageAdmin.capacity.result',compact('student','criterias','evaluations','evaluation_count','group'));
                    }else{
                        abort('404');
                    }

                }else{
                    abort('404');
                }
            }else{
                abort('403');
            }
        }else{
            abort('404');
        }


    }

    public function exportData($studentId,$groupId){
        if ($this->userCan('export-capacity',Session::get('centerId'))){
            $student = Student::with('center')
                ->where('center_id','=',Session::get('centerId'))
                ->find($studentId);
            if ($student){
                $evaluations    = EvaluateStudent::with('student')
                    ->where('student_id','=',$studentId)
                    ->get();
                if (count($evaluations) > 0){
                    $evaluation_ids = $evaluations->map(function ($evaluate){
                        return $evaluate->id;
                    });
                    $criterias = Criteria::with(['evaluates'=>function($q) use ($evaluation_ids){
                        $q->whereIn('evaluate_id',$evaluation_ids);
                    }])->get();
                    $evaluation_count = $evaluations->count();

//                    layout export

                    $phpWord = new \PhpOffice\PhpWord\PhpWord();

                    $section = $phpWord->addSection();
                    $titleReport = "CODEGYM CAREERPERFORMANCE REPORT ";

                    $section->addText($titleReport, array('name' => 'Tahoma', 'size' => 20,'bold' => true));
                    $nameStudent = $student->first_name . $student->last_name ;
                    $section->addText(htmlspecialchars('Học viên: '.$nameStudent));

                    $birthdayStudent =$student->birthday->format('Y-m-d');
                    $section->addText(htmlspecialchars('Ngày sinh: '.$birthdayStudent));
                    $householdStudent = $student->household;
                    $section->addText(htmlspecialchars('Địa chỉ: '.$householdStudent));
                    $startSchool = $student->start_school->format('Y-m-d');
                    $section->addText(htmlspecialchars('Thời gian nhập học: '.$startSchool));
                    $endSchool = $student->actual_end_date->format('Y-m-d');
                    $section->addText(htmlspecialchars('Thời gian tốt nghiệp: '.$endSchool));


                    $phpWord->addNumberingStyle(
                        'multilevel',
                        array(
                            'type' => 'multilevel',
                            'levels' => array(
                                array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
                                array('format' => 'upperLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720),
                            )
                        )
                    );
                    $section->addListItem('ĐÁNH GIÁ CHUNG', 0, array('bold'=>true), 'multilevel');
                    $section->addListItem('MỨC ĐỘ ĐÁNH GIÁ', 1, null, 'multilevel');
                    $section->addListItem('Xuất sắc', 2, null, 'multilevel');
                    $section->addListItem('Rất tốt ', 2, null, 'multilevel');
                    $section->addListItem('Đạt ', 2, null, 'multilevel');
                    $section->addListItem('Chưa dạt ', 2, null, 'multilevel');
                    $section->addListItem('Chưa được học ', 2, null, 'multilevel');
                    $section->addListItem('NHẬN XÉT CHUNG', 1, null, 'multilevel');
                    $section->addListItem('CHI TIẾT', 0, array('bold'=>true), 'multilevel');

                    $tableStyle = array(
                        'borderSize'  => 6,
                        'cellMargin'  => 50
                    );

                    $phpWord->addTableStyle('myTable', $tableStyle);
                    $table = $section->addTable('myTable');


                    $cellRowSpan = array('vMerge' => 'restart');
                    $cellRowContinue = array('vMerge' => 'continue');
                    $cellColSpan = array('gridSpan' => $evaluation_count);

                    $table->addRow();
                    $table->addCell(2000, $cellRowSpan)->addText("STT",array('bold'=>true));
                    $table->addCell(2000, $cellRowSpan)->addText("Năng Lực",array('bold'=>true));
                    $table->addCell(4000, $cellColSpan)->addText("Tiêu chí/Đánh giá",array('bold'=>true));

                    $table->addRow();
                    $table->addCell(null, $cellRowContinue);
                    $table->addCell(null, $cellRowContinue);

                    foreach ($evaluations as $index=> $evaluation){
                        $table->addCell(2000)->addText("Kỳ ". ++$index);
                    }
                    foreach ($criterias as $key=> $criteria){
                        $table->addRow();
                        $table->addCell(500)->addText(++$key);
                        $table->addCell(2000)->addText(htmlspecialchars($criteria->name));
                        foreach ($criteria->evaluates as $evaluate){
                            if ($evaluate->pivot->capacity ==0){
                                $table->addCell(500)->addText(htmlspecialchars('N/A'));
                            }elseif($evaluate->pivot->capacity ==1){
                                $table->addCell(500)->addText(htmlspecialchars('Không Đạt'));
                            }elseif($evaluate->pivot->capacity ==2){
                                $table->addCell(500)->addText(htmlspecialchars('Đạt'));
                            }elseif($evaluate->pivot->capacity ==2){
                                $table->addCell(500)->addText(htmlspecialchars('Tốt'));
                            }else{
                                $table->addCell(500)->addText(htmlspecialchars('Xuất Sắc'));
                            }
                        }
                    }
                    $object = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord,'Word2007');
                    $object->save(storage_path('CodeGym Career_Performance Report.docx'));
                    return response()->download(storage_path('CodeGym Career_Performance Report.docx'));

                }else{
                    abort('404');
                }

            }else{
                abort('404');
            }
        }
        else{
            abort('403');
        }
    }

    public function delete($evaluateId, $studentId = null,$groupId = null){
        $groupId = $_GET['group'];
        $group = Group::where('center_id','=',Session::get('centerId'))->find($groupId);
        if ($group){
            if ($this->userCan('crud-capacity',$group)){
                $studentId = $_GET['student'];
                $evaluate = $this->hashEvaluation($evaluateId,$studentId);
                return view('pageAdmin.capacity.delete',compact('evaluate','group'));
            }else{
                abort('403');
            }
        }else{
            abort('404');
        }

    }
    public function destroy($evaluateId, $studentId = null,$groupId = null){

        $groupId = $_GET['group'];
        $group = Group::where('center_id','=',Session::get('centerId'))->find($groupId);
        if ($group){
            if ($this->userCan('crud-capacity',$group)){
                $studentId = $_GET['student'];
                $evaluate = $this->hashEvaluation($evaluateId,$studentId);
                $evaluate->delete();
                $evaluate->criterias()->detach();
                Session::flash('success-evaluate', __('language.Delete_Success'));
                return redirect()->route('student.capacity', [$studentId,$groupId]) ;
            }else{
                abort('403');
            }
        }else{
            abort('404');
        }

    }

}
