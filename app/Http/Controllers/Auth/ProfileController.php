<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{
    public function detail(){
        return view('auth.profile.profile');
    }
    public function update()
    {
        return view('auth.profile.edit');
    }

    public function edit(Request $request)
    {
        $me = Auth::user();
            {
                $this->validate($request, [
                    'first_name' => 'required|min:1|max:30',
                    'last_name' => 'required|min:1|max:30',
                ],
                    [
                        'first_name.required' => __('language.full_name_required'),
                        'first_name.min' => __('language.full_name_min'),
                        'first_name.max' => __('language.full_name_max'),
                        'last_name.required' => __('language.full_name_required'),
                        'last_name.min' => __('language.full_name_min'),
                        'last_name.max' => __('language.full_name_max'),
                    ]);
                $me->first_name = $request->input('first_name');
                $me->last_name = $request->input('last_name');
        }
        $me->save();
        Session::flash('success', __('language.edit_success'));
        return redirect()->route('me.profile');
    }
}