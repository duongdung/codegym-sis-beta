<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class ChangePasswordController extends Controller
{
    public function update()
    {
        return view('auth.passwords.change-pass');
    }

    public function edit(ChangePasswordRequest $request)
    {

        $user = Auth::user();
        $hashedPassword = $user->password;

        $passold = $request->input('pass_old');
        $passnew = $request->input('pass_new');

        if (Hash::check($passold, $hashedPassword)) {
            $user->password = Hash::make($passnew);
            Session::flash('success', __('language.changePass_Success'));
            $user->save();
            return back();
        }
    }

}