<?php

namespace App\Http\Controllers;
use App\CategoryCriteria;
use App\Criteria;
use App\Http\Requests\CriteriaRequest;

class CriteriaController extends Controller
{

    public function create($reportId, $groupCriteriaId = null,$categoryCriteriaId = null){
        if ($this->userCan('crud-report')) {
            $groupCriteriaId = $_GET['group-criteria'];
            $categoryCriteriaId = $_GET['category-criteria'];
            $categoryCriteria = $this->hashCategoryCriteria($reportId,$groupCriteriaId,$categoryCriteriaId);
            return view('pageAdmin.criteria.add', compact('categoryCriteria', 'reportId'));
        }abort('403');
    }
    public function store(CriteriaRequest $request,$reportId, $groupCriteriaId = null,$categoryCriteriaId = null){

        if ($this->userCan('crud-report')) {

            $groupCriteriaId = $_GET['group-criteria'];
            $categoryCriteriaId = $_GET['category-criteria'];
            $categoryCriteria = $this->hashCategoryCriteria($reportId,$groupCriteriaId,$categoryCriteriaId);
            $criteria = new Criteria();
            $criteria->name = $request->input('name');
            $criteria->category_criteria_id = $categoryCriteria->id;
            $criteria->description = $request->description;
            $criteria->save();
            return redirect()->route('report.detail', $reportId);

        }abort('403');
    }
    public function hashCategoryCriteria($reportId,$groupCriteriaId,$categoryCriteriaId){

        if($reportId && $groupCriteriaId && $categoryCriteriaId ){

            $categoryCriteria = CategoryCriteria::with('groupCriteria')->where('group_criteria_id','=',$groupCriteriaId)->find($categoryCriteriaId);
            if ($categoryCriteria){
                $report_ids = $categoryCriteria->groupCriteria->report_id;
                if ($report_ids == $reportId){
                    return $categoryCriteria;
                }
                abort('404');
            }
            abort('404');
        }
        else{
            abort('404');
        }

    }
    public function updateName(CriteriaRequest $request,$reportId,$groupCriteriaId,$categoryCriteriaId,$criteriaId){
        if ($this->userCan('crud-report')) {
            $categoryCriteria = $this->hashCategoryCriteria($reportId,$groupCriteriaId,$categoryCriteriaId);
            $criteria = Criteria::with('category_criteria')->where('category_criteria_id','=',$categoryCriteriaId)->find($criteriaId);
            if ($criteria){
                $criteria->name = $request->input('value');
                $criteria->save();
                return response()->json([]);
            }else{
                abort('404');
            }
        }else{
            abort('403');
        }
    }
    public function updateDescription(CriteriaRequest $request,$reportId,$groupCriteriaId,$categoryCriteriaId,$criteriaId){
        if ($this->userCan('crud-report')) {
            $categoryCriteria = $this->hashCategoryCriteria($reportId,$groupCriteriaId,$categoryCriteriaId);
            $criteria = Criteria::with('category_criteria')->where('category_criteria_id','=',$categoryCriteriaId)->find($criteriaId);
            if ($criteria){
                $criteria->description = $request->input('value');
                $criteria->save();
                return response()->json([]);
            }else{
                abort('404');
            }
        }else{
            abort('403');
        }
    }

    public function destroy($reportId,$groupCriteriaId,$categoryCriteriaId,$criteriaId){
        if ($this->userCan('crud-report')) {
            $categoryCriteria = $this->hashCategoryCriteria($reportId,$groupCriteriaId,$categoryCriteriaId);
            $criteria = Criteria::with('category_criteria')->where('category_criteria_id','=',$categoryCriteriaId)->find($criteriaId);
            if ($criteria){
                $criteria->delete();
                return redirect()->back();
            }else{
                abort('404');
            }
        }else{
            abort('403');
        }
    }

}
