<?php

namespace App\Http\Controllers;

use App\Course;
use App\Group;
use App\Http\Requests\CourseRequest;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CourseController extends Controller
{
    public function create()
    {
        if ($this->userCan('crud-course', Session::get('centerId'))) {
            return view('pageAdmin.course.add');
        }
        abort('403');
    }

    public function store(CourseRequest $request)
    {

        if ($this->userCan('crud-course', Session::get('centerId'))) {
            $course = new Course();
            $course->course_code = $request->input('course_code');
            $course->course_name = $request->input('course_name');
            $course->description = $request->input('note');
            $course->save();
            Session::flash('success', __('language.Add_Success'));
            return redirect()->route('course.show');
        }
        abort('403');
    }

    public function show()
    {
        if ($this->userCan('crud-course', Session::get('centerId'))) {
            $courses = Course::paginate(20);
            return view('pageAdmin.course.list', compact('courses'));
        }
        abort('403');

    }

    public function delete($id)
    {

        if ($this->userCan('crud-course', Session::get('centerId'))) {
            $course = Course::find($id);
            if (empty($course)) {
                abort('404');
            }
            return view('pageAdmin.course.delete', compact('course'));
        }
    }

    public function destroy($id)
    {

        if ($this->userCan('crud-course', Session::get('centerId'))) {
            $courses = Course::find($id);
            if (empty($courses)) {
                abort('404');
            }
            $courses->delete();
            Session::flash('success', __('language.Success_delete'));
            return redirect()->route('course.show');
        }

    }

    public function edit($id)
    {
        if ($this->userCan('crud-course', Session::get('centerId'))) {
            $course = Course::find($id);
            if (empty($course)) {
                abort('404');
            }
            return view('pageAdmin.course.edit', compact('course'));
        }
        abort('403');
    }

    public function update(Request $request, $id)
    {
        if ($this->userCan('crud-course', Session::get('centerId'))) {
            $course = Course::find($id);
            if (empty($course)) {
                abort('404');
            }

            // Validator course_code
            $course_code = $course->course_code;
            if ($request->course_code !== $course_code) {
                {
                    $this->validate($request, [
                        'course_code'   =>'required|min:3|max:10|unique:courses,course_code',
                    ],
                        [
                            'course_code.required'   => __('language.course_code_required') ,
                            'course_code.min'        => __('language.course_code_min') ,
                            'course_code.max'        => __('language.course_code_max') ,
                            'course_code.unique'     => __('language.course_code_unique') ,
                        ]);
                    $course->course_code = $request->course_code;
                }
            }

            // Validator course_name
            $course_name = $course->course_name;
            if ($request->course_name !== $course_name) {
                {
                    $this->validate($request, [
                        'course_name'   =>'required|min:3|max:30|unique:courses,course_name',
                    ],
                        [
                            'course_name.required'   => __('language.course_name_required') ,
                            'course_name.min'        => __('language.course_name_min') ,
                            'course_name.max'        => __('language.course_name_max') ,
                            'course_name.unique'     => __('language.course_name_unique') ,
                        ]);
                    $course->course_name = $request->course_name;
                }
            }
            $course->description = $request->description;
            $course->save();
            Session::flash('success', __('language.Group_Edit_Success'));
            return redirect()->route('course.show');
        }
        abort('403');
    }

}
