<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Center;

class CenterComposer
{
    public function compose(View $view)
    {
        $centers = Center::all();
        $view->with('centers', end($centers));
    }
}