<?php

namespace App\Providers;

use App\Center;
use App\Http\Controllers\RoleConstants;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
    'App\Model' => 'App\Policies\ModelPolicy',
    Center::class => CenterPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
        
        // Passport::enableImplicitGrant();

        Gate::define('oauth2', function ($user) {
            return $user->hasRole(RoleConstants::ROLE_ADMIN);
        });

//        Centers
        Gate::define('view-center', function ($user) {
            return $user->hasRole(RoleConstants::ROLE_ADMIN)
            || $user->hasRole(RoleConstants::ROLE_HQMANAGER)
            || $user->hasRole(RoleConstants::ROLE_HQAAO);
        });
        Gate::define('crud-center', function ($user) {
            return $user->hasRole(RoleConstants::ROLE_ADMIN)
            || $user->hasRole(RoleConstants::ROLE_HQMANAGER);
        });

//        Users
        Gate::define('crud-user', function ($user, $center_id) {
            if ($user->hasRole(RoleConstants::ROLE_ADMIN)
                || $user->hasRole(RoleConstants::ROLE_HQMANAGER)
                || $user->hasRole(RoleConstants::ROLE_HQAAO)
                ) {
                return true;
        }
        if ($user->hasRole(RoleConstants::ROLE_CENTERMANAGER)
            || $user->hasRole(RoleConstants::ROLE_AAO)
            ) {
            return $user->belongToCenter($center_id);
    }
    return false;
});
        Gate::define('crud-all-user', function ($user) {
            return $user->hasRole(RoleConstants::ROLE_ADMIN);
        });
        Gate::define('crud-senior-user', function ($user) {
            return $user->hasRole(RoleConstants::ROLE_HQMANAGER);
        });
        Gate::define('crud-local-user', function ($user) {
            return $user->hasRole(RoleConstants::ROLE_HQAAO);
        });
        Gate::define('crud-user-aao', function ($user, $center_id) {
            if ($user->hasRole(RoleConstants::ROLE_CENTERMANAGER)) {
                return $user->belongToCenter($center_id);
            }
            return false;
        });
        Gate::define('crud-user-teacher', function ($user, $center_id) {
            if ($user->hasRole(RoleConstants::ROLE_AAO)) {
                return $user->belongToCenter($center_id);
            }
            return false;
        });

//        Group
        Gate::define('crud-group', function ($user, $center_id) {
            if ($user->hasRole(RoleConstants::ROLE_ADMIN)
                || $user->hasRole(RoleConstants::ROLE_HQMANAGER)
                || $user->hasRole(RoleConstants::ROLE_HQAAO)
                ) {
                return true;
        }
        if ($user->hasRole(RoleConstants::ROLE_CENTERMANAGER)
            || $user->hasRole(RoleConstants::ROLE_AAO)
            ) {
            return $user->belongToCenter($center_id);
    }
});
        Gate::define('view-groups', function ($user) {
            return $user->hasRole(RoleConstants::ROLE_ADMIN)
            || $user->hasRole(RoleConstants::ROLE_HQMANAGER)
            || $user->hasRole(RoleConstants::ROLE_HQAAO)
            || $user->hasRole(RoleConstants::ROLE_CENTERMANAGER)
            || $user->hasRole(RoleConstants::ROLE_AAO)
            || $user->hasRole(RoleConstants::ROLE_COACH)
            || $user->hasRole(RoleConstants::ROLE_TUTOR);
        });
        Gate::define('view-group', function ($user, $group) {
            if ($user->hasRole(RoleConstants::ROLE_ADMIN)
                || $user->hasRole(RoleConstants::ROLE_HQMANAGER)
                || $user->hasRole(RoleConstants::ROLE_HQAAO)
                ) {
                return true;
        }
        if ($user->hasRole(RoleConstants::ROLE_CENTERMANAGER)
            || $user->hasRole(RoleConstants::ROLE_AAO)
            ) {
            return $user->belongToCenter($group->center_id);
    }
    if ($user->hasRole(RoleConstants::ROLE_COACH)
        || $user->hasRole(RoleConstants::ROLE_TUTOR)
        ) {
        return $group->hasUser($user);
}
return false;
});
        Gate::define('add-group-log', function ($user, $group) {
            if ($user->hasRole(RoleConstants::ROLE_ADMIN)
                || $user->hasRole(RoleConstants::ROLE_HQMANAGER)
                || $user->hasRole(RoleConstants::ROLE_HQAAO)
                ) {
                return true;
        }
        if ($user->hasRole(RoleConstants::ROLE_AAO)) {
            return $user->belongToCenter($group->center_id);
        }
        if ($user->hasRole(RoleConstants::ROLE_COACH)
            || $user->hasRole(RoleConstants::ROLE_TUTOR)
            ) {
            return $group->hasUser($user);
    }
    return false;
});

//        Student
        Gate::define('curd-student', function ($user, $center_id) {
            if ($user->hasRole(RoleConstants::ROLE_ADMIN)
                || $user->hasRole(RoleConstants::ROLE_HQMANAGER)
                || $user->hasRole(RoleConstants::ROLE_HQAAO)
                ) {
                return true;
        }
        if ($user->hasRole(RoleConstants::ROLE_CENTERMANAGER)
            || $user->hasRole(RoleConstants::ROLE_AAO)
            ) {
            return $user->belongToCenter($center_id);
    }
    return false;
});
        Gate::define('view-list-students', function ($user, $center_id) {
            if ($user->hasRole(RoleConstants::ROLE_ADMIN)
                || $user->hasRole(RoleConstants::ROLE_HQMANAGER)
                || $user->hasRole(RoleConstants::ROLE_HQAAO)
                ) {
                return true;
        }
        if ($user->hasRole(RoleConstants::ROLE_CENTERMANAGER)
            || $user->hasRole(RoleConstants::ROLE_AAO)
            ) {
            return $user->belongToCenter($center_id);
    }
    return false;
});
        Gate::define('students', function ($user, $center_id) {
            if ($user->hasRole(RoleConstants::ROLE_STUDENT)) {
                return $user->belongToCenter($center_id);
            }
            return false;
        });
        Gate::define('view-students-group', function ($user, $group) {
            if ($user->hasRole(RoleConstants::ROLE_COACH)
                || $user->hasRole(RoleConstants::ROLE_TUTOR)
                ) {
                return $group->hasUser($user);
        }
        return false;
    });
        Gate::define('create-edit-view-attendance', function ($user, $group) {
            if ($user->hasRole(RoleConstants::ROLE_ADMIN)
                || $user->hasRole(RoleConstants::ROLE_HQMANAGER)
                || $user->hasRole(RoleConstants::ROLE_HQAAO)
                ) {
                return true;
        }
        if ($user->hasRole(RoleConstants::ROLE_CENTERMANAGER)
            || $user->hasRole(RoleConstants::ROLE_AAO)
            ) {
            return $user->belongToCenter($group->center_id);
    }
    if ($user->hasRole(RoleConstants::ROLE_COACH) || $user->hasRole(RoleConstants::ROLE_TUTOR)) {
        return $group->hasUser($user);
    }
    return false;
});
        Gate::define('delete-attendance', function ($user, $center_id) {
            if ($user->hasRole(RoleConstants::ROLE_ADMIN)) {
                return true;
            }
            if ($user->hasRole(RoleConstants::ROLE_CENTERMANAGER)
                || $user->hasRole(RoleConstants::ROLE_AAO)
                ) {
                return $user->belongToCenter($center_id);
        }
        return false;
    });
        Gate::define('manager-program', function ($user) {
            return $user->hasRole(RoleConstants::ROLE_ADMIN)
            || $user->hasRole(RoleConstants::ROLE_HQMANAGER);
        });
        Gate::define('crud-report', function ($user) {
            return $user->hasRole(RoleConstants::ROLE_ADMIN)
            || $user->hasRole(RoleConstants::ROLE_HQMANAGER)
            || $user->hasRole(RoleConstants::ROLE_HQAAO);
        });
        Gate::define('add-student-log', function ($user, $student) {
            if ($user->hasRole(RoleConstants::ROLE_ADMIN)
                || $user->hasRole(RoleConstants::ROLE_HQMANAGER)
                || $user->hasRole(RoleConstants::ROLE_HQAAO)
                ) {
                return true;
        }
        if ($user->hasRole(RoleConstants::ROLE_AAO)) {
            return $user->belongToCenter($student->center_id);
        }
        if ($user->hasRole(RoleConstants::ROLE_COACH)
            || $user->hasRole(RoleConstants::ROLE_TUTOR)
            ) {
            return $student->group->hasUser($user);
    }
});

        // Course
        Gate::define('crud-course', function ($user, $center_id) {
            if ($user->hasRole(RoleConstants::ROLE_ADMIN)
                || $user->hasRole(RoleConstants::ROLE_HQAAO)
                ) {
                return true;
        }
        if ($user->hasRole(RoleConstants::ROLE_CENTERMANAGER)
            || $user->hasRole(RoleConstants::ROLE_AAO)
            ) {
            return $user->belongToCenter($center_id);
    }
    return false;
});

        //Capacity
        Gate::define('crud-capacity', function ($user, $group) {
            if ($user->hasRole(RoleConstants::ROLE_ADMIN)
                || $user->hasRole(RoleConstants::ROLE_HQAAO)
                ) {
                return true;
        }
        if ($user->hasRole(RoleConstants::ROLE_AAO)) {
            return $user->belongToCenter($group->center_id);
        }

        if ($user->hasRole(RoleConstants::ROLE_COACH)) {
            return $group->hasUser($user);
        }
        return false;
    });

        Gate::define('export-capacity', function ($user, $center_id) {
            if ($user->hasRole(RoleConstants::ROLE_ADMIN)
                || $user->hasRole(RoleConstants::ROLE_HQAAO)
                ) {
                return true;
        }
        if ($user->hasRole(RoleConstants::ROLE_AAO)) {
            return $user->belongToCenter($center_id);
        }
        return false;
    });

        // Exam Score
        Gate::define('view-student-score', function ($user, $student) {
            if ($user->hasRole(RoleConstants::ROLE_AAO)) {
                return $user->belongToCenter($student->center_id);
            }
            if ($user->hasRole(RoleConstants::ROLE_COACH)
                || $user->hasRole(RoleConstants::ROLE_TUTOR)
                ) {
                return $student->group->hasUser($user);
            }
            return false;
        });
        Gate::define('view-group-score', function ($user, $group) {
            if ($user->hasRole(RoleConstants::ROLE_AAO)) {
                return $user->belongToCenter($group->center_id);
            }
            if ($user->hasRole(RoleConstants::ROLE_COACH)
                || $user->hasRole(RoleConstants::ROLE_TUTOR)
                ) {
                return $group->hasUser($user);
        }
        return false;
    });

        // Exam Score
        Gate::define('crud-student-score', function ($user, $center_id) {
            if ($user->hasRole(RoleConstants::ROLE_AAO)) {
                return $user->belongToCenter($center_id);
            }
            return false;
        });
        Gate::define('crud-group-score', function ($user, $center_id) {
            if ($user->hasRole(RoleConstants::ROLE_AAO)) {
                return $user->belongToCenter($center_id);
            }
            return false;
        });

        //Hompage
        Gate::define('view-statistics', function ($user, $center_id) {
            if ($user->hasRole(RoleConstants::ROLE_ADMIN)
                || $user->hasRole(RoleConstants::ROLE_HQMANAGER)
                || $user->hasRole(RoleConstants::ROLE_HQAAO)) {
                return true;
        }
        if ($user->hasRole(RoleConstants::ROLE_AAO)
            || $user->hasRole(RoleConstants::ROLE_CENTERMANAGER)
            || $user->hasRole(RoleConstants::ROLE_COACH)
            || $user->hasRole(RoleConstants::ROLE_TUTOR)) {
            return $user->belongToCenter($center_id);
    }
    return false;
});
        Gate::define('view-statistics-all', function ($user) {
            return $user->hasRole(RoleConstants::ROLE_ADMIN)
            || $user->hasRole(RoleConstants::ROLE_HQMANAGER)
            || $user->hasRole(RoleConstants::ROLE_HQAAO);
        });
        Gate::define('view-statistics-center', function ($user, $center_id) {
            if ($user->hasRole(RoleConstants::ROLE_CENTERMANAGER)
                || $user->hasRole(RoleConstants::ROLE_AAO)) {
                return $user->belongToCenter($center_id);
        }
        return false;
    });
        Gate::define('view-statistics-for-teacher', function ($user, $center_id) {
            if ($user->hasRole(RoleConstants::ROLE_COACH)
                || $user->hasRole(RoleConstants::ROLE_TUTOR)
                ) {
                return $user->belongToCenter($center_id);
        }
        return false;
    });
    }
}
