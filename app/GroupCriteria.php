<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupCriteria extends Model
{

    protected $table = 'groups_criteria';
    public function report(){
        return $this->belongsTo('App\Report');
    }

    public function categories_criteria(){
        return $this->hasMany('App\CategoryCriteria');
    }
}
