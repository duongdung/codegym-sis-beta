<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $table = 'attendances';

    public function user(){
       return $this->belongsTo('App\User');
    }

    public function group(){
        return $this->belongsTo('App\Group');
    }
    public function attendancedetails(){
        return $this->hasMany('App\Attendancedetail');
    }
    public function students(){
        return $this->belongsToMany('App\Student','attendance_details');
    }

}
