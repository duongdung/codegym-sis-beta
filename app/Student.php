<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at','birthday','startdate_id','start_school','expected_end_date','actual_end_date'];

    public function group(){
        return $this->belongsTo("App\Group");
    }
    public function role(){
        return $this->belongsTo("App\Role");
    }
    public function center(){
        return $this->belongsTo("App\Center");
    }
    public function attendancedetails(){
        return $this->hasMany("App\Attendancedetail");
    }
    public function attendances(){
        return $this->belongsToMany("App\Attendance", "attendance_details")->withPivot("attendance");
    }
    public function courses()
    {
        return $this->belongsToMany('App\Course')
            ->withPivot( 'test_score')
            ->withPivot( 'group')
            ->withTimestamps();
    }
    public function evaluate(){
        return $this->belongsTo('App\EvaluateStudent');
    }
    public function exams()
    {
        return $this->belongsToMany('App\Exam')
            ->withPivot('score')
            ->withTimestamps();
    }

}
