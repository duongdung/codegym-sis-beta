<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Center extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function users(){
        return $this->hasMany('App\User');
    }
    public function students(){
        return $this->hasMany('App\Student');
    }
    public function groups(){
        return $this->hasMany('App\Group');
    }
}
