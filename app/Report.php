<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public function groups_criteria(){
        return $this->hasMany('App\GroupCriteria');
    }
    public function evaluate(){
        return $this->belongsTo('App\EvaluateStudent');
    }
}
