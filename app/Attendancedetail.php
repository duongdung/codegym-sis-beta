<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendancedetail extends Model
{
    protected $table = 'attendance_details';
    public function attendance(){
        return $this->belongsTo('App\Attendance');
    }
    public function student(){
        return $this->belongsTo('App\Student');
    }
}
