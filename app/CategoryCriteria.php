<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryCriteria extends Model
{
    protected $table = 'categories_criteria';

    public function criterias(){
        return $this->hasMany('App\Criteria');
    }
    public function groupCriteria(){
        return $this->belongsTo('App\GroupCriteria');
    }
}
