<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at', 'expected_end_date_group', 'startdate_group'];


    public function students(){
        return $this->hasMany('App\Student');
    }

    public function program(){
        return $this->belongsTo('App\Program');
    }
    public function center(){
        return $this->belongsTo('App\Center');
    }

    public function users()
    {
        return $this->belongsToMany('App\User')
            ->withPivot( 'role')
            ->withTimestamps();
    }

    public function teachers(){
        return $this->users()->where('role', '2');
    }

    public function getTeachers(){
        return $this->teachers()->get();
    }

    public function attendaces(){
        return $this->hasMany('App\Attendance');
    }

    public function logs(){
        return $this->hasMany("App\GroupLog");
    }

    public function hasUser($user){
        $users = $this->users()->get();
        foreach ($users as $u){
            if($u->id === $user->id){
                return true;
            }
        }
        return false;
    }
}

