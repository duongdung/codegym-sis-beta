<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exam extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at','date'];

    public function course()
    {
        return $this->belongsTo('App\Course');
    }

    public function students()
    {
        return $this->belongsToMany('App\Student')
                    ->withPivot('score')
                    ->withTimestamps();
    }
    public function assignStudent($student, array $score)
    {
        return $this->students()->attach($student, $score);
    }

    public function removeStudent($student)
    {
        return $this->students()->detach($student);
    }
}
