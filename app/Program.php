<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Program extends Model
{
    protected $table = "programs";
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function groups(){
        return $this->hasMany('App\Group');
    }

}
