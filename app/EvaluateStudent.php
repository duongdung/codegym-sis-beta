<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluateStudent extends Model
{
    protected $table = 'evaluate_student';
    public function student(){
        return $this->belongsTo('App\Student');
    }
    public function report(){
        return $this->belongsTo('App\Report');
    }

    public function criterias(){
        return $this->belongsToMany('App\Criteria','evaluate_criteria','evaluate_id','criteria_id')
                    ->withPivot("capacity")
                    ->withTimestamps();

    }
    public function user(){
        return $this->belongsTo('App\User');
    }
}
