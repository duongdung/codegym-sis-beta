<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name', 'email', 'password','created_at',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function roles()
    {
        return $this->belongsToMany('App\Role')->withTimestamps();
    }
    public function center(){
        return $this->belongsTo('App\Center');
    }
    public function groups()
    {
        return $this->belongsToMany('App\Group')->withTimestamps();
    }

    public function assignRole($role)
    {
        return $this->roles()->attach($role);
    }

    public function removeRole($role)
    {
        return $this->roles()->detach($role);
    }

    public function attendances(){
        return $this->hasMany('App\Attendance');
    }

    public function hasRole($role){
        foreach ($this->roles as $r){
            if($r->id === $role){
                return true;
            }
        }
        return false;
    }

    public function belongToCenter($center_id){
        $myCenter = $this->center()->first();
        if($myCenter){
            return $myCenter->id === $center_id;
        }
        return false;
    }
    public function evaluate(){
        return $this->belongsTo('App\EvaluateStudent');
    }

}
