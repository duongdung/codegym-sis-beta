<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Criteria extends Model
{
    protected $table='criterias';

    public function category_criteria(){
        return $this->belongsTo('App\CategoryCriteria');
    }

    public function evaluates(){
        return $this->belongsToMany('App\EvaluateStudent','evaluate_criteria','criteria_id','evaluate_id')
            ->withPivot("capacity")
            ->withTimestamps();
    }
}
