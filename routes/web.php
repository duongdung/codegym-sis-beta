<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use AdvanceSearch\AdvanceSearchProvider\Search;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::group(["middleware"=> "localization"],function(){

    Auth::routes();

    Route::post('/language', 'LangController@postLang')->name('postLang');

    Route::get('/', function () {
        return view('auth.login');
    });

    Route::group(['middleware' => 'auth'], function () {

        Route::get('/home', 'HomeController@index')->name('home')->middleware('student.login');

    });

    Route::get('/oauth2', function() {
        return view('pageAdmin.Oauth2.oauth2');
    })->name('oauth2.show');

    Route::post('/center','Controller@postCenter')->name('postCenter');

    Route::post('/','Controller@postReport')->name('postReport');

    // Router Student
    Route::group(['middleware' => 'auth', 'prefix' => 'students'], function () {

        Route::get('/add-new','StudentController@create')->name('student.create');

        Route::post('/add-new','StudentController@store')->name('student.store');

        Route::get('/','StudentController@showList')->name('student.list');

        Route::get('/{id}/delete','StudentController@delete')->name('student.delete');

        Route::post('/{id}/delete','StudentController@destroy')->name('student.destroy');

        Route::get('/{id}/edit','StudentController@edit')->name('student.edit');

        Route::post('/{id}/edit','StudentController@update')->name('student.update');

        Route::get('/search','StudentController@searchStudent')->name('student.search');
        Route::post('{student_id}/add-log','StudentController@addLog')->name('student.log.add');
        Route::post('{student_id}/logs/{log_id}/delete','StudentController@removeLog')->name('student.log.remove');
        Route::post('{student_id}/logs/{log_id}/update','StudentController@updateLog')->name('student.log.update');
        Route::get('/{studentId}/group/{groupId}/details','StudentController@showDetail')->name('student.details');
        Route::get('/{id}/group','StudentController@showGroup')->name('student.group');
        Route::get('/{id}/log','StudentController@showLog')->name('student.log');
        Route::get('/{studentId}/group/{groupId}/capacity','StudentController@showCapacity')->name('student.capacity');

        Route::get('/{studentId}/group/{groupId}/show-evaluate-capacity','EvaluateController@showEvaluation')->name('student.showEvaluation');

        Route::get('/{id}/score','StudentController@showTabScore')->name('student.tab.score');
        Route::get('/{student_id}/course/{course_id}/add-score','StudentController@addScore')->name('student.add.score');
        Route::post('/{student_id}/course/{course_id}/add-score','StudentController@createScore')->name('student.create.score');
        Route::get('/{student_id}/exam/{exam_id}/edit-score','StudentController@editScore')->name('student.edit.score');
        Route::post('/{student_id}/exam/{examId}/course/{course_id}/edit-score','StudentController@updateScore')->name('student.update.score');
        Route::get('/{student_id}/exam/{examId}/delete-score','StudentController@deleteScore')->name('student.delete.score');
        Route::post('/{student_id}/exam/{examId}/delete-score','StudentController@destroyScore')->name('student.destroy.score');


        Route::get('/{studentId}/group/{groupId}/export','EvaluateController@exportData')->name('exportData');
    });

    // Router group
    Route::group(['middleware'=> 'auth', 'prefix'=> 'groups'], function(){

        Route::get('/add-new', 'GroupController@create')->name('group.create');

        Route::post('/add-new', 'GroupController@store')->name('group.store');

        Route::get('/','GroupController@showList')->name('group.list');

        Route::get('/{groupId}/detail','GroupController@showDetail')->name('group.detail');
        Route::get('/{groupId}/score','GroupController@showTabScore')->name('group.tab.score');
        Route::post('/{groupId}/check-score','GroupController@checkScore')->name('group.check.score');
        Route::post('/{groupId}/score','GroupController@addScore')->name('group.add.score');
        Route::get('/{groupId}/exam','GroupController@showScore')->name('group.show.score');
        Route::get('/{groupId}/log','GroupController@showLog')->name('group.log');
        Route::get('/{groupId}/students','GroupController@showStudent')->name('group.student');
        Route::get('/{groupId}/attendance','GroupController@showAttendance')->name('group.attendance');

        Route::get('/{id}/delete','GroupController@delete')->name('group.delete');

        Route::post('/{id}/delete','GroupController@destroy')->name('group.destroy');

        Route::get('/{id}/edit','GroupController@editGroup')->name('group.edit');

        Route::post('/{id}/edit','GroupController@update')->name('group.update');

        Route::get('/search','GroupController@searchGroup')->name('group.search.group');

        Route::get('/search-student/{keyword}','GroupController@searchStudent')->name('group.search');

        Route::get('/add-student/{groupid}/{studentid}','GroupController@addStudent');

        Route::get('/delete-teacher/{groupid}/{teacherid}','GroupController@deleteTeacher')->name('group.deleteTeacher');

        Route::get('/add-teacher/{groupid}/{teacherid}/{role}','GroupController@addTeacher');
        Route::post('{group_id}/add-log','GroupController@addLog')->name('group.log.add');
        Route::post('{group_id}/logs/{log_id}/delete','GroupController@removeLog')->name('group.log.remove');
        Route::post('{group_id}/logs/{log_id}/update','GroupController@updateLog')->name('group.log.update');

        Route::get('{groupId}/add-attendance', 'AttendanceController@create')->name('attendance.create');

        Route::post('{groupId}/add-attendance','AttendanceController@store')->name('attendance.store');

        Route::get('{groupId}/attendances','AttendanceController@showlist')->name('attendance.list');

        Route::get('/{groupId}/attendance-detail/','AttendanceController@detail')->name('attendance.detail');

        Route::get('{groupId}/attendance-update','AttendanceController@update')->name('attendance.update');

        Route::post('{groupId}/attendance-update','AttendanceController@edit')->name('attendance.edit');

        Route::get('{groupId}/attendance-delete','AttendanceController@delete')->name('attendance.delete');

        Route::post('{groupId}/attendance-delete','AttendanceController@destroy')->name('attendance.destroy');

        Route::get('/{groupId}/view-five-attendance','AttendanceDetailController@showfive')->name('attendancedetail.showfive');

        Route::post('/{groupId}/attendance-details','AttendanceDetailController@showatteanDate')->name('attendancedetail.date');

    });

    // Route study program
    Route::group(['middleware' => 'auth', 'prefix'=> 'programs'], function(){

        Route::get('/add-new', 'StudyProgramController@create')->name('program.add');

        Route::post('/add-new', 'StudyProgramController@store')->name('program.store');

        Route::get('/','StudyProgramController@showList')->name('program.list');

        Route::get('/{id}/detail','StudyProgramController@showDetail')->name('program.detail');

        Route::get('/{id}/delete','StudyProgramController@delete')->name('program.delete');

        Route::post('/{id}/delete','StudyProgramController@destroy')->name('program.destroy');

        Route::get('/{id}/edit','StudyProgramController@edit')->name('program.edit');

        Route::post('/{id}/edit','StudyProgramController@update')->name('program.update');
    });

    // Route Center

    Route::group(['middleware' => 'auth', 'prefix'=> 'centers'], function (){

        Route::get('add-new', 'CenterController@create')->name('center.add');

        Route::post('add-new','CenterController@store')->name('center.store');

        Route::get('/','CenterController@showList')->name('center.list');

        Route::get('{id}/delete','CenterController@delete')->name('center.delete');

        Route::post('{id}/delete','CenterController@destroy')->name('center.destroy');


        Route::get('{id}/update','CenterController@update')->name('center.update');

        Route::post('{id}/update','CenterController@edit')->name('center.edit');


    });

    // Route User
    Route::group(['middleware' => 'auth', 'prefix'=> 'users'], function (){

        Route::get('add-new', 'UserController@create')->name('user.create');

        Route::post('add-new','UserController@store')->name('user.store');

        Route::get('/','UserController@showList')->name('user.list');

        Route::get('search-user','UserController@searchUser')->name('user.searchUser');

        Route::get('search-teacher/{keyword}','UserController@searchTeacher');

        Route::get('{id}/delete','UserController@delete')->name('user.delete');

        Route::post('{id}/delete','UserController@destroy')->name('user.destroy');

        Route::get('{id}/detail','UserController@detail')->name('user.detail');

        Route::get('{id}/update','UserController@update')->name('user.update');

        Route::post('{id}/update','UserController@edit')->name('user.edit');
    });

    //Route me
    Route::group(['middleware' => 'auth', 'prefix'=> 'me'], function (){

        Route::get('/change-password','Auth\ChangePasswordController@update')->name('changePass.update');

        Route::post('/change-password','Auth\ChangePasswordController@edit')->name('changePass.edit');

        Route::get('/update','Auth\ProfileController@update')->name('me.update');

        Route::post('/update','Auth\ProfileController@edit')->name('me.edit');

        Route::get('/profile','Auth\ProfileController@detail')->name('me.profile');




    });
    //Route report
    Route::group(['middleware' => 'auth', 'prefix'=> 'reports'], function (){

        Route::get('add-new', 'ReportController@create')->name('report.create');

        Route::post('add-new', 'ReportController@store')->name('report.store');

        Route::get('/','ReportController@showList')->name('report.list');

        Route::get('{id}/delete','ReportController@delete')->name('report.delete');

        Route::post('{id}/delete','ReportController@destroy')->name('report.destroy');

        Route::get('{id}/detail','ReportController@detail')->name('report.detail');

        Route::post('{id}/update-name','ReportController@updateName')->name('report.update-name');

        Route::post('{id}/update-description','ReportController@updateDescription')->name('report.update-name');

        //group criteria
        Route::get('{reportId}/groups-criteria/add-new', 'GroupCriteriaController@create')->name('group_criteria.create');

        Route::post('{reportId}/groups-criteria/add-new', 'GroupCriteriaController@store')->name('group_criteria.store');

        Route::post('{reportId}/groups-criteria/{group_criteriaId}/update-name','GroupCriteriaController@updateName')->name('group_criteria.updateName');

        Route::post('{reportId}/groups-criteria/{group_criteriaId}/update-description','GroupCriteriaController@updateDescription')->name('group_criteria.updateDescription');

        Route::post('{reportId}/groups-criteria/{group_criteriaId}/delete','GroupCriteriaController@destroy')->name('group_criteria.destroy');



        //category criteria
        Route::get('{reportId}/categories-criteria/add-new','CategoryCriteriaController@create')->name('category.createCategory');

        Route::post('{reportId}/categories-criteria/add-new','CategoryCriteriaController@store')->name('criteria.storeCategory');

        Route::post('{reportId}/groups-criteria/{group_criteriaId}/categories-criteria/{category_criteriaId}/update','CategoryCriteriaController@update')->name('category.edit');

        Route::post('{reportId}/groups-criteria/{group_criteriaId}/categories-criteria/{category_criteriaId}/delete','CategoryCriteriaController@destroy')->name('category.destroy');

        //criteria
        Route::get('{reportId}/criterias/add-new','CriteriaController@create')->name('criteria.create');

        Route::post('{reportId}/criterias/add-new','CriteriaController@store')->name('criteria.store');

        Route::post('{reportId}/groups-criteria/{group_criteriaId}/categories-criteria/{category_criteriaId}/criterias/{criteriaId}/update-name','CriteriaController@updateName')->name('criteria.updateName');
        Route::post('{reportId}/groups-criteria/{group_criteriaId}/categories-criteria/{category_criteriaId}/criterias/{criteriaId}/update-description','CriteriaController@updateDescription')->name('criteria.updateDescription');

        Route::post('{reportId}/groups-criteria/{group_criteriaId}/categories-criteria/{category_criteriaId}/criterias/{criteriaId}/delete','CriteriaController@destroy')->name('criteria.destroy');


    });

    Route::group(['middleware' => 'auth', 'prefix'=> 'evaluate-capacities'], function () {

        Route::get('create-new','EvaluateController@create')->name('evaluate.create');

        Route::post('create-new','EvaluateController@store')->name('evaluate.store');

        Route::get('{id}/detail','EvaluateController@detail')->name('evaluate.detail');

        Route::get('{id}/update','EvaluateController@update')->name('evaluate.update');

        Route::post('{id}/update','EvaluateController@edit')->name('evaluate.edit');

        Route::get('{id}/delete','EvaluateController@delete')->name('evaluate.delete');

        Route::post('{id}/delete','EvaluateController@destroy')->name('evaluate.destroy');



    });



    Route::group(['middleware' => 'auth', 'prefix'=> 'courses'], function () {

        Route::get('add-new', 'CourseController@create')->name('course.create');

        Route::post('add-new','CourseController@store')->name('course.store');

        Route::get('/','CourseController@show')->name('course.show');

        Route::get('{id}/delete','CourseController@delete')->name('course.delete');

        Route::post('{id}/delete','CourseController@destroy')->name('course.destroy');

        Route::get('/{id}/edit','CourseController@edit')->name('course.edit');

        Route::post('/{id}/edit','CourseController@update')->name('course.update');

    });




});


