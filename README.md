# Run docker compose
```docker-compose up -d```

# install dependencies
```docker run --rm -v $(pwd):/app composer/composer install```

# Run migration and seeding
```docker-compose exec app php artisan migrate --seed```

# Access
```http://localhost:9090```

# Change permission
```docker-compose exec app chmod -R 777 /var/www/storage```

```docker-compose exec app chmod -R 777 /var/www/bootstrap/cache```

# Generate key
```docker-compose exec app php artisan key:generate```

# Migration and seeding
```docker-compose exec app php artisan migrate```