let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |

 */
mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .sass('resources/assets/sass/main.scss', 'public/css')
    .sass('resources/assets/sass/layout.scss', 'public/css');

mix.copy('node_modules/bootstrap-sass/assets/fonts/bootstrap/', 'public/fonts/');
mix.copy('resources/assets/css', 'public/css');
mix.copy('resources/assets/library-js', 'public/js');
mix.copy('resources/assets/img', 'public/img');
mix.copy('resources/assets/logo.png', 'public/logo.png');

