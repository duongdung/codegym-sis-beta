@if(Session::has('one-error'))
    <p class="text-danger no-padding no-margin">
        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ Session::get('one-error')}}
    </p>
@endif