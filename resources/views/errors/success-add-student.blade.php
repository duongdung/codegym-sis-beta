@if(Session::has('success-add-student'))
    <p class="text-success no-padding no-margin">
        <i class="fa fa-check" aria-hidden="true"></i>{{ Session::get('success')}}
    </p>
@endif