@if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $error)
                <li><p class="text-danger">{{ $error }}</p></li>
            @endforeach
        </ul>
@endif