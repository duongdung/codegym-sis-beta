<div class="ml-auto">
    <a class="pt-2 pl-3" href="{{ route($routeEdit ,$id) }}">
        <i class="fa fa-pencil-square-o"></i> {{__('language.edit')}}
    </a>
    <a class="pt-2 pl-3" href="{{ route($routeDelete ,$id) }}">
        <i class="fa fa-times text-danger" aria-hidden="true"></i> {{__('language.delete')}}
    </a>
</div>