<div class="{{ $col }} border-left-bottom">
    <div class="row item align-items-center p-4">
        <div class="col-8 text">
            <div class="number"><strong>{{ $count }}</strong></div>
            <div class="title">
                <a href="{{ route($route) }}">{{ $name }}</a>
            </div>
        </div>
        <div class="col-4 icon pr-4"><i class="fa {{ $icon }}" aria-hidden="true"></i></div>
        <div class="col-12 progress p-0 mt-2">
            <div role="progressbar" style="width: 100%; height: 4px;" class="progress-bar bg-orange"></div>
        </div>
    </div>
</div>