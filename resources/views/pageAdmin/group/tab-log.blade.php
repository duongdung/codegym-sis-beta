@extends('layouts.app')

@section('head.title')
    {{ __('language.Group_detail') }}
@endsection

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('group.list') }}">{{ __('language.Group') }} </a></li>
        <li class="breadcrumb-item text-muted">{{ $group->group_id }}</li>
    </ol>

    <div class="main">
        <div class="row">
            <div class="col-12 col-md-12 ">
                <ul class="nav nav-tabs" id="myTab" role="tablist">

                    <!-- Tab info -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-home-tab"
                           href="{{ route('group.detail',$group->id) }}">
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                            {{ __('language.Group_detail') }}
                        </a>
                    </li>

                    <!-- Tab student -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                           href="{{ route('group.student',$group->id) }}">
                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                            {{ __('language.Student') }}
                        </a>
                    </li>

                    <!-- Tab Log -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary active" id="nav-profile-tab" href="{{ route('group.log', $group->id) }}">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                            {{ __('language.Logs') }}
                        </a>
                    </li>

                <!-- Tab Attendance-->
                    @can('create-edit-view-attendance', $group)
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                               href="{{ route('attendance.create', $group->id) }}">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                {{ __('language.Attendance') }}
                            </a>
                        </li>
                    @endcan

                <!-- Tab exam score -->
                    @can('view-group-score', $group)
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                               href="{{ route('group.show.score',$group->id) }}">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                {{ __('language.Test_Score') }}
                            </a>
                        </li>
                    @endcan
                </ul>
                <div class="tab-content" id="myTabContent">
                    <!-- Group logs -->
                    <div class="border-list p-4">
                        <div class="table-responsive">
                            <table id="myTable" class="table table-hover">
                                <thead>
                                <tr>
                                    <th class="border-top-0">{{ __('language.Log') }}</th>
                                    <th class="border-top-0">{{ __('language.date') }}</th>
                                    <th class="border-top-0">{{ __('language.Log_User') }}</th>
                                    <th class="border-top-0"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($logs) === 0)
                                    <tr>
                                        <td colspan="4" class="text-center" class="border-top-0">{{__('language.Empty_Data')}}</td>
                                    </tr>
                                @endif
                                @foreach($logs as $log)
                                    <tr id="group-log-{{$log->id}}">
                                        <td class="border-top-0">
                                            <span class="editable-input"
                                                  data-name="log"
                                                  data-type="textarea"
                                                  data-pk="{{$log->id}}"
                                                  data-url="/groups/{{$group->id}}/logs/{{$log->id}}/update"
                                                  data-title="{{__('language.Edit')}}">{!! $log->log !!}
                                            </span>
                                            <span class="fa fa-edit editable-icon"></span>
                                        </td>
                                        <td class="border-top-0">{{$log->created_at->format('d-m-Y')}}</td>
                                        <td class="border-top-0">{{$log->user->first_name}} {{$log->user->last_name}}</td>
                                        <td class="border-top-0">
                                            @can('add-group-log', $group)
                                                <a onclick="deleteGroupLog({{$group->id}}, {{$log->id}})"
                                                   class="fa fa-remove text-danger" href="javascript: void(0);"></a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{$logs->fragment('group-logs')->links()}}
                            @can('add-group-log', $group)
                                <h4>{{__('language.Add_Log')}}</h4>
                                <form action="{{route('group.log.add', ['group_id' => $group->id])}}" method="post">
                                    {{csrf_field()}}
                                    <div class="form-group {{($errors->has('log'))? 'is-invalid': ''}}">
                                        <div class="input-group">
                                        <textarea rows="4"
                                                  class="col-12 col-md-12 col-lg-12 form-control"
                                                  type="text"
                                                  name="log"
                                                  placeholder=""></textarea>
                                        </div>
                                        @if($errors->has('log'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('log')  }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" value="{{__('language.Add')}}"
                                               class="btn btn-primary"/>
                                    </div>
                                </form>
                            @endcan
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection