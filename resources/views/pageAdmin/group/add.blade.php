@extends('layouts.app')

@section('head.title')
    {{ __('language.Group_Add') }}
@endsection

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('group.list') }}">{{ __('language.Group') }} </a></li>
        <li class="breadcrumb-item text-muted">{{ __('language.Group_Add') }} </li>
    </ol>

    <div class="main">
        <div class="row">
            <div class="col-12">
                <div class="p-4">
                    <form action="{{route('group.store')}}" method="POST">
                        {{ csrf_field() }}
                        @include('errors.success')
                        <p class="text-muted text-left">
                            {{ __('language.Fields_with') }}
                            <span class="text-danger">*</span>
                            {{ __('language.Are_required') }}
                        </p>
                        <!--Group Name-->
                        <div class="form-group row">
                            <label for="inputName"
                                   class="col-12 col-md-2 col-form-label">{{ __('language.Group') }}
                                <span class="align-middle text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="text"
                                       class="form-control @if($errors->has('group_id')) is-invalid @endif"
                                       id="inputName"
                                       placeholder="VD: CGC0717G"
                                       value="{{ old('group_id') }}"
                                       name="group_id" required>
                                @if($errors->has('group_id'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('group_id')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--Group Description-->
                        <div class="form-group row">
                            <label for="inputDescription"
                                   class="col-12 col-md-2 col-form-label">{{ __('language.Description') }}
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="text"
                                       class="form-control"
                                       id="inputDescription"
                                       placeholder=""
                                       value="{{ old('description') }}"
                                       name="description">
                            </div>
                        </div>

                        <!--Study Program-->
                        <div class="form-group row">
                            <label for="inputStudyProgram"
                                   class="col-12 col-md-2 col-form-label">{{ __('language.Study_Program') }}
                                <span class="align-middle text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <select class="form-control" id="inputStudyProgram" name="program_id">
                                    @foreach($programs as $program)
                                        <option class="text-muted"
                                                value="{{$program->id}}">{{$program->name_program}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <!--Start Date-->
                        <div class="form-group row">
                            <label for="inputStartData"
                                   class="col-12 col-md-2 col-form-label">{{ __('language.Start_Date') }}
                                <span class="align-middle text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <input placeholder="dd-mm-yyyy"
                                       class="datepicker form-control @if($errors->has('startdate_group')) is-invalid @endif"
                                       id="inputStartData"
                                       value="{{ date('d-m-Y') }}"
                                       name="startdate_group" required>
                                @if($errors->has('startdate_group'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('startdate_group')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--Expected Date-->
                        <div class="form-group row">
                            <label for="inputStartData"
                                   class="col-12 col-md-2 col-form-label">{{ __('language.Expected_End_Date') }}
                                <span class="align-middle text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <input placeholder="dd-mm-yyyy"
                                       class="datepicker form-control @if($errors->has('expected_end_date_group')) is-invalid @endif"
                                       id="inputStartData"
                                       value="{{ old('expected_end_date_group') }}"
                                       name="expected_end_date_group" required>
                                @if($errors->has('expected_end_date_group'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('expected_end_date_group')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--ghi chu-->
                        <div class="form-group row">
                            <label for="inputBlog"
                                   class="col-12 col-md-2 col-form-label">{{__('language.Note')}}
                            </label>
                            <div class="col-12 col-md-10">
                                <textarea class="editor" name="note"
                                          placeholder=""
                                          rows="4">{{  old('note') }}
                                    </textarea>
                            </div>

                        </div>

                        <!--Button submit and back-->
                        <div class="col-10 col-md-12">
                            <div class="row justify-content-end">
                                <button type="submit"
                                        class="btn btn-primary button-submit">{{__('language.create')}}</button>
                                <a href="{{ route('group.list') }}">
                                    <input type="button"
                                           class=" btn btn-outline-secondary button-submit"
                                           value="{{__('language.Cancel')}}">
                                </a>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection