@extends('layouts.app')

@section('head.title')
    {{ __('language.Group_detail') }}
@endsection

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('group.list') }}">{{ __('language.Group') }} </a></li>
        <li class="breadcrumb-item text-muted">{{ $group->group_id }}</li>
        <li class="breadcrumb-item text-muted">{{ __('language.Create_Test_Score') }}</li>
    </ol>

    <div class="main">
        <div class="row">
            <div class="col-12 col-md-12 ">
                <ul class="nav nav-tabs" id="myTab" role="tablist">

                    <!-- Tab info -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-home-tab"
                           href="{{ route('group.detail',$group->id) }}">
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                            {{ __('language.Group_detail') }}
                        </a>
                    </li>

                    <!-- Tab student -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                           href="{{ route('group.student',$group->id) }}">
                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                            {{ __('language.Student') }}
                        </a>
                    </li>

                    <!-- Tab Log -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                           href="{{ route('group.log', $group->id) }}">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                            {{ __('language.Logs') }}
                        </a>
                    </li>

                    <!-- Tab Attendance-->
                    @can('create-edit-view-attendance', $group)
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                               href="{{ route('attendance.create', $group->id) }}">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                {{ __('language.Attendance') }}
                            </a>
                        </li>
                    @endcan

                <!-- Tab exam score -->
                    @can('view-group-score', $group)
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary active" id="nav-profile-tab"
                               href="{{ route('group.show.score', $group->id) }}">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                {{ __('language.Test_Score') }}
                            </a>
                        </li>
                    @endcan

                <!-- Button edit, delete -->
                    @can('crud-group', Session::get('centerId'))
                        @include('subViews.button-edit-delete', ['routeEdit'=>'group.edit', 'routeDelete'=>'group.destroy','id' => $group->id])
                    @endcan
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="border-list p-4">
                        @can('view-group-score', $group)
                        <a href=" {{ route('group.show.score', $group->id) }} ">
                            <i class="fa fa-eye" aria-hidden="true"></i> {{__('language.View_Test_Score')}}
                        </a>
                        @endcan
                        @if(Session::has('add_score_success'))
                            <p class="text-success no-margin">
                                <i class="fa fa-check" aria-hidden="true"></i>
                                {{Session::get('add_score_success')}}
                            </p>
                        @endif
                        @if(Session::has('edit_score_success'))
                            <p class="text-success no-margin">
                                <i class="fa fa-check" aria-hidden="true"></i>
                                {{Session::get('edit_score_success')}}
                            </p>
                        @endif
                        @if(Session::has('delete_score_success'))
                            <p class="text-success no-margin">
                                <i class="fa fa-check" aria-hidden="true"></i>
                                {{Session::get('delete_score_success')}}
                            </p>
                        @endif
                        @if(Session::has('no_select_course_id'))
                            <p class="text-success no-margin">
                                <i class="fa fa-check" aria-hidden="true"></i>
                                {{Session::get('no_select_course_id')}}
                            </p>
                        @endif
                        <form action="{{ route('group.check.score', $group->id) }}"
                              method="post"
                              class="pt-3">
                            <!--course-->
                            <div class="form-group">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label for="inputStartData"
                                           class="col-12 col-md-2 col-form-label">{{__('language.Course')}}
                                        <span class="align-middle text-danger">*</span>
                                    </label>
                                    <div class="col-12 col-md-10">
                                        @if(isset($courses))
                                            <select class="js-example-basic-single @if($errors->has('selectCourse')) is-invalid @endif"
                                                    style="width: 100%"
                                                    name="selectCourse"
                                                    onchange="this.form.submit()">
                                                <option value="">{{__('language.Choose_Course')}}</option>
                                                @foreach($courses as $key=>$course)
                                                    @if (isset($selectCourse))
                                                        <option value="{{ $course->id }}" {{ ($course->id === $selectCourse->id) ? 'selected' : '' }} >{{ $course->course_name }}</option>
                                                    @else
                                                        <option value="{{ $course->id }}">{{ $course->course_name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            @if($errors->has('selectCourse'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('selectCourse')  }}
                                                </div>
                                            @endif
                                        @else
                                            <p class="text-danger">{{__('language.Empty_Data')}}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </form>
                        @if (Session::has('select_course_id'))
                            <form action="{{ route('group.add.score', $group->id) }}"
                                  method="post"
                                  enctype="multipart/form-data"
                                  class="pt-3">
                            {{ csrf_field() }}
                            <!--Ngay thi-->
                                <div class="form-group row">
                                    <label for="inputStartData"
                                           class="col-12 col-md-2 col-form-label">{{ __('language.Exam_Date') }}
                                        <span class="align-middle text-danger">*</span>
                                    </label>
                                    <div class="col-12 col-md-10">
                                        <input placeholder="dd-mm-yyyy"
                                               class="datepicker form-control @if($errors->has('exam_date')) is-invalid @endif"
                                               value="{{ old('exam_date') }}"
                                               name="exam_date" required>
                                        <input placeholder=""
                                               type="hidden"
                                               value="{{ Session::get('select_course_id') }}"
                                               name="courseSelectId" >
                                        @if($errors->has('exam_date'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('exam_date')  }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th style="width: 5%" class="border-top-0 text-secondary text-center">#</th>
                                            <th style="width: 15%"
                                                class="border-top-0">{{__('language.Student_Id')}}</th>
                                            <th style="width: 25%"
                                                class="border-top-0">{{__('language.Full_Name')}}</th>
                                            <th class="border-top-0">{{__('language.Test_Score')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($group->students as $key => $studentGroup)
                                            <tr>
                                                <td class="border-top-0">{{++$key}}
                                                    <input type="hidden" placeholder="" value="{{ $studentGroup->id }}"
                                                           name="studentId[]">
                                                </td>
                                                <td class="border-top-0">
                                                    <a class="p-0"
                                                       href="{{route('student.details',[$studentGroup->id, $studentGroup->group_id])}}">{{$studentGroup->student_id}}</a>
                                                </td>
                                                <td class="border-top-0">{{$studentGroup->first_name .' '.$studentGroup->last_name}}</td>
                                                <td class="border-top-0">
                                                    <input type="text" placeholder="" name="testScore[]" value=""
                                                           style="width: 40px">
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!--Button submit and back-->
                                <div class="row justify-content-start">
                                    <button type="submit"
                                            class="btn btn-primary button-submit">{{__('language.create')}}</button>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection