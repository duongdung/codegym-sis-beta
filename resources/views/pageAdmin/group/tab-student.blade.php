@extends('layouts.app')

@section('head.title')
    {{ __('language.Group_detail') }}
@endsection

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('group.list') }}">{{ __('language.Group') }} </a></li>
        <li class="breadcrumb-item text-muted">{{ $group->group_id }}</li>
    </ol>

    <div class="main">
        <div class="row">
            <div class="col-12 col-md-12 ">
                <ul class="nav nav-tabs" id="myTab" role="tablist">

                    <!-- Tab info -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-home-tab"
                           href="{{ route('group.detail',$group->id) }}">
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                            {{ __('language.Group_detail') }}
                        </a>
                    </li>

                    <!-- Tab student -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary active" id="nav-profile-tab"
                           href="{{ route('group.student',$group->id) }}">
                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                            {{ __('language.Student') }}
                        </a>
                    </li>

                    <!-- Tab Log -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                           href="{{ route('group.log', $group->id) }}">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                            {{ __('language.Logs') }}
                        </a>
                    </li>

                <!-- Tab Attendance-->
                    @can('create-edit-view-attendance', $group)
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                               href="{{ route('attendance.create', $group->id) }}">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                {{ __('language.Attendance') }}
                            </a>
                        </li>
                    @endcan

                <!-- Tab exam score -->
                    @can('view-group-score', $group)
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                               href="{{ route('group.show.score',$group->id) }}">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                {{ __('language.Test_Score') }}
                            </a>
                        </li>
                    @endcan
                </ul>
                <div class="tab-content" id="myTabContent">

                    <!-- List Student -->
                    <div class="border-list p-4">
                        @if(count($group->students) > 0)
                            {{--status students--}}
                            <p class="h6 p-3 m-0 text-muted">
                                <i class="fa fa-info-circle"></i>
                                {{__('language.have')}} {{count($group->students)}} {{__('language.err_group')}}
                            </p>

                            {{--table list students--}}
                            <div class="table-responsive">
                                <table id="myTable" class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th class="border-top-0 text-secondary text-center">#</th>
                                        <th class="border-top-0">{{__('language.Student_Id')}}</th>
                                        <th class="border-top-0">{{__('language.Full_Name')}}</th>
                                        <th class="border-top-0">{{__('language.Email')}}</th>
                                        @can('crud-group', Session::get('centerId'))
                                            <th class="border-top-0">
                                                <a href="#" class="float-right"
                                                   data-toggle="modal" data-target="#addStudentModal">
                                                    <i class="fa fa-user-plus" aria-hidden="true"></i>
                                                    {{__('language.Add_Student_Group')}}
                                                </a>
                                            </th>
                                        @endcan
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($group->students as $key => $studentGroup)
                                        <tr>
                                            <td class="border-top-0">{{++$key}}</td>
                                            <td class="border-top-0">
                                                <a class="p-0"
                                                   href="{{route('student.details',[$studentGroup->id, $studentGroup->group_id])}}">{{$studentGroup->student_id}}</a>
                                            </td>
                                            <td class="border-top-0">{{$studentGroup->first_name .' '.$studentGroup->last_name}}</td>
                                            <td class="border-top-0">{{$studentGroup->email}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <p class="h6 p-3 m-0 text-muted">
                                <i class="fa fa-info-circle"></i>
                                {{__('language.No_Student')}}
                                @can('crud-group', Session::get('centerId'))
                                    <a href="#" class=""
                                       data-toggle="modal" data-target="#addStudentModal">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        {{__('language.Add_Student_Group')}}
                                    </a>
                                @endcan
                            </p>
                        @endif
                    </div>
                </div>

                <!-- Modal add student -->
                <div class="modal fade bd-example-modal-lg" id="addStudentModal" tabindex="-1"
                     role="dialog"
                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-height" role="document">
                        <div class="modal-content">
                            <div class="search-student pt-5">
                                <div class="form-group d-flex justify-content-center">
                                    <input type="text" class="form-control" id="input-student"
                                           placeholder="{{__('language.Search_Add_Student')}}"
                                           value=""
                                           style="width: 60%"
                                           name="search-student">
                                    <button id="search-student" value="{{ $group->id }}"
                                            class=" btn btn-outline-secondary d-none">
                                    </button>
                                </div>
                            </div>
                            <div class="modal-body">
                                @include('errors.success-add-student')

                                <div id="list-student">
                                    {{--Form search ajax--}}
                                </div>

                            </div>
                            <div class="modal-footer">
                                <a href="{{ route('group.student', $group->id) }}">
                                    <button type="button" class="btn btn-primary ">
                                        {{__('language.Done_Add_Student')}}</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>

@endsection