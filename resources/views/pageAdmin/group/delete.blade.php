@extends('layouts.app')
@section('head.title')
    {{ __('language.Delete_group') }}
@endsection
@section('content')

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-12 col-md-8" style="height: 300px;border-radius: 5px;">

                @if(count($group->students) > 0)
                    <div class="col-12 col-md-12 no-delete pt-5 text-center">
                        <h4><p class="text-danger">{{__('language.No_Delete')}}</p></h4>
                    </div>
                    <div class="suport">
                        <p>{{__('language.Delete_Group')}}</p>
                    </div>
                    <div class="col-12 col-md-12 text-center">
                        <a href="{{route( 'group.detail',$group->id )}}" class="btn btn-outline-secondary btn-lg">{{__('language.Cancel')}}</a>
                    </div>
                @else
                <div class="delete" style="padding-top: 3em" >
                    <div class="name-program text-center" >
                        <p style="padding-top: 10px;"><h5 class="text-danger">{{ __('language.Do_you_Delete_Group') }}:
                            <strong>{{ $group->group_id }}?</strong></h5></p>
                    </div>
                    <div class="form-group row justify-content-md-center">
                        <form action="{{ route('group.destroy',$group->id) }}" method="post">
                            {{ csrf_field() }}
                            <div class="col-6 col-md-3">
                                <button type="submit" class="btn btn-danger btn-lg">{{__('language.Delete')}}</button>
                            </div>
                        </form>
                        <div class="col-6 col-md-3 ">
                            <a href="{{ route('group.detail',$group->id) }}"><button type="button" class="btn btn-outline-secondary btn-lg">{{__('language.Cancel')}}</button></a>
                        </div>

                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

@endsection