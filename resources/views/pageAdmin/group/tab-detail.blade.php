@extends('layouts.app')

@section('head.title')
    {{ __('language.Group_detail') }}
@endsection

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('group.list') }}">{{ __('language.Group') }} </a></li>
        <li class="breadcrumb-item text-muted">{{ $group->group_id }}</li>
    </ol>

    <div class="main">
        <div class="row">
            <div class="col-12 col-md-12 ">
                <ul class="nav nav-tabs" id="myTab" role="tablist">

                    <!-- Tab info -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary active" id="nav-home-tab"
                           href="{{ route('group.detail',$group->id) }}">
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                            {{ __('language.Group_detail') }}
                        </a>
                    </li>

                    <!-- Tab student -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                           href="{{ route('group.student',$group->id) }}">
                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                            {{ __('language.Student') }}
                        </a>
                    </li>

                    <!-- Tab Log -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                           href="{{ route('group.log', $group->id) }}">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                            {{ __('language.Logs') }}
                        </a>
                    </li>

                <!-- Tab Attendance-->
                    @can('create-edit-view-attendance', $group)
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                               href="{{ route('attendance.create', $group->id) }}">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                {{ __('language.Attendance') }}
                            </a>
                        </li>
                    @endcan

                <!-- Tab exam score -->
                    @can('view-group-score', $group)
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                               href="{{ route('group.show.score',$group->id) }}">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                {{ __('language.Test_Score') }}
                            </a>
                        </li>
                    @endcan

                <!-- Button edit, delete -->
                    @can('crud-group', Session::get('centerId'))
                        @include('subViews.button-edit-delete', ['routeEdit'=>'group.edit', 'routeDelete'=>'group.destroy','id' => $group->id])
                    @endcan
                </ul>
                <div class="tab-content" id="myTabContent">
                    <!-- Group details -->
                    <div class="border-list p-4">
                        <div class="table-responsive col-4">
                            <table id="myTable" class="table">
                                {{--<thead>--}}
                                <tbody>
                                <tr>
                                    <th class="border-top-0">{{ __('language.name') }} </th>
                                    <td class="border-top-0">: {{ $group->group_id }}</td>
                                </tr>
                                <tr>
                                    <th class="border-top-0">{{ __('language.Description_Program') }}</th>
                                    <td class="border-top-0">
                                        : {{ $group->group_description }}</td>
                                </tr>
                                <tr>
                                    <th class="border-top-0"
                                        style="width: 60%">{{ __('language.Study_Program') }}</th>
                                    <td class="border-top-0">: {{ $group->program->name_program }}</td>
                                </tr>
                                <tr>
                                    <th class="border-top-0">{{ __('language.Start_Date') }}</th>
                                    <td class="border-top-0">: {{ $group->startdate_group->format('d-m-Y') }}</td>
                                </tr>
                                <tr>
                                    <th class="border-top-0">{{ __('language.Expected_End_Date') }}</th>
                                    <td class="border-top-0">
                                        : {{ $group->expected_end_date_group->format('d-m-Y') }}</td>
                                </tr>
                                <tr>
                                    <th class="border-top-0">{{ __('language.status') }}</th>
                                    <td class="border-top-0">
                                        : {{ $group->note }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        @if(count($group->users) > 0)
                            <p class="h6 p-3 m-0 text-muted">
                                <i class="fa fa-info-circle"></i>
                                {{__('language.have')}} {{count($group->users)}} {{__('language.teacher_group')}}
                            </p>
                            <div class="table-responsive ">
                                <table id="myTable" class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th class="border-top-0 text-secondary text-center">#</th>
                                        <th class="border-top-0">{{__('language.Full_Name')}}</th>
                                        <th class="border-top-0">{{__('language.Email')}}</th>
                                        <th class="border-top-0">{{__('language.role')}}</th>
                                        @can('crud-group', Session::get('centerId'))
                                            <th class="border-top-0">
                                                <a href="#" class="float-right"
                                                   data-toggle="modal" data-target="#addTeacherModal">
                                                    <i class="fa fa-user-plus" aria-hidden="true"></i>
                                                    {{__('language.add_teacher')}}
                                                </a>
                                            </th>
                                            <th class="border-top-0"></th>
                                        @endcan
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($group->users as $key =>$teacher)
                                        <tr style="position: relative">
                                            <td class="border-top-0">{{++$key}}</td>
                                            <td class="teacher-id d-none">{{$teacher->id}}</td>
                                            <td class="border-top-0"
                                                style="width: 200px">{{ $teacher->first_name. ' ' .$teacher->last_name }}</td>
                                            <td class="border-top-0">{{$teacher->email}}</td>
                                            <td class="border-top-0">
                                                @if($teacher->pivot->role == 1)
                                                    {{__('language.teacher')}}
                                                @elseif($teacher->pivot->role == 2)
                                                    {{__('language.tutor')}}
                                                @endif
                                            </td>
                                            @can('crud-group', Session::get('centerId'))
                                                <td class="border-top-0">

                                                </td>
                                                <td class="border-top-0">
                                                    <a class="p-0" data-toggle="modal"
                                                       data-target="#{{$teacher->id}}">
                                                        <i class="fa fa-times text-danger"
                                                           aria-hidden="true"></i>
                                                    </a>
                                                    <!-- Modal delete -->
                                                    <div class="modal fade" id="{{$teacher->id}}" tabindex="-1"
                                                         role="dialog"
                                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header text-danger">
                                                                    <h5 class="modal-title"
                                                                        id="exampleModalLabel">{{__('language.delete_teacher')}}</h5>
                                                                </div>
                                                                <div class="modal-body">
                                                                    {{__('language.Do_you_Delete_Teacher')}}
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button"
                                                                            class="btn btn-secondary "
                                                                            data-dismiss="modal">{{__('language.Cancel')}}
                                                                    </button>
                                                                    <a href="{{ route('group.deleteTeacher', [$group->id, $teacher->id]) }}">
                                                                        <button type="submit"
                                                                                class="btn btn-primary">
                                                                            {{__('language.Delete')}}
                                                                        </button>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            @endcan
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <p class="h6 p-3 m-0 text-muted">
                                <i class="fa fa-info-circle"></i>
                                {{__('language.No_Teacher')}}

                                <a href="#" class=""
                                   data-toggle="modal" data-target="#addTeacherModal">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                    {{__('language.add_teacher')}}
                                </a>
                            </p>
                        @endif
                    </div>

                    <!-- Modal add teacher -->
                    <div class="modal fade bd-example-modal-lg" id="addTeacherModal" tabindex="-1"
                         role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-height" role="document">
                            <div class="modal-content">
                                <div class="search-student pt-5">
                                    <div class="form-group d-flex justify-content-center">
                                        <input type="text" class="form-control" id="input-teacher"
                                               placeholder="{{__('language.Search_Add_Teacher')}}"
                                               value=""
                                               name="search-teacher"
                                               style="width: 60%">
                                        <button id="search-teacher" value="{{ $group->id }}"
                                                class=" btn btn-outline-secondary d-none">
                                        </button>
                                    </div>
                                </div>
                                <div class="modal-body">
                                    @include('errors.success-add-student')

                                    <div id="list-teacher">
                                        {{--Form search ajax--}}
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <a href="{{ route('group.detail', $group->id) }}">
                                        <button type="button" class="btn btn-primary ">
                                            {{__('language.Done_Add_Teacher')}}</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection