@extends('layouts.app')
@section('head.title')
    {{__('language.Group_List')}}
@endsection

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted"><i class="" aria-hidden="true"></i>{{__('language.Group_List')}}</li>
    </ol>
    @include('errors.success')
    @include('errors.one-error')
    <div class="row">
        <div class="col-6">
            @can('crud-group', Session::get('centerId'))
                <a class="btn btn-outline-primary" href="{{ route('group.create') }}">
                    <i class="fa fa-plus" aria-hidden="true"></i> {{__('language.Add')}}
                </a>
            @endcan
        </div>
        <div class="col-6">
            @if(count($groups) == 0)
            @else
                <form class="form-inline" action="{{ route('group.search.group') }}"
                      method="GET">
                    {{ csrf_field() }}
                    <input class="form-control col-9" type="text"
                           name="searchGroup"
                           placeholder="{{__('language.search')}}"
                           aria-label="Search">
                    <button class="btn btn-outline-secondary col-3"
                            type="submit" value="searchGroup">{{__('language.search')}}</button>
                </form>
            @endif
        </div>
        <!-- Result -->
        <div class="col-12">
            @if(Session::has('no-result'))
                <p class="text-muted">
                    {{ Session::get('no-result')}}
                </p>
            @endif
            @if(Session::has('have-result'))
                <p class="text-muted">
                    {{ Session::get('have-result')}}
                </p>
            @endif
        </div><!-- End Result -->
    </div>
    <div class="main p-3">
        <div class="row">
            <div class="col-12 col-md-12 table-responsive no-padding">
                <table id="myTable" class="table table-hover">
                    <thead class="thead-table">
                    <tr>
                        <th >#</th>
                        <th >{{__('language.Group_ID')}}</th>
                        <th >{{__('language.Description')}}</th>
                        <th >{{__('language.Start_Date')}}</th>
                        <th >{{__('language.Expected_End_Date')}}</th>
                        @can('crud-group', Session::get('centerId'))
                            <th ></th>
                        @endcan
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($groups) == 0)
                        <p class="text-center">{{ __('language.No_Data') }}</p>
                    @else
                        @foreach($groups as $key => $group)
                            <tr>
                                <td >{{ ++$key }}</td>
                                <td ><a
                                            href="{{ route('group.detail', $group->id) }}">{{$group->group_id}}</a>
                                </td>
                                <td >{{ $group->group_description}}</td>
                                <td >{{ $group->startdate_group->format('d-m-Y') }}</td>
                                <td >{{ $group->expected_end_date_group->format('d-m-Y') }}</td>
                                @can('crud-group', Session::get('centerId'))
                                    <td class="text-center ">
                                        @include('subViews.button-edit-delete', ['routeEdit'=>'group.update', 'routeDelete'=>'group.delete','id' => $group->id])
                                    </td>
                                @endcan
                            </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
                <div class="pagination" style="margin-top: 10px;float: right">
                    {{ $groups->links() }}
                </div>
            </div>
        </div>
    </div>
    <!--Hết -->
@endsection