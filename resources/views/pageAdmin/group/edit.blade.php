@extends('layouts.app')

@section('head.title')
    {{ __('language.Edit_Groups') }}
@endsection


@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('group.list') }}">{{ __('language.Group') }} </a></li>
        <li class="breadcrumb-item text-muted"> {{ __('language.Edit_Groups') }}</li>
        <li class="breadcrumb-item text-muted"> {{ $group->group_id }}</li>
    </ol>

    <div class="main">
        <div class="row">
            <div class="col-12">
                <div class="p-4">
                    <form action="{{route('group.update',$group->id)}}" method="POST">
                    {{ csrf_field() }}
                    @include('errors.success')
                    <!--Group Name-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <p class="col-12 text-muted p-2 text-note">
                                    {{ __('language.Fields_with') }}
                                    <span class="text-danger">*</span>
                                    {{ __('language.Are_required') }}
                                </p>
                                <label for="inputName"
                                       class="col-12 col-md-2 col-form-label">{{ __('language.Group') }}
                                    <span class="align-middle text-danger">*</span>
                                </label>
                                <div class="col-12 col-md-10">
                                    <input type="text"
                                           class="form-control @if($errors->has('group_id')) is-invalid @endif"
                                           id="inputName"
                                           placeholder=""
                                           value="{{ $group->group_id }}"
                                           name="group_id" required>
                                    @if($errors->has('group_id'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('group_id')  }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--Group Description-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputDescription"
                                       class="col-12 col-md-2 col-form-label">{{ __('language.Description') }}
                                </label>
                                <div class="col-12 col-md-10">
                                    <input type="text"
                                           class="form-control @if($errors->has('description')) is-invalid @endif"
                                           id="inputDescription"
                                           placeholder=""
                                           value="{{ $group->group_description }}"
                                           name="description">
                                    @if($errors->has('description'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('description')  }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--Study Program-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputStudyProgram"
                                       class="col-12 col-md-2 col-form-label">{{ __('language.Study_Program') }}
                                    <span class="align-middle text-danger">*</span>
                                </label>
                                <div class="col-12 col-md-10">
                                    <select class="form-control" id="inputStudyProgram" name="program_id">
                                        @foreach($programs as $program)
                                            <option
                                                    @if($group->program_id == $program->id)
                                                    {{"selected"}}
                                                    @endif
                                                    value="{{$program->id}}">{{$program->name_program}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                        </div>

                        <!--Start Date-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputStartData"
                                       class="col-12 col-md-2 col-form-label">{{ __('language.Start_Date') }}
                                    <span class="align-middle text-danger">*</span>
                                </label>
                                <div class="col-12 col-md-10">
                                    <input class="datepicker form-control @if($errors->has('startdate_group')) is-invalid @endif"
                                           id="inputStartData"
                                           placeholder=""
                                           value="{{ $group->startdate_group->format('Y-m-d') }}"
                                           name="startdate_group" required>
                                    @if($errors->has('startdate_group'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('startdate_group')  }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--Expected Date-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputStartData"
                                       class="col-12 col-md-2 col-form-label">{{ __('language.Expected_End_Date') }}
                                    <span class="align-middle text-danger">*</span>
                                </label>
                                <div class="col-12 col-md-10">
                                    <input class="datepicker form-control @if($errors->has('expected_end_date_group')) is-invalid @endif"
                                           id="inputStartData"
                                           placeholder=""
                                           value="{{ $group->expected_end_date_group->format('Y-m-d') }}"
                                           name="expected_end_date_group" required>
                                    @if($errors->has('expected_end_date_group'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('expected_end_date_group')  }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--ghi chu-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputBlog"
                                       class="col-12 col-md-2 col-form-label">{{__('language.Note')}}
                                </label>
                                <div class="col-12 col-md-10">
                                <textarea class="form-control editor" name="note"
                                          placeholder=""
                                          rows="4">{{  $group->note }}
                                </textarea>
                                </div>
                            </div>
                        </div>

                        <!--Button submit and back-->
                        <div class="col-10 col-md-12">
                            <div class="row justify-content-end">
                                <button type="submit"
                                        class="btn btn-primary button-submit">{{__('language.Save')}}</button>
                                <a href="{{ route('group.detail',$group->id) }}">
                                    <input type="button"
                                           class=" btn btn-outline-secondary button-submit"
                                           value="{{__('language.Cancel')}}">
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection