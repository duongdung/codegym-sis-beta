@extends('layouts.app')

@section('head.title')
    {{__('language.create_group_criteria')}}
@endsection

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted">{{__('language.create_group_criteria')}}</li>
    </ol>
    <div class="main">
        <div class="row">
            <div class="col-12">
                <div class="p-4">
                    <form action="{{ route('group_criteria.store',$reports->id) }}" method="POST">
                    {{ csrf_field() }}
                        <!--Name-->
                        <div class="form-group row">
                            <label for="inputName"
                                   class="col-12 col-md-2 col-form-label">{{__('language.name')}}
                                <span class="align-middle text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="text"
                                       class=" form-control @if($errors->has('name')) is-invalid @endif"
                                       id="name"
                                       placeholder=""
                                       value="{{ old('name') }}"
                                       name="name" required>
                                @if($errors->has('name'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('name')  }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputReport"
                                   class=" col-12 col-md-2 col-form-label">{{__('language.Report')}}
                                <span class="align-middle text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10 col-lg-9">
                                <label class="pt-2">{{ $reports->name }}</label>
                            </div>
                        </div>
                        <!--ghi chu-->
                        <div class="form-group row">
                            <label for="inputBlog"
                                   class="col-12 col-md-2 col-form-label">{{__('language.Note')}}
                            </label>
                            <div class="col-12 col-md-10">
                                <textarea class="col-12 col-md-8 col-lg-12 form-control" name="description" placeholder="" rows="4">{{  old('description') }}</textarea>
                            </div>
                        </div>

                        <!--Button submit and back-->
                        <div class="row justify-content-end">
                            <button type="submit"
                                    class="btn btn-primary button-submit">{{__('language.create')}}</button>
                            <a href="{{ route('report.detail',$reports->id) }}">
                                <input type="button"
                                       class=" btn btn-outline-secondary button-submit"
                                       value="{{__('language.Cancel')}}">
                            </a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection