@extends('layouts.app')

@section('head.title')
    {{ __('language.student_update') }}
@endsection

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted">{{__('language.Student')}} {{ $student->first_name .' '. $student->last_name }}</li>
        <li class="breadcrumb-item text-muted"> {{ __('language.student_update') }}</li>

    </ol>

    <div class="main">
        <div class="row">
            <div class="col-12">
                <div class="p-4">
                    <form action="{{route('student.update',$student->id)}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <!--Ma sinh vien-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <p class="col-12 text-muted p-2 text-note">
                                    {{ __('language.Fields_with') }}
                                    <span class="text-danger">*</span>
                                    {{ __('language.Are_required') }}
                                </p>
                                <label for="inputId"
                                       class="col-12 col-md-2 col-form-label">{{__('language.Student_Id')}}
                                    <span class="align-middle text-danger">*</span>
                                </label>
                                <div class="col-12 col-md-10">
                                    <input type="text"
                                           class=" form-control @if($errors->has('student_id')) is-invalid @endif"
                                           id="inputId"
                                           placeholder=""
                                           value="{{ $student->student_id  }}"
                                           name="student_id" required>
                                    @if($errors->has('student_id'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('student_id')  }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--First Name-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputFirstName"
                                       class="col-12 col-md-2 col-form-label">{{__('language.first_name')}}
                                    <span class="align-middle text-danger">*</span>
                                </label>
                                <div class="col-12 col-md-10">
                                    <input type="text"
                                           class=" form-control @if($errors->has('first_name')) is-invalid @endif"
                                           id="first_name"
                                           placeholder=""
                                           value="{{ $student->first_name }}"
                                           name="first_name" required>
                                    @if($errors->has('first_name'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('first_name')  }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--Last Name-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputLastName"
                                       class="col-12 col-md-2 col-form-label">{{__('language.last_name')}}
                                    <span class="align-middle text-danger">*</span>
                                </label>
                                <div class="col-12 col-md-10">
                                    <input type="text"
                                           class=" form-control @if($errors->has('last_name')) is-invalid @endif"
                                           id="last_name"
                                           placeholder=""
                                           value="{{ $student->last_name }}"
                                           name="last_name" required>
                                    @if($errors->has('last_name'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('last_name')  }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        {{--Avatar--}}
                        <div class="col-10 col-md-12 col-lg-12 ">
                            <div class="form-group row">
                                <label for="inputLastName"
                                       class="col-12 col-md-2 col-form-label">{{__('language.Avatar')}}
                                </label>
                                <div class="col-12 col-md-10">
                                    <img id="img-preview" class="img-thumbnail d-block img-responsive "
                                         src="{{ ($student->image) ? asset('storage/'.$student->image) : asset('img/no-avatar.jpg') }}">
                                    <span class="btn btn-outline-primary btn-file col-md-2 mt-3">
                                                {{ __('language.Choose_File') }}
                                        <input id="avatar" class="btn" name="image" type="file">
                                            </span>
                                    @if($errors->has('image'))
                                        <div class="invalid-feedback" style="display: block">
                                            {{ $errors->first('image')  }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--Birthday-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputBirthday"
                                       class="col-12 col-md-2 col-form-label">{{__('language.Birthday')}}
                                </label>
                                <div class="col-12 col-md-10">
                                    <input class="datepicker form-control @if($errors->has('birthday')) is-invalid @endif"
                                           id="inputBirthday"
                                           placeholder="dd-mm-yyyy"
                                           value="{{ ($student->birthday) ? $student->birthday->format('d-m-Y') : '' }}"
                                           name="birthday" required>
                                    @if($errors->has('birthday'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('birthday')  }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--Phone-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputPhone"
                                       class="col-12 col-md-2 col-form-label">{{__('language.Phone_Number')}}
                                    <span class="align-middle text-danger">*</span>
                                </label>
                                <div class="col-12 col-md-10">
                                    <input type="text"
                                           class="form-control @if($errors->has('phone')) is-invalid @endif"
                                           id="inputPhone"
                                           placeholder=""
                                           value="{{ $student->phone  }}"
                                           name="phone" required>
                                    @if($errors->has('phone'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('phone')  }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--Group id-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputHousehold"
                                       class="col-12 col-md-2 col-form-label">{{__('language.Group')}}
                                    <span class="align-middle text-danger">*</span>
                                </label>
                                <div class="col-12 col-md-10">
                                    <select class="form-control" id="exampleSelect1"
                                            name="group_id">
                                        @foreach($groups as $group)
                                            <option
                                                    @if($student->group_id == $group->id)
                                                    {{"selected"}}
                                                    @endif
                                                    value="{{$group->id}}">{{$group->group_id}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                        </div>

                        <!--email-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputEmail"
                                       class="col-12 col-md-2 col-form-label">{{__('language.Email')}}
                                    <span class="align-middle text-danger">*</span>
                                </label>
                                <div class="col-12 col-md-10">
                                    <input type="email"
                                           class="form-control @if($errors->has('email')) is-invalid @endif"
                                           id="inputEmail"
                                           placeholder=""
                                           value="{{ $student->email }}"
                                           name="email" required>
                                    @if($errors->has('email'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('email')  }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--CMND-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputIdentifyCard"
                                       class="col-12 col-md-2 col-form-label">{{__('language.Identify_Card')}}
                                </label>
                                <div class="col-12 col-md-10">
                                    <input type="text"
                                           class="form-control @if($errors->has('identify_card')) is-invalid @endif"
                                           id="inputIdentifyCard"
                                           placeholder=""
                                           value="{{ $student->identify_card }}"
                                           name="identify_card">
                                    @if($errors->has('identify_card'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('identify_card')  }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--Ngay cap-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputStartdateId"
                                       class="col-12 col-md-2 col-form-label">{{__('language.Startdate_ID')}}
                                </label>
                                <div class="col-12 col-md-10">
                                    <input class="datepicker form-control @if($errors->has('startdate_id')) is-invalid @endif"
                                           id="inputStartdateId"
                                           placeholder="dd-mm-yyyy"
                                           value="{{ ($student->startdate_id) ? $student->startdate_id->format('d-m-Y') : '' }}"
                                           name="startdate_id">
                                    @if($errors->has('startdate_id'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('startdate_id')  }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--Noi cap-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputAddressId"
                                       class="col-12 col-md-2 col-form-label">{{__('language.Address_ID')}}
                                </label>
                                <div class="col-12 col-md-10">
                                    <input type="text"
                                           class="form-control @if($errors->has('address_id')) is-invalid @endif"
                                           id="inputAddressId"
                                           placeholder=""
                                           value="{{ $student->address_id }}"
                                           name="address_id">
                                    @if($errors->has('address_id'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('address_id')  }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--Ho khau thuong chu-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputHousehold"
                                       class="col-12 col-md-2 col-form-label">{{__('language.Household')}}
                                </label>
                                <div class="col-12 col-md-10">
                                    <input type="text"
                                           class="form-control"
                                           id="inputHousehold"
                                           placeholder=""
                                           value="{{$student->Household }}"
                                           name="household">

                                </div>
                            </div>
                        </div>

                        <!--cong ty-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputCompany"
                                       class="col-12 col-md-2 col-form-label">{{ __('language.Company') }}
                                </label>
                                <div class="col-12 col-md-10">
                                    <input type="text"
                                           class="form-control"
                                           id="inputCompany"
                                           placeholder=""
                                           value="{{ $student->company  }}"
                                           name="company">
                                </div>
                            </div>
                        </div>

                        <!--Github-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputGithub"
                                       class="col-12 col-md-2 col-form-label">{{__('language.Link_GitHub')}}
                                </label>
                                <div class="col-12 col-md-10">
                                    <input type="text"
                                           class="form-control"
                                           id="inputGithub"
                                           placeholder=""
                                           value="{{ $student->link_github  }}"
                                           name="link_github">
                                </div>
                            </div>
                        </div>

                        <!--link_blog-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputBlog"
                                       class="col-12 col-md-2 col-form-label">{{__('language.Link_Blog')}}
                                </label>
                                <div class="col-12 col-md-10">
                                    <input type="text"
                                           class="form-control"
                                           id="inputBlog"
                                           placeholder=""
                                           value="{{ $student->link_blog  }}"
                                           name="link_blog">
                                </div>
                            </div>
                        </div>

                        <!--Agilearn_ID-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputAgilearn"
                                       class="col-12 col-md-2 col-form-label">{{__('language.Agilearn_ID')}}
                                </label>
                                <div class="col-12 col-md-10">
                                    <input type="text"
                                           class="form-control"
                                           id="inputAgilearn"
                                           placeholder=""
                                           value="{{ $student->agilearn_id  }}"
                                           name="agilearn_id">
                                </div>
                            </div>
                        </div>

                        <!--Ngay bat dau hoc-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputPassword"
                                       class="col-12 col-md-2 col-form-label">{{__('language.Start_School')}}
                                </label>
                                <div class="col-12 col-md-10">
                                    <input class="datepicker form-control @if($errors->has('start_school')) is-invalid @endif"
                                           id="inputPassword"
                                           placeholder="dd-mm-yyyy"
                                           value="{{ ($student->start_school) ? $student->start_school->format('d-m-Y') : '' }}"
                                           name="start_school">
                                </div>
                            </div>
                        </div>

                        <!--ngay ket thuc du kien-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputExpected"
                                       class="col-12 col-md-2 col-form-label">{{__('language.Expected_End_Date')}}
                                </label>
                                <div class="col-12 col-md-10">
                                    <input class="datepicker form-control @if($errors->has('expected_end_date')) is-invalid @endif"
                                           id="inputExpected"
                                           placeholder="dd-mm-yyyy"
                                           value="{{ ($student->expected_end_date) ? $student->expected_end_date->format('d-m-Y') : '' }}"
                                           name="expected_end_date">
                                    @if($errors->has('expected_end_date'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('expected_end_date')  }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--ngay ket thuc thuc te-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputExpected"
                                       class="col-12 col-md-2 col-form-label">{{__('language.Actual_End_Date')}}
                                </label>
                                <div class="col-12 col-md-10">
                                    <input class="datepicker form-control @if($errors->has('actual_end_date')) is-invalid @endif"
                                           id="inputExpected"
                                           placeholder="dd-mm-yyyy"
                                           value="{{ ($student->actual_end_date) ? $student->actual_end_date->format('d-m-Y') : '' }}"
                                           name="actual_end_date">
                                    @if($errors->has('actual_end_date'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('actual_end_date')  }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--ghi chu-->
                        <div class="col-10 col-md-12">
                            <div class="form-group row">
                                <label for="inputBlog"
                                       class="col-12 col-12 col-md-2 col-form-label">{{__('language.Note')}}
                                </label>
                                <div class="col-12 col-md-10">
                                                <textarea class="form-control editor" name="note"
                                                          placeholder=""
                                                          rows="4">{{  $student->note }}</textarea>
                                </div>
                            </div>
                        </div>

                        <!--Button submit and back-->
                        <div class="col-10 col-md-12">
                            <div class="row justify-content-end">
                                <button type="submit"
                                        class="btn btn-primary button-submit">{{__('language.change_info')}}</button>
                                <a href="{{ route('student.details',[$student->id, $student->group_id]) }}">
                                    <input type="button"
                                           class=" btn btn-outline-secondary button-submit"
                                           value="{{__('language.Cancel')}}">
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection