@extends('layouts.app')

@section('head.title')
    {{ __('language.Log') }}
@endsection

@section('content')
    @if(count($students) === 0)
        <p class="text-center mt-5">{{__('language.Empty_Data')}}</p>
    @else
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted">{{__('language.Student')}} {{ $students->first_name .' '. $students->last_name }}</li>
        <li class="breadcrumb-item text-muted">{{__('language.Log')}}</li>
    </ol>
    <div class="main">
        <div class="row">
            @include('errors.success-evaluate')
            {{--@include('errors.success')--}}
            <div class="col-12 col-md-12 ">
                <ul class=" nav nav-tabs responsive" role="tablist">

                    <!-- Tab thong tin ca nhan -->
                    <li class="nav-item">
                        <a class="nav-link text-secondary"
                           href="{{ route('student.details', [$students->id, $students->group_id]) }}">{{__('language.Personal')}}</a>
                    </li>
                @can('view-list-students',Session::get('centerId'))
                    <!-- Tab lop hoc -->
                    <li class="nav-item">
                        <a class="nav-link text-secondary"
                           href="{{ route('student.group', $students->id) }}">{{__('language.Group')}}</a>
                    </li>
                @endcan
                @can('add-student-log', $students)
                    <!-- Tab nhat ky -->
                    <li class="nav-item">
                        <a class="nav-link text-secondary active"
                           href="{{ route('student.log', $students->id) }}">{{__('language.Logs')}}
                        </a>
                    </li>
                @endcan
                    <!-- Tab bao cao -->
                    @can('crud-capacity',$students->group)
                        <li class="nav-item">
                            <a class="nav-link text-secondary"
                               href="{{ route('student.capacity', [$students->id,$students->group_id]) }}"
                            >{{__('language.evaluate_capacity')}}</a>
                        </li>
                    @endcan
                @can('view-student-score', $student)
                <!-- Tab diem thi -->
                    <li class="nav-item">
                        <a class="nav-link text-secondary"
                           href="{{ route('student.tab.score', $students->id) }}"
                        >{{__('language.Test_Score')}}</a>
                    </li>
                @endcan
                </ul>
                <!-- Tab panes -->
                <div class="tab-content" id="myTabContent">
                    <div class="border-list p-4">
                        <div class="table-responsive">
                            <table id="myTable" class="table table-hover">
                                <thead>
                                <tr>
                                    <th>{{ __('language.Log') }}</th>
                                    <th>{{ __('language.date') }}</th>
                                    <th>{{ __('language.Log_User') }}</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($logs) === 0)
                                    <tr>
                                        <td colspan="4" class="text-center">{{__('language.Empty_Data')}}</td>
                                    </tr>
                                @endif

                                @foreach($logs as $log)
                                    <tr id="student-log-{{$log->id}}">
                                        <td>
                            <span class="editable-input"
                                  data-name="log"
                                  data-type="textarea"
                                  data-pk="{{$log->id}}"
                                  data-url="/students/{{$students->id}}/logs/{{$log->id}}/update"
                                  data-title="{{__('language.Edit')}}">{!! $log->log !!}</span>
                                            <span class="fa fa-edit editable-icon"></span>
                                        </td>
                                        <td>{{$log->created_at->format('d-m-Y')}}</td>
                                        <td>{{$log->user->first_name}} {{$log->user->last_name}}</td>
                                        <td>
                                            @can('add-student-log', $students)
                                                <a onclick="deleteStudentLog({{$students->id}}, {{$log->id}})"
                                                   class="fa fa-remove text-danger" href="javascript: void(0);"></a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{$logs->fragment('logs')->links()}}
                            @can('add-student-log', $students)
                                <h4>{{__('language.Add_Log')}}</h4>
                                <form action="{{route('student.log.add', ['student_id' => $students->id])}}"
                                      method="post">
                                    {{csrf_field()}}
                                    <div class="form-group {{($errors->has('log'))? 'is-invalid': ''}}">
                                        <div class="input-group">
                            <textarea rows="4"
                                      class="col-12 col-md-12 col-lg-12 form-control"
                                      type="text"
                                      name="log"
                                      placeholder=""></textarea>
                                        </div>
                                        @if($errors->has('log'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('log')  }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" value="{{__('language.Add')}}"
                                               class="btn btn-primary"/>
                                    </div>
                                </form>
                            @endcan
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection



