@extends('layouts.app')

@section('head.title')
    {{ __('language.evaluate_capacity') }}
@endsection

@section('content')
    @if(count($student) === 0)
        <p class="text-center mt-5">{{__('language.Empty_Data')}}</p>
    @else
        <ol class="breadcrumb">
            <li class="breadcrumb-item text-muted">{{__('language.Student')}} {{ $student->first_name .' '. $student->last_name }}</li>
            <li class="breadcrumb-item text-muted">{{__('language.evaluate_capacity')}}</li>
        </ol>
        <div class="main">
            <div class="row">
                <div class="col-12 col-md-12 ">
                    @include('errors.success-evaluate')
                    @include('errors.success')
                    <ul class=" nav nav-tabs responsive" role="tablist">

                        <!-- Tab thong tin ca nhan -->
                        <li class="nav-item">
                            <a class="nav-link text-secondary"
                               href="{{ route('student.details', [$student->id, $student->group_id]) }}">{{__('language.Personal')}}</a>
                        </li>
                    @can('view-list-students',Session::get('centerId'))
                        <!-- Tab lop hoc -->
                            <li class="nav-item">
                                <a class="nav-link text-secondary"
                                   href="{{ route('student.group', $student->id) }}">{{__('language.Group')}}</a>
                            </li>
                    @endcan
                    @can('add-student-log', $student)
                        <!-- Tab nhat ky -->
                            <li class="nav-item">
                                <a class="nav-link text-secondary"
                                   href="{{ route('student.log', $student->id) }}">{{__('language.Logs')}}
                                </a>
                            </li>
                    @endcan
                    <!-- Tab bao cao -->
                        @can('crud-capacity',$group)
                            <li class="nav-item">
                                <a class="nav-link text-secondary active"
                                   href="{{ route('student.capacity',[$student->id,$group->id] ) }}"
                                >{{__('language.evaluate_capacity')}}</a>
                            </li>
                        @endcan
                        @can('view-student-score', $student)
                        <!-- Tab diem thi -->
                            <li class="nav-item">
                                <a class="nav-link text-secondary"
                                   href="{{ route('student.tab.score', $student->id) }}"
                                >{{__('language.Test_Score')}}</a>
                            </li>
                        @endcan
                    </ul>
                    <!--Tab bao cao-->
                    <div class="tab-content" id="myTabContent">
                        <div class="border-list p-4">
                            @include('errors.success')
                            @can('crud-capacity',$group)
                                @if(count($reports)==0)
                                    <p>{{__('language.no_report')}}</p>
                                @else
                                    <div class="row">
                                        <div class="col-12">
                                            <a class="btn btn-primary button-submit"
                                               href="{{ route('evaluate.create',['student'=>$student->id,'group'=>$student->group->id]) }}">{{__('language.create_evaluate')}}</a>
                                        </div>
                                        <div class="col-12">
                                            @if(count($evaluates) > 0)
                                                <a href="{{route('student.showEvaluation',[$group->id,$student->id])}}"><i
                                                            class="fa fa-file-word-o" aria-hidden="true"></i>
                                                    {{__('language.view-evaluate')}}</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th class="border-top-0">#</th>
                                                <th class="border-top-0">{{__('language.create_evaluate')}}</th>
                                                <th class="border-top-0">{{__('language.date')}}</th>
                                                <th class="border-top-0">{{__('language.evaluator')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($evaluates)=== 0)
                                                <tr>
                                                    <td colspan="4"
                                                        class="text-center">{{ __('language.No_Data') }}</td>
                                                </tr>
                                            @else
                                                @foreach($evaluates as $key=> $evaluate)
                                                    <tr>
                                                        <th scope="row" class="border-top-0">{{++$key}}</th>
                                                        <td class="border-top-0">
                                                            <a href="{{ route('evaluate.detail',[$evaluate->id,'student'=>$student->id,'group'=>$group->id ])}}">{{__('language.evaluation_round')}} {{$key}}</a>
                                                        </td>
                                                        <td class="border-top-0">{{ $evaluate->created_at->format('d-m-Y') }}</td>
                                                        <td class="border-top-0">{{ $evaluate->user->first_name .' '.$evaluate->user->last_name}}</td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                            @endcan
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection





