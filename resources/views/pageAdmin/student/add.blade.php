@extends('layouts.app')

@section('head.title')
    {{ __('language.Student_Add') }}
@endsection

@section('content')
    <ol class="breadcrumb pb-4">
        <li class="breadcrumb-item text-muted">{{__('language.Student_Add')}}</li>
    </ol>
    <div class="main">
        @include('errors.success')
        @if(Session::has('have-an-error'))
            <p class="text-danger no-margin">
                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                {{ Session::get('have-an-error')}}
            </p>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="p-4">
                    <form action="{{route('student.store')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <!--Ma sinh vien-->
                        <div class="form-group row">
                            <p class="col-12 text-muted p-2 text-note">
                                {{ __('language.Fields_with') }}
                                <span class="text-danger">*</span>
                                {{ __('language.Are_required') }}
                            </p>
                            <label for="inputId"
                                   class="col-12 col-md-2 col-form-label">{{__('language.Student_Id')}}
                                <span class="align-middle text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="text"
                                       class=" form-control @if($errors->has('student_id')) is-invalid @endif"
                                       id="inputId"
                                       placeholder="VD: CGHN00001"
                                       value="{{ old('student_id') }}"
                                       name="student_id" required>
                                @if($errors->has('student_id'))
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $errors->first('student_id')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--First Name-->
                        <div class="form-group row">
                            <label for="inputFirstName"
                                   class="col-12 col-md-2 col-form-label">{{__('language.first_name')}}
                                <span class="align-middle text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="text"
                                       class=" form-control @if($errors->has('first_name')) is-invalid @endif"
                                       id="inputFirstName"
                                       placeholder=""
                                       value="{{ old('first_name') }}"
                                       name="first_name" required>
                                @if($errors->has('first_name'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('first_name')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--Last Name-->
                        <div class="form-group row">
                            <label for="inputLastName"
                                   class="col-12 col-md-2 col-form-label">{{__('language.last_name')}}
                                <span class="align-middle text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="text"
                                       class=" form-control @if($errors->has('last_name')) is-invalid @endif"
                                       id="inputLastName"
                                       placeholder=""
                                       value="{{ old('last_name') }}"
                                       name="last_name" required>
                                @if($errors->has('last_name'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('last_name')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--Birthday-->
                        <div class="form-group row">
                            <label for="inputBirthday"
                                   class="col-12 col-md-2 col-form-label">{{__('language.Birthday')}}
                            </label>
                            <div class="col-12 col-md-10">
                                <input id="inputBirthday"
                                       class="datepicker form-control @if($errors->has('birthday')) is-invalid @endif"
                                       placeholder="dd-mm-yyyy"
                                       value="{{ old('birthday') }}"
                                       name="birthday" min="1979-12-31">
                                @if($errors->has('birthday'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('birthday')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--Phone-->
                        <div class="form-group row">
                            <label for="inputPhone"
                                   class="col-12 col-md-2 col-form-label">{{__('language.Phone_Number')}}
                                <span class="align-middle text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="text"
                                       class="form-control @if($errors->has('phone')) is-invalid @endif"
                                       id="inputPhone"
                                       placeholder=""
                                       value="{{ old('phone') }}"
                                       name="phone" required>
                                @if($errors->has('phone'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('phone')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--Group id-->
                        <div class="form-group row">
                            <label class="col-12 col-md-2 col-form-label">{{__('language.Group')}}
                                <span class="align-middle text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <select class="form-control" id="exampleSelect1"
                                        name="group_id">
                                    @foreach($groups as $group)
                                        <option value="{{$group->id}}">{{$group->group_id}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <!--email-->
                        <div class="form-group row">
                            <label for="inputEmail"
                                   class="col-12 col-md-2 col-form-label">{{__('language.Email')}}
                                <span class="align-middle text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="email"
                                       class="form-control @if($errors->has('email')) is-invalid @endif"
                                       id="inputEmail"
                                       placeholder=""
                                       value="{{ old('email') }}"
                                       name="email" required>
                                @if($errors->has('email'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('email')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--password-->
                        <div class="form-group row">
                            <label for="inputPassword"
                                   class="col-12 col-md-2 col-form-label">{{__('language.password')}}
                                <span class="align-middle text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="password"
                                       class="form-control @if($errors->has('password')) is-invalid @endif"
                                       id="inputPassword"
                                       placeholder=""
                                       value="{{ old('password') }}"
                                       name="password" required>
                                @if($errors->has('password'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('password')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--password confim-->
                        <div class="form-group row">
                            <label for="inputPasswordConfim"
                                   class="col-12 col-md-2 col-form-label">{{__('language.password_confirm')}}
                                <span class="align-middle text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="password"
                                       class="form-control @if($errors->has('password_confim')) is-invalid @endif"
                                       id="inputPasswordConfim"
                                       placeholder=""
                                       value=""
                                       name="password_confim" required>
                                @if($errors->has('password_confim'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('password_confim')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--CMND-->
                        <div class="form-group row">
                            <label for="inputIdentifyCard"
                                   class="col-12 col-md-2 col-form-label">{{__('language.Identify_Card')}}
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="text"
                                       class="form-control @if($errors->has('identify_card')) is-invalid @endif"
                                       id="inputIdentifyCard"
                                       placeholder=""
                                       value="{{ old('identify_card') }}"
                                       name="identify_card">
                                @if($errors->has('identify_card'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('identify_card')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--Ngay cap-->
                        <div class="form-group row">
                            <label for="inputStartdateId"
                                   class="col-12 col-md-2 col-form-label">{{__('language.Startdate_ID')}}
                            </label>
                            <div class="col-12 col-md-10">
                                <input placeholder="dd-mm-yyyy"
                                       class="datepicker form-control @if($errors->has('startdate_id')) is-invalid @endif"
                                       id="inputStartdateId"
                                       value="{{ old('startdate_id') }}"
                                       name="startdate_id">
                                @if($errors->has('startdate_id'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('startdate_id')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--Noi cap-->
                        <div class="form-group row">
                            <label for="inputAddressId"
                                   class="col-12 col-md-2 col-form-label">{{__('language.Address_ID')}}
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="text"
                                       class="form-control @if($errors->has('address_id')) is-invalid @endif"
                                       id="inputAddressId"
                                       placeholder=""
                                       value="{{ old('address_id') }}"
                                       name="address_id">
                                @if($errors->has('address_id'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('address_id')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--Ho khau thuong chu-->
                        <div class="form-group row">
                            <label for="inputHousehold"
                                   class="col-12 col-md-2 col-form-label">{{__('language.Household')}}
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="text"
                                       class="form-control"
                                       id="inputHousehold"
                                       placeholder=""
                                       value="{{ old('household') }}"
                                       name="household">
                            </div>
                        </div>

                        <!--cong ty-->
                        <div class="form-group row">
                            <label for="inputCompany"
                                   class="col-12 col-md-2 col-form-label">{{ __('language.Company') }}
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="text"
                                       class="form-control"
                                       id="inputCompany"
                                       placeholder=""
                                       value="{{ old('company') }}"
                                       name="company">
                            </div>
                        </div>

                        <!--Github-->
                        <div class="form-group row">
                            <label for="inputGithub"
                                   class="col-12 col-md-2 col-form-label">{{__('language.Link_GitHub')}}
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="text"
                                       class="form-control"
                                       id="inputGithub"
                                       placeholder=""
                                       value="{{ old('link_github') }}"
                                       name="link_github">
                            </div>
                        </div>

                        <!--link_blog-->
                        <div class="form-group row">
                            <label for="inputBlog"
                                   class="col-12 col-md-2 col-form-label">{{__('language.Link_Blog')}}
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="text"
                                       class="form-control"
                                       id="inputBlog"
                                       placeholder=""
                                       value="{{ old('link_blog') }}"
                                       name="link_blog">
                            </div>
                        </div>
                        <!--Agilearn_ID-->
                        <div class="form-group row">
                            <label for="inputAgilearn"
                                   class="col-12 col-md-2 col-form-label">{{__('language.Agilearn_ID')}}
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="text"
                                       class="form-control"
                                       id="inputAgilearn"
                                       placeholder=""
                                       value="{{ old('agilearn_id') }}"
                                       name="agilearn_id">
                            </div>
                        </div>

                        <!--Ngay bat dau hoc-->
                        <div class="form-group row">
                            <label for="inputStartschool"
                                   class="col-12 col-md-2 col-form-label">{{__('language.Start_School')}}
                            </label>
                            <div class="col-12 col-md-10">
                                <input placeholder="dd-mm-yyyy"
                                       class="datepicker form-control @if($errors->has('start_school')) is-invalid @endif"
                                       id="inputStartschool"
                                       value="{{ date('d-m-Y') }}"
                                       name="start_school">
                            </div>
                        </div>

                        <!--ngay ket thuc du kien-->
                        <div class="form-group row">
                            <label for="inputExpected"
                                   class="col-12 col-md-2 col-form-label">{{__('language.Expected_End_Date')}}
                            </label>
                            <div class="col-12 col-md-10">
                                <input placeholder="dd-mm-yyyy"
                                       class="datepicker form-control @if($errors->has('expected_end_date')) is-invalid @endif"
                                       id="inputExpected"
                                       value="{{ old('expected_end_date') }}"
                                       name="expected_end_date">
                                @if($errors->has('expected_end_date'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('expected_end_date')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--avatar-->
                        <div class="form-group row">
                            <label for="inputAvatar"
                                   class="col-12 col-md-2 col-form-label">{{__('language.Avatar')}}
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="file" class="form-control-file" id="exampleInputFile"
                                       aria-describedby="fileHelp" name="image">
                                @if($errors->has('image'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('image')  }}
                                    </div>
                                @endif
                            </div>


                        </div>

                        <!--ghi chu-->
                        <div class="form-group row">
                            <label for="inputBlog"
                                   class="col-12 col-12 col-md-2 col-form-label">{{__('language.Note')}}
                            </label>
                            <div class="col-12 col-md-10">
                                <textarea class="col-12 col-md-8 col-lg-9 form-control editor"
                                          name="note"
                                          placeholder=""
                                          rows="4">{{  old('note') }}
                                </textarea>
                            </div>
                        </div>

                        <!--Button submit and back-->
                        <div class="row justify-content-end">
                            <button type="submit"
                                    class="btn btn-primary button-submit">{{__('language.create')}}</button>
                            <a href="{{ route('student.list') }}">
                                <input type="button"
                                       class=" btn btn-outline-secondary button-submit"
                                       value="{{__('language.Cancel')}}">
                            </a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection