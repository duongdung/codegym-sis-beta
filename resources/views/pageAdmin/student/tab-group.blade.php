@extends('layouts.app')

@section('head.title')
    {{ __('language.Group') }}
@endsection

@section('content')
    @if(count($student) === 0)
        <p class="text-center mt-5">{{__('language.Empty_Data')}}</p>
    @else
        <ol class="breadcrumb">
            <li class="breadcrumb-item text-muted">{{__('language.Student')}} {{ $student->first_name .' '. $student->last_name }}</li>
        </ol>
        <div class="main">
            <div class="row">
                @include('errors.success-evaluate')
                @include('errors.success')
                <div class="col-12 col-md-12 ">
                    <ul class=" nav nav-tabs responsive" role="tablist">

                        <!-- Tab thong tin ca nhan -->
                        <li class="nav-item">
                            <a class="nav-link text-secondary"
                               href="{{ route('student.details', [$student->id, $student->group_id]) }}">{{__('language.Personal')}}</a>
                        </li>
                    @can('view-list-students',Session::get('centerId'))
                        <!-- Tab lop hoc -->
                            <li class="nav-item">
                                <a class="nav-link text-secondary active"
                                   href="{{ route('student.group', $student->id) }}">{{__('language.Group')}}</a>
                            </li>
                    @endcan
                    @can('add-student-log', $student)
                        <!-- Tab nhat ky -->
                            <li class="nav-item">
                                <a class="nav-link text-secondary"
                                   href="{{ route('student.log', $student->id) }}">{{__('language.Logs')}}
                                </a>
                            </li>
                    @endcan
                    @can('crud-capacity',$student->group)
                        <!-- Tab bao cao -->
                            <li class="nav-item">
                                <a class="nav-link text-secondary"
                                   href="{{ route('student.capacity', [$student->id,$student->group_id]) }}"
                                >{{__('language.evaluate_capacity')}}</a>
                            </li>
                    @endcan
                    @can('view-student-score', $student)
                    <!-- Tab diem thi -->
                        <li class="nav-item">
                            <a class="nav-link text-secondary"
                               href="{{ route('student.tab.score', $student->id) }}"
                            >{{__('language.Test_Score')}}</a>
                        </li>
                    @endcan
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content" id="myTabContent">
                        <!-- Student details -->
                        <div class="border-list p-4">
                            <!--Tab lop hoc-->
                            @can('view-list-students',Session::get('centerId'))
                                <div class="row">
                                    <div class="col-12 col-md-4">
                                        <div class="table-responsive">
                                            <table id="myTable" class="table">
                                                <tbody>
                                                <tr>
                                                    <th class="border-top-0">{{__('language.Group')}}</th>
                                                    <td class="border-top-0">:
                                                        <a href="{{ route('group.detail', $student->group->id) }}">{{ $student->group->group_id }}</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th class="border-top-0">{{__('language.Description')}}</th>
                                                    <td class="border-top-0">
                                                        : {{ $student->group->group_description }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="border-top-0">{{__('language.Study_Program')}}</th>
                                                    <td class="border-top-0">: {{ $student->group->program->name_program }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="border-top-0">{{__('language.Start_Date')}}</th>
                                                    <td class="border-top-0">
                                                        : {{ $student->group->startdate_group->format('d/m/Y') }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="border-top-0">{{__('language.Expected_End_Date')}}</th>
                                                    <td class="border-top-0">
                                                        : {{ $student->group->expected_end_date_group->format('d/m/Y') }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            @endcan
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
