@extends('layouts.app')

@section('head.title')
    {{ __('language.Test_Score') }}
@endsection

@section('content')
    @if(count($student) === 0)
        <p class="text-center mt-5">{{__('language.Empty_Data')}}</p>
    @else
        <ol class="breadcrumb">
            <li class="breadcrumb-item text-muted">{{__('language.Student')}} {{ $student->first_name .' '. $student->last_name }}</li>
            <li class="breadcrumb-item text-muted">{{__('language.Test_Score')}}</li>
        </ol>
        <div class="main">
            <div class="row">
                <div class="col-12 col-md-12 ">
                    <ul class=" nav nav-tabs responsive" role="tablist">
                        <!-- Tab thong tin ca nhan -->
                        <li class="nav-item">
                            <a class="nav-link text-secondary"
                               href="{{ route('student.details', [$student->id, $student->group_id]) }}">{{__('language.Personal')}}</a>
                        </li>
                    @can('view-list-students',Session::get('centerId'))
                        <!-- Tab lop hoc -->
                            <li class="nav-item">
                                <a class="nav-link text-secondary"
                                   href="{{ route('student.group', $student->id) }}">{{__('language.Group')}}</a>
                            </li>
                    @endcan
                    @can('add-student-log', $student)
                        <!-- Tab nhat ky -->
                            <li class="nav-item">
                                <a class="nav-link text-secondary"
                                   href="{{ route('student.log', $student->id) }}">{{__('language.Logs')}}
                                </a>
                            </li>
                    @endcan
                    @can('crud-capacity',$student->group)
                        <!-- Tab bao cao -->
                            <li class="nav-item">
                                <a class="nav-link text-secondary"
                                   href="{{ route('student.capacity',[$student->id,$student->group_id] ) }}"
                                >{{__('language.evaluate_capacity')}}</a>
                            </li>
                    @endcan
                    @can('view-student-score', $student)
                        <!-- Tab diem thi -->
                            <li class="nav-item">
                                <a class="nav-link text-secondary active"
                                   href="{{ route('student.tab.score', $student->id) }}"
                                >{{__('language.Test_Score')}}</a>
                            </li>
                    @endcan
                    <!-- Button edit, delete -->
                        @can('curd-student', Session::get('centerId'))
                            @include('subViews.button-edit-delete', ['routeEdit'=>'student.edit', 'routeDelete'=>'student.delete','id' => $student->id])
                        @endcan
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content" id="myTabContent">
                        <div class="border-list p-4">
                            @if(Session::has('add_score_success'))
                                <p class="text-success no-margin">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    {{Session::get('add_score_success')}}
                                </p>
                            @endif
                            @if(Session::has('edit_score_success'))
                                <p class="text-success no-margin">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    {{Session::get('edit_score_success')}}
                                </p>
                            @endif
                            @if(Session::has('delete_score_success'))
                                <p class="text-success no-margin">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    {{Session::get('delete_score_success')}}
                                </p>
                            @endif
                            <form action="{{ route('student.tab.score', $student->id) }}"
                                  method="get"
                                  enctype="multipart/form-data"
                                  class="pt-3">
                                <!--course-->
                                <div class="form-group">
                                    <label class="col-form-label">{{__('language.Choose_Course')}}</label>
                                    @if(isset($courses))
                                        <select class="js-example-basic-single" style="width: 100%" name="selectCourse"
                                                onchange="this.form.submit()">
                                            <option value="">{{__('language.Choose_Course')}}</option>
                                            @foreach($courses as $key=>$course)
                                                @if (isset($selectCourse))
                                                    <option value="{{ $course->id }}" {{ ($course->id === $selectCourse->id) ? 'selected' : '' }} >{{ $course->course_name }}</option>
                                                @else
                                                    <option value="{{ $course->id }}">{{ $course->course_name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    @else
                                        <p class="text-danger">{{__('language.Empty_Data')}}</p>
                                    @endif
                                </div>
                                @can('crud-student-score', Session::get('centerId'))
                                    @if (isset($selectCourse))
                                        <div class="col-12 col-md-7">
                                            <a href="{{ route('student.add.score', [$student->id, $selectCourse->id]) }}">
                                                <i class="fa fa-plus"
                                                   aria-hidden="true"></i> {{__('language.Create_Test_Score')}}
                                            </a>
                                        </div>
                                    @endif
                                @endcan
                            </form>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th style="width: 5%" class="border-top-0 text-secondary text-center">#</th>
                                        <th style="width: 20%"
                                            class="border-top-0 text-secondary">{{__('language.Exam_Date')}}</th>
                                        <th style="width: 20%"
                                            class="border-top-0 text-secondary">{{__('language.Course')}}</th>
                                        <th class="border-top-0 text-secondary">{{__('language.Test_Score')}}</th>
                                        <th style="width: 5%" class="border-top-0 text-secondary"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if (isset($exams))
                                        @foreach($exams as $key=>$exam)
                                            <tr>
                                                <td class="border-top-0 text-secondary text-center">{{++$key}}</td>
                                                <td class="border-top-0 ">{{ ($exam->date) ? $exam->date->format('d/m/Y') : '' }}</td>
                                                <td class="border-top-0 ">{{$selectCourse->course_name}}</td>
                                                <td class="border-top-0 ">
                                                    @foreach($exam->students as $score)
                                                        @if ($score->id === $student->id)
                                                            {{ $score->pivot->score }}
                                                        @endif
                                                    @endforeach
                                                </td>
                                                <td class="border-top-0 ">
                                                    <!-- Button edit, delete -->
                                                    @can('crud-student-score', Session::get('centerId'))
                                                        <div class="ml-auto">
                                                            <a class="pt-2 pl-3"
                                                               href="{{ route('student.edit.score' ,[$student->id, $exam->id]) }}">
                                                                <i class="fa fa-pencil-square-o"></i>
                                                            </a>
                                                            <a class="pt-2 pl-3"
                                                               href="{{ route('student.delete.score',[$student->id, $exam->id]) }}">
                                                                <i class="fa fa-times text-danger"></i>
                                                            </a>
                                                        </div>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="h6 text-muted">
                                                <i class="fa fa-info-circle"></i>
                                                {{__('language.Empty_Data')}}
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

