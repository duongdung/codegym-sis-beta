@extends('layouts.app')

@section('head.title')
    {{ __('language.detail') }}
@endsection

@section('content')
    @if(count($student) === 0)
        <p class="text-center mt-5">{{__('language.Empty_Data')}}</p>
    @else
        <ol class="breadcrumb">
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('student.list') }}">{{__('language.Student_List')}}</a>
            </li>
            <li class="breadcrumb-item text-muted">{{__('language.Student')}} {{ $student->first_name .' '. $student->last_name }}</li>
        </ol>
        <div class="main">
            @include('errors.success-evaluate')
            @include('errors.success')
            <div class="row">
                <div class="col-12 col-md-12 ">
                    <ul class=" nav nav-tabs responsive" role="tablist">

                        <!-- Tab thong tin ca nhan -->
                        <li class="nav-item">
                            <a class="nav-link text-secondary active"
                               href="{{ route('student.details', [$student->id, $student->group_id]) }}">{{__('language.Personal')}}</a>
                        </li>
                    @can('view-list-students',Session::get('centerId'))
                        <!-- Tab lop hoc -->
                            <li class="nav-item">
                                <a class="nav-link text-secondary"
                                   href="{{ route('student.group', $student->id) }}">{{__('language.Group')}}</a>
                            </li>
                    @endcan
                    @can('add-student-log', $student)
                        <!-- Tab nhat ky -->
                            <li class="nav-item">
                                <a class="nav-link text-secondary"
                                   href="{{ route('student.log', $student->id) }}">{{__('language.Logs')}}
                                </a>
                            </li>
                    @endcan
                    @can('crud-capacity',$group)
                        <!-- Tab bao cao -->
                            <li class="nav-item">
                                <a class="nav-link text-secondary"
                                   href="{{ route('student.capacity',[$student->id,$group->id]) }}"
                                >{{__('language.evaluate_capacity')}}</a>
                            </li>
                    @endcan
                    @can('view-student-score', $student)
                    <!-- Tab diem thi -->
                        <li class="nav-item">
                            <a class="nav-link text-secondary"
                               href="{{ route('student.tab.score', $student->id) }}"
                            >{{__('language.Test_Score')}}</a>
                        </li>
                    @endcan
                    @can('students', Session::get('centerId'))
                        <!-- Tab diem thi -->
                            <li class="nav-item">
                                <a class="nav-link text-secondary"
                                   href="{{ route('student.tab.score', $student->id) }}"
                                >{{__('language.Test_Score')}}</a>
                            </li>
                    @endcan
                    <!-- Button edit, delete -->
                        @can('curd-student', Session::get('centerId'))
                            @include('subViews.button-edit-delete', ['routeEdit'=>'student.edit', 'routeDelete'=>'student.delete','id' => $student->id])
                        @endcan
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content" id="myTabContent">
                        <!-- Student details -->
                        <div class="border-list p-4">
                            <div class="row flex-md-row-reverse">
                                <div class="col-12 col-md-8">
                                    <div class="avatar d-inline-block">
                                        <img id="img-preview" class="d-block img-thumbnail img-responsive mb-2"
                                             src="{{ ($student->image) ? asset('storage/'.$student->image) : asset('img/no-avatar.jpg') }}">
                                        <span class="d-block text-center">{{__('language.Avatar')}}</span>
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="table-responsive">
                                        <table id="myTable" class="table">
                                            <tbody>
                                            <tr>
                                                <th class="border-top-0">{{__('language.Student_Id')}}</th>
                                                <td class="border-top-0">: {{ $student->student_id }}</td>
                                            </tr>
                                            <tr>
                                                <th class="border-top-0">{{__('language.first_name')}}</th>
                                                <td class="border-top-0">: {{ $student->first_name }}</td>
                                            </tr>
                                            <tr>
                                                <th class="border-top-0">{{__('language.last_name')}}</th>
                                                <td class="border-top-0">: {{ $student->last_name }}</td>
                                            </tr>
                                            <tr>
                                                <th class="border-top-0">{{__('language.Phone_Number')}}</th>
                                                <td class="border-top-0">: {{ $student->phone }}</td>
                                            </tr>
                                            <tr>
                                                <th class="border-top-0">{{__('language.Email')}}</th>
                                                <td class="border-top-0">: {{ $student->email }}</td>
                                            </tr>
                                            <tr>
                                                <th class="border-top-0">{{__('language.Birthday')}}</th>
                                                <td class="border-top-0">
                                                    : {{ ($student->birthday) ? $student->birthday->format('d/m/Y') : __('language.No_Data') }}</td>
                                            </tr>
                                            <tr>
                                                <th class="border-top-0">{{__('language.Identify_Card')}}</th>
                                                <td class="border-top-0">
                                                : {{ ($student->identify_card) ? $student->identify_card : __('language.No_Data') }}</td>
                                            </tr>
                                            <tr>
                                                <th class="border-top-0">{{__('language.Startdate_ID')}}</th>
                                                <td class="border-top-0">
                                                    : {{ ($student->startdate_id) ? $student->startdate_id->format('d/m/Y') : __('language.No_Data') }}</td>
                                            </tr>
                                            <tr>
                                                <th class="border-top-0">{{__('language.Address_ID')}}</th>
                                                <td class="border-top-0">
                                                    : {{ ($student->address_id) ? $student->address_id : __('language.No_Data') }}</td>
                                            </tr>
                                            <tr>
                                                <th class="border-top-0">{{__('language.Household')}}</th>
                                                <td class="border-top-0">:
                                                   {{ ($student->household) ? $student->household : __('language.No_Data') }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="border-top-0">{{__('language.Start_School')}}</th>
                                                <td class="border-top-0">
                                                    : {{ ($student->start_school) ? $student->start_school->format('d/m/Y') : __('language.No_Data') }}</td>
                                            </tr>
                                            <tr>
                                                <th class="border-top-0">{{__('language.Expected_End_Date')}}</th>
                                                <td class="border-top-0">
                                                    : {{ ($student->expected_end_date) ? $student->expected_end_date->format('d/m/Y') : __('language.No_Data') }}</td>
                                            </tr>
                                            <tr>
                                                <th class="border-top-0">{{__('language.Actual_End_Date')}}</th>
                                                <td class="border-top-0">:
                                                    {{ ($student->actual_end_date) ? $student->actual_end_date : __('language.No_Data') }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="border-top-0">{{__('language.Company')}}</th>
                                                <td class="border-top-0">:
                                                    {{ ($student->company) ? $student->company : __('language.No_Data') }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="border-top-0">{{__('language.Link_GitHub')}}</th>
                                                <td class="border-top-0">:
                                                    {{ ($student->link_github) ? $student->link_github : __('language.No_Data') }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="border-top-0">{{__('language.Link_Blog')}}</th>
                                                <td class="border-top-0">:
                                                    {{ ($student->link_blog) ? $student->link_blog : __('language.No_Data') }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="border-top-0">{{__('language.Agilearn')}}</th>
                                                <td class="border-top-0">:
                                                    @if(empty($student->agilearn_id))
                                                        {{ __('language.No_Data') }}
                                                    @else
                                                        <a href="{{config('app.agilearn').'user/profile.php?id='.$student->agilearn_id}}" target="_blank">{{__('language.Agilearn')}}</a>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="border-top-0">{{__('language.Note')}}</th>
                                                <td class="border-top-0">:
                                                    {{ ($student->note) ? $student->note : __('language.No_Data') }}
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @can('curd-student',Session::get('centerId'))
                            <div class="col-10 col-md-12  pt-4 pb-4">
                                <div class="row justify-content-end">
                                    <a href="{{ route('student.edit',$student->id) }}">
                                        <input type="button"
                                               class=" btn btn-primary button-submit"
                                               value="{{__('language.change_info')}}">
                                    </a>
                                    <a href="{{ route('student.list') }}">
                                        <input type="button"
                                               class=" btn btn-outline-secondary button-submit"
                                               value="{{__('language.Cancel')}}">
                                    </a>
                                </div>
                            </div>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

