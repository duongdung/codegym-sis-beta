@extends('layouts.app')
@section('head.title')
    {{__('language.Student_List')}}
@endsection
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted">{{__('language.Student_List')}}</li>
    </ol>
    @if(Session::has('add-new-success'))
        <p class="text-success">
            <i class="fa fa-check" aria-hidden="true"></i>
            {{ Session::get('add-new-success')}}
        </p>
    @endif
    <div class="row">
        <div class="col-6">
            <a class="btn btn-outline-primary" href="{{ route('student.create') }}">
                <i class="fa fa-plus" aria-hidden="true"></i> {{__('language.Add')}}
            </a>
        </div>
        <div class="col-6">
            @if(count($students) == 0)
            @else
                <form class="form-inline"
                      action="{{ route('student.search') }}"
                      method="GET">
                    {{ csrf_field() }}
                    <input class="form-control  col-9" type="text" name="searchStudent" value=""
                           placeholder="{{__('language.search')}}"
                           aria-label="Search">
                    <button class="btn btn-outline-secondary my-2 my-sm-0 col-3"
                            type="submit" value="searchStudent">{{__('language.search')}}</button>
                </form>
            @endif
        </div>
        <!-- Result -->
        <div class="col-12">
            @if(Session::has('no-result'))
                <p class="text-muted">
                    {{ Session::get('no-result')}}
                </p>
            @endif
            @if(Session::has('have-result'))
                <p class="text-muted">
                    {{ Session::get('have-result')}}
                </p>
            @endif
        </div><!-- End Result -->
    </div>
    <div class="main p-3">
        <div class="row">
            <div class="col-12 col-md-12 table-responsive no-padding">
                <table id="myTable" class="table table-hover">
                    <thead class="thead-table">
                    <tr>
                        <th class=" text-center">#</th>
                        <th>{{__('language.Student_Id')}}</th>
                        <th>{{__('language.full_name')}}</th>
                        <th>{{__('language.Phone_Number')}}</th>
                        <th>{{__('language.Email')}}</th>
                        <th>{{__('language.Agilearn')}}</th>
                    </tr>
                    </thead>
                    @if(count($students) == 0)
                        <p class="text-center">{{ __('language.No_Data') }}</p>
                    @else
                        <tbody>
                        @foreach($students as $key=> $student)
                            <tr>
                                <td class="text-center">{{++$key}}</td>
                                <td><a
                                            href="{{ route('student.details', [$student->id, $student->group_id]) }}">{{ $student->student_id }}</a>
                                </td>
                                <td><a
                                            href="{{ route('student.details', [$student->id, $student->group_id]) }}">{{ $student->first_name.' '.$student->last_name }} </a>
                                </td>
                                <td>{{$student->phone}}</td>
                                <td>{{$student->email}}</td>
                                <td>
                                    @if(isset($student->agilearn_id))
                                        <a href="{{config('app.agilearn').'user/profile.php?id='.$student->agilearn_id}}"
                                           target="_blank">{{__('language.Agilearn')}}</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    @endif
                </table>
                <div class="pagination" style="margin-top: 10px;float: right">
                    {{ $students->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection

