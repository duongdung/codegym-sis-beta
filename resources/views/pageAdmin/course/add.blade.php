@extends('layouts.app')

@section('head.title')
    {{ __('language.Course_Add') }}
@endsection

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('course.show')}}">{{ __('language.Course') }}</a></li>
        <li class="breadcrumb-item text-muted">{{ __('language.Course_Add') }}</li>
    </ol>

    <div class="main">
        <div class="row">
            <div class="col-12">
                <div class="p-4">
                    <form class="form-horizontal" action="{{ route('course.store') }}"
                          method="POST">
                    {{ csrf_field() }}
                    @include('errors.success')
                    <!--Course Code-->
                        <div class="form-group row">
                            <p class="col-12 text-muted p-2 text-note">
                                {{ __('language.Fields_with') }}
                                <span class="text-danger">*</span>
                                {{ __('language.Are_required') }}
                            </p>
                            <label class="col-12 col-md-2 col-form-label">{{ __('language.Course_Code') }}
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <input class="form-control @if($errors->has('course_code')) is-invalid @endif"
                                       type="text" value="{{ old('course_code') }}" name="course_code"
                                       required>
                                @if($errors->has('course_code'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('course_code')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--Name Course-->
                        <div class="form-group row">
                            <label class="col-12 col-md-2 col-form-label">{{ __('language.Course_Name') }}
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <input class="form-control @if($errors->has('course_name')) is-invalid @endif"
                                       type="text" value="{{ old('course_name') }}" placeholder=""
                                       name="course_name"
                                       required>
                                @if($errors->has('course_name'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('course_name')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--Description Course-->
                        <div class="form-group row">
                            <label class="col-12 col-md-2 col-form-label">{{__('language.Note')}}
                            </label>
                            <div class="col-12 col-md-10">
                                <textarea class="col-12 col-md-8 col-lg-9 form-control editor" name="note"
                                          placeholder=""
                                          rows="4">{{  old('note') }}</textarea>
                            </div>
                        </div>

                        <!--Button submit and back-->
                        <div class="float-right">
                            <button type="submit"
                                    class="btn btn-primary button-submit">{{__('language.create')}}</button>
                            <a href="{{ route('course.show') }}">
                                <input type="button"
                                       class="button-submit btn btn-outline-secondary"
                                       value="{{__('language.Cancel')}}">
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection