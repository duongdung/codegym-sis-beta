@extends('layouts.app')
@section('head.title')
    {{ __('language.Delete_Course') }}
@endsection
@section('content')

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-12 col-md-8" style="height: 300px;border-radius: 5px;">
                    <div class="delete" style="padding-top: 3em" >
                        <div class="name-program text-center">
                            <h5 class="p-4 text-danger">{{__('language.Do_you_deleteCourse'). $course->course_name }} ?</h5>
                        </div>

                        <div class="form-group row justify-content-md-center">
                            <form action="{{ route('course.destroy',$course->id) }}" method="post">
                                {{csrf_field()}}
                                <div class="col-6 col-md-3">
                                    <button type="submit" class="btn btn-danger btn-lg">{{__('language.Delete')}}</button>
                                </div>
                            </form>
                            <div class="col-6 col-md-3 ">
                                <a href="{{ route('course.show') }}"><button type="button" class="btn btn-outline-secondary btn-lg">{{__('language.Cancel')}}</button></a>
                            </div>

                        </div>
                    </div>
                {{--@endif--}}
            </div>
        </div>
    </div>

@endsection