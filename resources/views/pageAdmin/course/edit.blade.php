@extends('layouts.app')
@section('head.title')
    {{ __('language.Edit_Course') }}
@endsection
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('course.show') }}">{{ __('language.Course') }} </a></li>
        <li class="breadcrumb-item text-muted"> {{ __('language.Edit_Course') }}</li>
    </ol>

    <div class="main">
        <form action="{{route('course.update',$course->id)}}" method="POST">
            {{ csrf_field() }}
            @include('errors.success')
            <div class="main-content">
                <div class="">
                    <div class="">
                        <div class="">
                            <!--Course Code-->
                            <div class="col-10 col-md-10">
                                <div class="form-group row">
                                    <p class="col-12 text-muted p-2 text-note">
                                        {{ __('language.Fields_with') }}
                                        <span class="text-danger">*</span>
                                        {{ __('language.Are_required') }}
                                    </p>
                                    <label for="courseCode"
                                           class="col-md-4 col-lg-3 col-form-label">{{ __('language.Course_Code') }}
                                        <span class="align-middle text-danger">*</span>
                                    </label>
                                    <div class="col-md-8 col-lg-9 no-padding">
                                        <input type="text"
                                               class="form-control @if($errors->has('course_code')) is-invalid @endif"
                                               id="courseCode"
                                               placeholder=""
                                               value="{{ $course->course_code }}"
                                               name="course_code" required>
                                        @if($errors->has('course_code'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('course_code')  }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <!--Course Name-->
                            <div class="col-10 col-md-10">
                                <div class="form-group row">
                                    <label for="courseName"
                                           class="col-md-4 col-lg-3 col-form-label">{{ __('language.Course_Name') }}
                                        <span class="align-middle text-danger">*</span>
                                    </label>
                                    <div class="col-md-8 col-lg-9 no-padding">
                                        <input type="text"
                                               class="form-control @if($errors->has('course_name')) is-invalid @endif"
                                               id="courseName"
                                               placeholder=""
                                               value="{{ $course->course_name }}"
                                               name="course_name" required>
                                        @if($errors->has('course_name'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('course_name')  }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <!--ghi chu-->
                            <div class="col-10 col-md-10">
                                <div class="form-group row">
                                    <label for="inputBlog"
                                           class="col-12 col-md-4 col-lg-3 col-form-label">{{__('language.Description')}}
                                    </label>
                                    <textarea class="col-12 col-md-8 col-lg-9 form-control editor" name="note"
                                              placeholder=""
                                              rows="4">{{  $course->description }}
                                    </textarea>
                                </div>
                            </div>

                            <!--Button submit and back-->
                            <div class="col-10 col-md-10">
                                <div class="row justify-content-end">
                                    <button type="submit"
                                            class="btn btn-primary button-submit">{{__('language.Save')}}</button>
                                    <a href="{{ route('course.show') }}">
                                        <input type="button"
                                               class=" btn btn-outline-secondary button-submit"
                                               value="{{__('language.Cancel')}}">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection