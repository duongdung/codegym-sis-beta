@extends('layouts.app')

@section('head.title')
    {{__('language.List_Course')}}
@endsection

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted">
            <i class="" aria-hidden="true"></i> {{__('language.List_Course')}}
        </li>
    </ol>
    @include('errors.success')
    <a class="btn btn-outline-primary" href="{{ route('course.create') }}">
        <i class="fa fa-plus" aria-hidden="true"></i> {{__('language.Add')}}
    </a>
    <div class="main p-3">
        <div class="row">

                <div class="col-12 col-md-12 table-responsive no-padding">
                    <table id="myTable" class="table table-hover">
                        <thead class="thead-table">
                        <tr>
                            <th class="text-center">#</th>
                            <th >{{__('language.Course_Code')}}</th>
                            <th >{{__('language.Course_Name')}}</th>
                            <th >{{__('language.Description')}}</th>
                            <th ></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($courses as $key=> $course)
                            <tr>
                                <td class="text-center">{{++$key}}</td>
                                <td >{{ $course->course_code }}</td>
                                <td >{{ $course->course_name }}</td>
                                <td >{{$course->description }}</td>
                                <td class="text-center ">
                                    @include('subViews.button-edit-delete', ['routeEdit'=>'course.edit', 'routeDelete'=>'course.delete','id' => $course->id])
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
        </div>
    </div>

@endsection

