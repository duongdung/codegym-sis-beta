@extends('layouts.app')

@section('head.title')
    {{__('language.Edit_program')}}
@endsection

@section('content')


   <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">{{ __('language.Study_Program') }} </a></li>
       <li class="breadcrumb-item text-muted">{{ $program->name_program }} </li>
        <li class="breadcrumb-item active">{{__('language.Edit_program')}} </li>
    </ol>

   <div class="main">
       <form class="form-horizontal" action="{{ route('program.update',$program->id) }}"
             method="POST">
       {{ csrf_field() }}
           <div class="main-content">
               <div class="row">
                   <div class="col-12 col-md-8 col-lg-9">
                       <div class="col-12 col-md-12">
                           <div class="form-group row">
                               <label for="input"
                                      class="col-md-4 col-lg-3 col-form-label">{{__('language.Name_Program')}}
                                   <span class="align-middle text-danger">*</span>
                               </label>
                               <div class="col-md-8 col-lg-9 no-padding">
                                   <input type="text"
                                          class=" form-control @if($errors->has('name_study_program')) is-invalid @endif"
                                          id="name_study_program"
                                          placeholder=""
                                          value="{{ $program->name_program }}"
                                          name="name_study_program" required>
                                   @if($errors->has('name_study_program'))
                                       <div class="invalid-feedback">
                                           {{ $errors->first('name_study_program')  }}
                                       </div>
                                   @endif
                               </div>
                           </div>
                       </div>
                       <div class="col-12 col-md-12">
                           <div class="form-group row">
                               <label for="input"
                                      class="col-md-4 col-lg-3 col-form-label">{{__('language.Length_of_Program')}}
                                   <span class="align-middle text-danger">*</span>
                               </label>
                               <div class="col-md-8 col-lg-9 no-padding">
                                   <input type="text"
                                          class=" form-control @if($errors->has('length_of_program')) is-invalid @endif"
                                          id="first_name"
                                          placeholder=""
                                          value="{{ $program->length_of_program }}"
                                          name="length_of_program" required>
                                   @if($errors->has('length_of_program'))
                                       <div class="invalid-feedback">
                                           {{ $errors->first('length_of_program')  }}
                                       </div>
                                   @endif
                               </div>
                           </div>
                       </div>

                       <div class="col-12 col-md-12">
                           <div class="form-group row">
                               <label for="input"
                                      class="col-12 col-md-4 col-lg-3 col-form-label">{{__('language.Note')}}
                               </label>
                               <textarea class="col-12 col-md-8 col-lg-9 form-control editor" name="study_program_description"
                                         placeholder=""
                                         rows="4">{{ $program->program_description }}</textarea>
                           </div>
                       </div>
                       <!--Button submit and back-->
                       <div class="col-12 col-md-12">
                           <div class="row justify-content-end">
                               <button type="submit"
                                       class="btn btn-primary button-submit">{{__('language.Save')}}</button>
                               <a href="{{ route('program.list') }}">
                                   <input type="button"
                                          class=" btn btn-outline-secondary button-submit"
                                          value="{{__('language.Cancel')}}">
                               </a>
                           </div>
                       </div>
                   </div>
               </div>

           </div>
       </form>

   </div>



@endsection