@extends('layouts.app')

@section('head.title')
    {{__('language.Study_Program_List')}}
@endsection

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted ">{{__('language.Study_Program_List')}}</li>
    </ol>
    @include('errors.success')
    <a class="btn btn-outline-primary" href="{{ route('program.add') }}">
        <i class="fa fa-plus" aria-hidden="true"></i> {{__('language.Add')}}
    </a>
    <div class="main p-3">
        <div class="row">
            <div class="col-12 col-md-12 table-responsive no-padding">
                <table id="myTable" class="table table-hover">
                    <thead class="thead-table">
                    <tr>
                        <th >#</th>
                        <th >{{__('language.Name_Program')}}</th>
                        <th >{{__('language.Description_Program')}}</th>
                        <th >{{__('language.Length_of_Program')}}</th>
                        <th ></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($programs as $key => $program)
                        <tr>
                            <td >{{ ++$key }}</td>
                            <td >
                                <a href="{{ route('program.detail',$program->id) }}">{{ $program->name_program }}</a>
                            </td>
                            <td >{{ $program->program_description }}</td>
                            <td >{{ $program->length_of_program }}</td>
                            @can('crud-center')
                                <td class="text-center">
                                    @include('subViews.button-edit-delete', ['routeEdit'=>'program.edit', 'routeDelete'=>'program.delete','id' => $program->id])
                                </td>
                            @endcan
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

