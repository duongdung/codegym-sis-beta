@extends('layouts.app')
@section('head.title')
    {{ __('language.Delete_program') }}
@endsection
@section('content')

    <div class="container">

        <div class="row justify-content-md-center">
            <div class="col-12 col-md-8" style="height: 300px;border-radius: 5px;">

                @if(count($studyProgram->groups) > 0)
                    <div class="col-12 col-md-12 no-delete pt-5 text-center">
                        <h4><p class="text-danger">{{__('language.No_Delete')}}</p></h4>
                    </div>
                    <div class="support">
                        <p>{{__('language.Delete_Program')}}</p>
                    </div>
                    <div class="col-12 col-md-12 text-center">
                        <a href="{{route( 'program.detail',$studyProgram->id )}}" class="btn btn-outline-secondary btn-lg">{{__('language.Cancel')}}</a>
                    </div>
                @else
                <div class="delete" style="padding-top: 3em" >
                    <div class="name-program text-center">
                        <p style="padding-top: 10px;"><h5 class="text-danger">{{ __('language.Do_you_deleteProgram') }}:
                            <strong>{{ $studyProgram->name_program }}?</strong></h5></p>
                    </div>
                    <div class="form-group row justify-content-md-center">
                        <form action="{{ route('program.destroy',$studyProgram->id)}}" method="post">
                            {{ csrf_field() }}
                        <div class="col-6 col-md-3">
                            <button type="submit" class="btn btn-danger btn-lg">{{__('language.Delete')}}</button>
                        </div>
                        </form>
                        <div class="col-6 col-md-3 ">
                            <a href="{{ route('program.list') }}"><button type="button" class="btn btn-outline-secondary btn-lg">{{__('language.Cancel')}}</button></a>
                        </div>

                    </div>
                </div>
                 @endif
            </div>
        </div>
    </div>

@endsection