@extends('layouts.app')

@section('head.title')
    {{ __('language.program_detail') }}
@endsection


@section('content')
    
   <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted"><a href="{{ route('program.list') }}">{{ __('language.Study_Program') }} </a></li>
        <li class="breadcrumb-item text-muted">{{ $program->name_program }}</li>
    </ol>



   <div class="pt-4">
       <!--Tiêu đề -->
       <div class="row">
           <div class="col-12 col-sm-12 col-md-12 col-lg-12 student-content">

               <div class="col-12 col-md-12">
                   <div class="row">
                       <div class="col-10 col-md-10">
                           {{__('language.groups_into_program') }}
                       </div>
                       <div class="col-2 col-md-2">
                           @include('subViews.button-edit-delete', ['routeEdit'=>'program.edit', 'routeDelete'=>'program.delete','id' => $program->id])
                       </div>
                   </div>

               </div>

               <div class="table-responsive pt-2">
                   <table id="myTable" class="table table-hover">
                       <thead>
                       <tr>
                           <th class="border-top-0">#</th>
                           <th class="border-top-0">{{__('language.Group_ID')}}</th>
                           <th class="border-top-0">{{__('language.Description')}}</th>
                           <th class="border-top-0">{{__('language.Start_Date')}}</th>
                           <th class="border-top-0">{{__('language.Expected_End_Date')}}</th>
                       </tr>
                       </thead>
                       <tbody>
                       @if(count($program->groups) == 0)
                           <tr>
                               <td class="text-center border-top-0" colspan="5">{{ __('language.No_Data') }}</td>
                           </tr>
                       @endif
                       @foreach($groupPrograms as $key => $groupProgram)
                           <tr>
                               <td class="border-top-0">{{ ++$key }}</td>
                               <td class="border-top-0"><a
                                           href="{{ route('group.detail', $groupProgram->id) }}">{{$groupProgram->group_id}}</a></td>
                               <td class="border-top-0">{{ $groupProgram->group_description}}</td>
                               <td class="border-top-0">{{ $groupProgram->startdate_group->format('d-m-Y') }}</td>
                               <td class="border-top-0">{{ $groupProgram->expected_end_date_group->format('d-m-Y') }}</td>
                           </tr>
                       @endforeach
                       </tbody>
                   </table>
                   <div class="pagination" style="margin-top: 10px;float: right">
                       {{ $groupPrograms->links() }}
                   </div>
               </div>

           </div>
       </div>
   </div>
@endsection