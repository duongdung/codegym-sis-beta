@extends('layouts.app')

@section('head.title')
    {{ __('language.detail') }}
@endsection
@section('content')
<ol class="breadcrumb">
    @if($user->id !== Auth::id())
        <li class="breadcrumb-item"><a href="{{ route('user.list') }}">{{__('language.List_Employees')}}</a></li>
    @endif
    <li class="breadcrumb-item text-muted">{{ $user->first_name }} {{ $user->last_name }}</li>
    <li class="breadcrumb-item text-muted">{{__('language.detail')}}</li>
</ol>

<div class="col-12 col-sm-12 col-md-12 col-lg-12">
    @include('errors.success')
    <div class="row justify-content-center">

        <div class="col-12 col-sm-10 col-md-6">
            <div class="profile pt-5">
                <div class="card">
                    <div class="card-header">
                        <h5 class="no-margin">{{__('language.detail')}}</h5>
                    </div>
                    <div class="card-block mt-3 mb-3">
                        <div class="col-12 col-md-12 row">
                            <div class="col-12 col-sm-5 col-md-4 tab-pane-label"><strong>{{__('language.full_name')}}</strong></div>
                            <div class="col-12 col-sm-7 col-md-8">{{ $user->first_name }} {{ $user->last_name }}</div>
                        </div>
                        <div class="col-12 col-md-12 row pt-2">
                            <div class="col-12 col-sm-5 col-md-4 tab-pane-label"><strong>{{__('language.id')}}</strong></div>
                            <div class="col-12 col-sm-7 col-md-8">{{ $user->id }}</div>
                        </div>
                        <div class="col-12 col-md-12 row pt-2">
                            <div class="col-12 col-sm-5 col-md-4 tab-pane-label"><strong>{{__('language.date_create')}}</strong></div>
                            <div class="col-12 col-sm-7 col-md-8">{{ $user->created_at->format('d-m-Y') }}</div>
                        </div>
                        <div class="col-12 col-md-12 row pt-2">
                            <div class="col-12 col-sm-5 col-md-4 tab-pane-label"><strong>Email</strong></div>
                            <div class="col-12 col-sm-7 col-md-8">{{ $user->email }}</div>
                        </div>
                        <div class="col-12 col-md-12 row pt-2">
                            <div class="col-12 col-sm-5 col-md-4 tab-pane-label"><strong>{{__('language.role')}}</strong></div>
                            <div class="col-12 col-sm-7 col-md-8">
                                @foreach($user->roles as $role)
                                    @if(count($user->roles) == 1)
                                        {{ $role->name}}
                                    @else
                                        {{ $role->name . ',' }}
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-10 col-md-12  pt-4 pb-4">
                    <div class="row justify-content-end">
                        <a href="{{route('user.update',$user->id) }}">
                            <input type="button"
                                   class=" btn btn-primary button-submit"
                                   value="{{__('language.change_info')}}">
                        </a>
                        <a href="{{ route('user.list') }}">
                            <input type="button"
                                   class=" btn btn-outline-secondary button-submit"
                                   value="{{__('language.Cancel')}}">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
