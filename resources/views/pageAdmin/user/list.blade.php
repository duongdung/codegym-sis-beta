@extends('layouts.app')

@section('head.title')
    {{__('language.List_Employees')}}
@endsection

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted"> {{__('language.List_Employees')}}</li>
    </ol>
    @include('errors.success')
    @can('crud-user', Session::get('centerId'))
        <a class="btn btn-outline-primary" href="{{ route('user.create') }}">
            <i class="fa fa-plus" aria-hidden="true"></i> {{__('language.Add')}}
        </a>
    @endcan
    <div class="main p-3">
        <div class="row">
            <div class="col-12 col-md-12 no-padding">
                @can('crud-user', Session::get('centerId'))
                    <nav class="nav nav-tabs responsive" id="myTab" role="tablist">
                        <a class="nav-item nav-link text-secondary active" id="nav-profile-tab" data-toggle="tab"
                           href="#nav-user"
                           role="tab"
                           aria-controls="nav-profile">
                            <i class="fa fa-user-circle-o" aria-hidden="true"></i> {{__('language.Employee')}}</a>
                        <a class="nav-item nav-link text-secondary " id="nav-home-tab" data-toggle="tab"
                           href="#nav-manager"
                           role="tab"
                           aria-controls="nav-home"
                           aria-expanded="true">
                            <i class="fa fa-user-o" aria-hidden="true"></i> {{__('language.Manager')}}
                        </a>
                    </nav>

                    <div class="tab-content" id="nav-tabContent">
                        {{--List user manager--}}
                        <div class="tab-pane fade border-list" id="nav-manager" role="tabpanel"
                             aria-labelledby="nav-home-tab">
                            <div class="table-responsive">
                                <table id="myTable" class="table table-hover no-margin">
                                    <thead class="thead-table">
                                    <tr>
                                        <th style="width: 5%" class="text-center">#</th>
                                        <th style="width: 20%" class="">{{__('language.full_name')}}</th>
                                        <th class="">{{__('language.role')}}</th>
                                        <th style="width: 15%" class=""></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($usersManager))
                                        @foreach($usersManager as $key=> $userManager)
                                            <tr>
                                                <td class="text-center ">{{++$key}}</td>
                                                <td ><a
                                                            href="{{ route('user.detail',$userManager->id) }}">{{ $userManager->first_name }} {{ $userManager->last_name }}</a>
                                                </td>
                                                <td >
                                                    @foreach($userManager->roles as $role)
                                                        @if(count($userManager->roles) == 1)
                                                            {{ $role->name}}
                                                        @else
                                                            {{ $role->name . ',' }}
                                                        @endif
                                                    @endforeach
                                                </td>

                                                @if($userManager->hasRole(\App\Http\Controllers\RoleConstants::ROLE_ADMIN) || $userManager->id == Auth::user()->id)
                                                    <td class="text-center ">
                                                    </td>
                                                @else
                                                    <td class="text-center ">
                                                        @include('subViews.button-edit-delete', ['routeEdit'=>'user.update', 'routeDelete'=>'user.delete','id' => $userManager->id])
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @endif
                                    @if(isset($usersManagerCenter))
                                        @foreach($usersManagerCenter as $key=> $userManagerCenter)
                                            <tr>
                                                <td class="text-center">{{++$key}}</td>
                                                <td ><a
                                                            href="{{ route('user.detail',$userManagerCenter->id) }}">{{ $userManagerCenter->first_name }} {{ $userManagerCenter->last_name }}</a>
                                                </td>
                                                <td >
                                                    @foreach($userManagerCenter->roles as $role)
                                                        @if(count($userManagerCenter->roles) == 1)
                                                            {{ $role->name}}
                                                        @else
                                                            {{ $role->name . ',' }}
                                                        @endif
                                                    @endforeach
                                                </td>

                                                @if($userManagerCenter->id == Auth::user()->id)
                                                @else
                                                    <td class="text-center ">
                                                        @include('subViews.button-edit-delete', ['routeEdit'=>'user.update', 'routeDelete'=>'user.delete','id' => $userManagerCenter->id])
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        {{--List user center--}}
                        <div class="tab-pane fade border-list show active" id="nav-user" role="tabpanel"
                             aria-labelledby="nav-profile-tab">
                            <div class="table-responsive">
                                <table id="myTable" class="table table-hover no-margin">
                                    <thead class="thead-table">
                                    <tr>
                                        <th style="width: 5%" class="text-center">#</th>
                                        <th style="width: 20%" >{{__('language.full_name')}}</th>
                                        <th >{{__('language.role')}}</th>
                                        <th style="width: 15%" ></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($usersTeacher))
                                        @foreach($usersTeacher as $key=> $userTeacher)
                                            <tr>
                                                <td class="text-center">{{++$key}}</td>
                                                <td ><a
                                                            href="{{ route('user.detail',$userTeacher->id) }}">{{ $userTeacher->first_name }} {{ $userTeacher->last_name }}</a>
                                                </td>
                                                <td >
                                                    @foreach($userTeacher->roles as $role)
                                                        @if(count($userTeacher->roles) == 1)
                                                            {{ $role->name}}
                                                        @else
                                                            {{ $role->name . ',' }}
                                                        @endif
                                                    @endforeach
                                                </td>
                                                @if($userTeacher->id == Auth::user()->id)
                                                    <td class="text-center ">
                                                    </td>
                                                @else
                                                    <td class="text-center ">
                                                        @include('subViews.button-edit-delete', ['routeEdit'=>'user.update', 'routeDelete'=>'user.delete','id' => $userTeacher->id])
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @else
                                        <p class="text-center">{{ __('language.No_Data') }}</p>
                                    @endif
                                    </tbody>
                                </table>
                                {{--<div class="pagination pagination-sm">--}}
                                {{--{{ $userTeacher->links() }}--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>
                @endcan
            </div>
        </div>
    </div>

@endsection

