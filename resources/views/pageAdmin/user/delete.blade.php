@extends('layouts.app')
@section('head.title')
    {{ __('language.Delete_user') }}
@endsection
@section('content')

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-12 col-md-8">
                <div class="delete" style="padding-top: 3em" >
                    <div class="name-program text-center">
                        <p style="padding-top: 10px;"><h5 class="text-danger">{{ __('language.Do_you_Delete') }}:
                            <strong>{{ $user->first_name. ' ' .$user->last_name }}</strong></h5></p>
                    </div>
                    @if(count($user->groups) > 0)
                        <p class="text-danger text-center">{{__('language.Do_You_Delete_Teacher')}}</p>
                    @else
                    @endif
                    <div class="form-group row justify-content-md-center">
                        <form action="{{ route('user.destroy',$user->id)}}" method="post">
                            {{ csrf_field() }}
                            <div class="col-6 col-md-3">
                                <button type="submit" class="btn btn-danger btn-lg">{{__('language.Delete')}}</button>
                    </div>
                        </form>
                        <div class="col-6 col-md-3 ">
                            <a href="{{ route('user.list') }}"><button type="button" class="btn btn-outline-secondary btn-lg">{{__('language.Cancel')}}</button></a>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection