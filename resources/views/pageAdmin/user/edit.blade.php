@extends('layouts.app')
@section('head.title')
    {{ __('language.Edit') }}
@endsection
@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('user.list') }}">{{ __('language.Employee') }}</a></li>
        <li class="breadcrumb-item text-muted"><a
                    href="{{route('user.detail',$user->id)}}">{{ $user->first_name .' '. $user->last_name }}</a></li>
        <li class="breadcrumb-item text-muted">{{ __('language.Edit') }}</li>
    </ol>
    @include('errors.success')
    <div class="main">
        <form class="form-horizontal" action="{{route('user.edit',$user->id)}}" method="POST">
            {{ csrf_field() }}
            <div class="main-content">
                <div class="row">
                    <!--information-->
                    <div class="col-12 col-md-8 col-lg-9">

                        <div class="row justify-content-center">
                            <!--First Name-->
                            <div class="col-10 col-md-12">
                                <div class="form-group row">
                                    <p class="col-12 text-muted p-2 text-note text-left">
                                        {{ __('language.Fields_with') }}
                                        <span class="text-danger">*</span>
                                        {{ __('language.Are_required') }}
                                    </p>
                                    <label for="inputFirstName"
                                           class="col-md-4 col-lg-3 col-form-label">{{__('language.first_name')}}
                                        <span class="align-middle text-danger">*</span>
                                    </label>
                                    <div class="col-md-8 col-lg-9 no-padding">
                                        <input type="text"
                                               class="form-control @if($errors->has('first_name')) is-invalid @endif"
                                               id="inputFirstName"
                                               placeholder=""
                                               value="{{ $user->first_name }}"
                                               name="first_name" required>
                                        @if($errors->has('first_name'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('first_name')  }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            {{--Last-name--}}
                            <div class="col-10 col-md-12">
                                <div class="form-group row">
                                    <label for="inputEmail"
                                           class="col-md-4 col-lg-3 col-form-label">{{__('language.last_name')}}
                                        <span class="align-middle text-danger">*</span>
                                    </label>
                                    <div class="col-md-8 col-lg-9 no-padding">
                                        <input type="text"
                                               class="form-control @if($errors->has('last_name')) is-invalid @endif"
                                               id="inputLastName"
                                               placeholder=""
                                               value="{{ $user->last_name }}"
                                               name="last_name" required>
                                        @if($errors->has('last_name'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('last_name')  }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <!--email-->
                            <div class="col-10 col-md-12">
                                <div class="form-group row">
                                    <label for="inputEmail"
                                           class="col-md-4 col-lg-3 col-form-label">{{__('language.Email')}}
                                        <span class="align-middle text-danger">*</span>
                                    </label>
                                    <div class="col-md-8 col-lg-9 no-padding">
                                        <input type="email"
                                               class="form-control @if($errors->has('email')) is-invalid @endif"
                                               id="inputEmail"
                                               placeholder=""
                                               value="{{ $user->email }}"
                                               name="email" required>
                                        @if($errors->has('email'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('email')  }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <!--role-->
                            <div class="col-10 col-md-12">
                                <div class="form-group row">
                                    <label class="col-md-4 col-lg-3 col-form-label">{{__('language.role')}}</label>
                                    <div class="col-md-8 col-lg-9 no-padding">
                                        @can('crud-all-user')
                                            <ul>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{\App\Http\Controllers\RoleConstants::ROLE_HQMANAGER}}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_HQMANAGER) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.Headquater_Maneger')}}</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{\App\Http\Controllers\RoleConstants::ROLE_HQAAO}}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_HQAAO) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.Headquater_Maneger')}}</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{ \App\Http\Controllers\RoleConstants::ROLE_CENTERMANAGER }}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_CENTERMANAGER) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.Center_Manager')}}</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{ \App\Http\Controllers\RoleConstants::ROLE_AAO }}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_AAO) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.AAO')}}</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{ \App\Http\Controllers\RoleConstants::ROLE_COACH }}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_COACH) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.Coach')}}</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{ \App\Http\Controllers\RoleConstants::ROLE_TUTOR }}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_TUTOR) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.Tutor')}}</span>
                                                    </label>
                                                </li>
                                            </ul>

                                        @elsecan('crud-senior-user')
                                            <ul>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{ \App\Http\Controllers\RoleConstants::ROLE_HQAAO }}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_HQAAO) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.Headquater_AAO')}}</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{ \App\Http\Controllers\RoleConstants::ROLE_CENTERMANAGER }}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_CENTERMANAGER) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.Center_Manager')}}</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{ \App\Http\Controllers\RoleConstants::ROLE_AAO }}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_AAO) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.AAO')}}</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{ \App\Http\Controllers\RoleConstants::ROLE_COACH }}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_COACH) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.Coach')}}</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{ \App\Http\Controllers\RoleConstants::ROLE_TUTOR }}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_TUTOR) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.Tutor')}}</span>
                                                    </label>
                                                </li>
                                            </ul>
                                        @elsecan('crud-local-user')
                                            <ul>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{ \App\Http\Controllers\RoleConstants::ROLE_CENTERMANAGER }}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_CENTERMANAGER) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.Center_Manager')}}</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{ \App\Http\Controllers\RoleConstants::ROLE_AAO }}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_AAO) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.AAO')}}</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{ \App\Http\Controllers\RoleConstants::ROLE_COACH }}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_COACH) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.Coach')}}</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{ \App\Http\Controllers\RoleConstants::ROLE_TUTOR }}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_TUTOR) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.Tutor')}}</span>
                                                    </label>
                                                </li>
                                            </ul>
                                        @elsecan('crud-user-aao', Session::get('centerId'))
                                            <ul>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{ \App\Http\Controllers\RoleConstants::ROLE_AAO }}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_AAO) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.AAO')}}</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{ \App\Http\Controllers\RoleConstants::ROLE_COACH }}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_COACH) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.Coach')}}</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{ \App\Http\Controllers\RoleConstants::ROLE_TUTOR }}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_TUTOR) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.Tutor')}}</span>
                                                    </label>
                                                </li>
                                            </ul>
                                        @elsecan('crud-teacher', Session::get('centerId'))
                                            <ul>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{ \App\Http\Controllers\RoleConstants::ROLE_COACH }}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_COACH) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.Coach')}}</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" name="roles[]" value="{{ \App\Http\Controllers\RoleConstants::ROLE_TUTOR }}"
                                                               class="custom-control-input"
                                                        @foreach( $user->roles as $role)
                                                            {{ ($role->id==\App\Http\Controllers\RoleConstants::ROLE_TUTOR) ? 'checked' : '' }}
                                                                @endforeach >
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">{{__('language.Tutor')}}</span>
                                                    </label>
                                                </li>
                                            </ul>
                                        @endcan
                                        @if ($errors->has('roles'))
                                            <p class="text-danger">
                                                {{ $errors->first('roles') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <!--Button submit and back-->
                            <div class="col-10 col-md-12">
                                <div class="row justify-content-end">
                                    <button type="submit"
                                            class="btn btn-primary button-submit">{{__('language.Save')}}</button>
                                    <a href="{{ route('user.detail',$user->id) }}">
                                        <input type="button"
                                               class=" btn btn-outline-secondary button-submit"
                                               value="{{__('language.Cancel')}}">
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection