@extends('layouts.app')
@section('head.title')
    {{ __('language.employee_add') }}
@endsection
@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('user.list') }}">{{ __('language.Employee') }}</a></li>
        <li class="breadcrumb-item active">{{ __('language.employee_add') }}</li>
    </ol>
    @include('errors.success')
    <div class="main">
        <div class="row">
            <div class="col-12">
                <div class="p-4">
                    <form class="form-horizontal" action="{{ route('user.store') }}" method="POST">
                        {{ csrf_field() }}

                        <div class="form-group row">
                            <p class="col-12 text-muted p-2 text-note text-left">
                                {{ __('language.Fields_with') }}
                                <span class="text-danger">*</span>
                                {{ __('language.Are_required') }}
                            </p>
                            <label for="inputFirstName"
                                   class="col-12 col-md-2 col-form-label">{{__('language.first_name')}}
                                <span class="align-middle text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="text"
                                       class="form-control @if($errors->has('first_name')) is-invalid @endif"
                                       id="inputFirstName"
                                       placeholder=""
                                       value="{{ old('first_name') }}"
                                       name="first_name" required>
                                @if($errors->has('first_name'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('first_name')  }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        {{--Last-name--}}
                        <div class="form-group row">
                            <label for="inputEmail"
                                   class="col-12 col-md-2 col-form-label">{{__('language.last_name')}}
                                <span class="align-middle text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="text"
                                       class="form-control @if($errors->has('last_name')) is-invalid @endif"
                                       id="inputLastName"
                                       placeholder=""
                                       value="{{ old('last_name') }}"
                                       name="last_name" required>
                                @if($errors->has('last_name'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('last_name')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--email-->
                        <div class="form-group row">
                            <label for="inputEmail"
                                   class="col-12 col-md-2 col-form-label">{{__('language.Email')}}
                                <span class="align-middle text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="email"
                                       class="form-control @if($errors->has('email')) is-invalid @endif"
                                       id="inputEmail"
                                       placeholder=""
                                       value="{{ old('email') }}"
                                       name="email" required>
                                @if($errors->has('email'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('email')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--password-->
                        <div class="form-group row">
                            <label for="inputPassword"
                                   class="col-12 col-md-2 col-form-label">{{__('language.password')}}
                                <span class="align-middle text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="password"
                                       class="form-control @if($errors->has('password')) is-invalid @endif"
                                       id="inputPassword"
                                       placeholder=""
                                       value="{{ old('password') }}"
                                       name="password" required>
                                @if($errors->has('password'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('password')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!--password confim-->
                        <div class="form-group row">
                            <label for="inputPasswordConfim"
                                   class="col-12 col-md-2 col-form-label">{{__('language.password_confirm')}}
                                <span class="align-middle text-danger">*</span>
                            </label>
                            <div class="col-12 col-md-10">
                                <input type="password"
                                       class="form-control @if($errors->has('password_confim')) is-invalid @endif"
                                       id="inputPasswordConfim"
                                       placeholder=""
                                       value=""
                                       name="password_confim" required>
                                @if($errors->has('password_confim'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('password_confim')  }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-12 col-md-2 col-form-label">{{__('language.role')}}<span
                                        class="text-danger"> *</span></label>
                            <div class="col-12 col-md-10">
                                @can('crud-all-user')
                                    <ul>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="2"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.Headquater_Maneger')}}</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="3"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.Headquater_AAO')}}</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="4"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.Center_Manager')}}</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="5"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.AAO')}}</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="6"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.Coach')}}</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="7"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.Tutor')}}</span>
                                            </label>
                                        </li>
                                    </ul>
                                @elsecan('crud-senior-user')
                                    <ul>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="3"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.Headquater_AAO')}}</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="4"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.Center_Manager')}}</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="5"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.AAO')}}</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="6"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.Coach')}}</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="7"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.Tutor')}}</span>
                                            </label>
                                        </li>
                                    </ul>
                                @elsecan('crud-local-user')
                                    <ul>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="4"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.Center_Manager')}}</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="5"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.AAO')}}</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="6"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.Coach')}}</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="7"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.Tutor')}}</span>
                                            </label>
                                        </li>
                                    </ul>
                                @elsecan('crud-user-aao', Session::get('centerId'))
                                    <ul>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="5"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.AAO')}}</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="6"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.Coach')}}</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="7"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.Tutor')}}</span>
                                            </label>
                                        </li>
                                    </ul>
                                @elsecan('crud-user-teacher', Session::get('centerId'))
                                    <ul>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="6"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.Coach')}}</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="roles[]" value="7"
                                                       class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{__('language.Tutor')}}</span>
                                            </label>
                                        </li>
                                    </ul>
                                @endcan
                                @if ($errors->has('roles'))
                                    <p class="text-danger">
                                        {{ $errors->first('roles') }}
                                    </p>
                                @endif
                            </div>
                        </div>

                        <!--Button submit and back-->
                        <div class="col-10 col-md-12">
                            <div class="row justify-content-end">
                                <button type="submit"
                                        class="btn btn-primary button-submit">{{__('language.create')}}</button>
                                <a href="{{ route('user.list') }}">
                                    <input type="button"
                                           class=" btn btn-outline-secondary button-submit"
                                           value="{{__('language.Cancel')}}">
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection