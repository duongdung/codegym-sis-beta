@extends('layouts.app')
@section('head.title')
    {{ __('language.Delete_center') }}
@endsection
@section('content')

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-12 col-md-8">

                @if(count($center->groups) > 0)
                    <div class="col-12 col-md-12 no-delete pt-5 text-center">
                        <h4><p class="text-danger">{{__('language.No_Delete')}}</p></h4>
                    </div>
                    <div class="suport">
                        <p class="text-danger">{{__('language.No_delete_center')}}</p>
                    </div>
                    <div class="col-12 col-md-12 text-center">
                        <a href="{{route('center.list')}}" class="btn btn-secondary btn-lg">{{__('language.Cancel')}}</a>
                    </div>
                @else
                <div class="delete" style="padding-top: 3em" >
                    <div class="name-program text-center">
                        <p class="pt-10"><h5 class="text-danger">{{__('language.Do_you_deleteCenter')}}:
                            <strong>{{$center->name}}?</strong></h5></p>
                    </div>

                    <div class="form-group row justify-content-md-center">
                        <form action="{{ route('center.destroy',$center->id) }}" method="post">
                            {{ csrf_field() }}
                            <div class="col-6 col-md-3">
                                <button type="submit" class="btn btn-danger btn-lg">{{__('language.Delete')}}</button>
                            </div>
                        </form>
                        <div class="col-6 col-md-3 ">
                            <a href="{{ route('center.list') }}"><button type="button" class="btn btn-outline-secondary button-submit btn-lg">{{__('language.Cancel')}}</button></a>
                        </div>

                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

@endsection