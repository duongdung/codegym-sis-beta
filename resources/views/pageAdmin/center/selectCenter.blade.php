    <form action="{{ route('postCenter')}}" class="form-lang" method="post">
        {{ csrf_field() }}
        <select class="custom-select w-100" name="center" onchange='this.form.submit();'>
            @foreach($centers as $center)
                @if($center->id == Session::get('centerId')){
                <option selected value=" {{$center->id}}">{{$center->name}} <i class="fa fa-angle-down" aria-hidden="true"></i>
                </option>
                }@else{
                <option value=" {{$center->id}}">{{$center->name}}
                </option>
                @endif
            @endforeach
        </select>
    </form>
