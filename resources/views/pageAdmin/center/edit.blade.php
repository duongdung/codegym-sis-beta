@extends('layouts.app')

@section('head.title')
    {{ __('language.center_edit') }}
@endsection

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('center.list')}}">{{ __('language.Center') }}</a></li>
        <li class="breadcrumb-item text-muted">{{ $center->name }}</li>
        <li class="breadcrumb-item text-muted">{{ __('language.center_edit') }}</li>
    </ol>

    <div class="main">
        <form class="form-horizontal" action=""
              method="POST">
            {{ csrf_field() }}
            <div class="main-content">
                <div class="row">
                    <!--Center Name-->
                    <div class="col-12">
                        <div class="form-group row">
                            <p class="col-12 text-muted p-2 text-note">
                                {{ __('language.Fields_with') }}
                                <span class="text-danger">*</span>
                                {{ __('language.Are_required') }}
                            </p>
                            <label for="example-text-input"
                                   class="col-12 col-md-2 col-form-label">{{ __('language.Center_Name') }}
                                <span class="text-danger">*</span></label>
                            <div class="col-12 col-md-10">
                                <input class="form-control @if($errors->has('center_name')) is-invalid @endif"
                                       type="text" value="{{ $center->name }}" name="center_name"
                                       required>
                                @if($errors->has('center_name'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('center_name')  }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--Center Code-->
                    <div class="col-12">
                        <div class="form-group row">
                            <label for="example-text-input"
                                   class="col-12 col-md-2 col-form-label">{{ __('language.Center_Code') }}
                                <span class="text-danger">*</span></label>
                            <div class="col-12 col-md-10">
                                <input class="form-control @if($errors->has('center_code')) is-invalid @endif"
                                       type="text" value="{{ $center->center_code }}" name="center_code"
                                       required>
                                @if($errors->has('center_code'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('center_code')  }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--Email-->
                    <div class="col-12">
                        <div class="form-group row">
                            <label for="example-text-input"
                                   class="col-12 col-md-2 col-form-label">{{ __('language.Email') }}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-12 col-md-10">
                                <input class="form-control @if($errors->has('center_email')) is-invalid @endif"
                                       type="email" value="{{ $center->email }}" name="center_email"
                                       required>
                                @if($errors->has('center_email'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('center_email')  }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--Phone-->
                    <div class="col-12">
                        <div class="form-group row">
                            <label for="example-text-input"
                                   class="col-12 col-md-2 col-form-label">{{ __('language.Phone') }}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-12 col-md-10">
                                <input class="form-control @if($errors->has('center_phone')) is-invalid @endif"
                                       type="text" value="{{ $center->phone }}" name="center_phone"
                                       required>
                                @if($errors->has('center_phone'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('center_phone')  }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--Address-->
                    <div class="col-12">
                        <div class="form-group row">
                            <label for="example-text-input"
                                   class="col-12 col-md-2 col-form-label">{{ __('language.Address') }}
                                <span class="text-danger">*</span></label>
                            <div class="col-12 col-md-10">
                                <input rows="4"
                                       class="form-control @if($errors->has('center_address')) is-invalid @endif"
                                       type="text"
                                       name="center_address" value="{{ $center->address }}" required>
                                @if($errors->has('center_address'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('center_address')  }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--Button submit and back-->
                    <div class="col-12">
                        <div class="row justify-content-end">
                            <button type="submit"
                                    class="btn btn-primary button-submit">{{__('language.Save')}}</button>
                            <a href="{{ route('center.list') }}">
                                <input type="button"
                                       class=" btn btn-outline-secondary button-submit"
                                       value="{{__('language.Cancel')}}">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection