@extends('layouts.app')

@section('head.title')
    {{__('language.List_Center')}}
@endsection

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted"><i class="" aria-hidden="true"></i> {{__('language.List_Center')}}</li>
    </ol>
    @include('errors.success')
    @can('crud-center')
        <a class="btn btn-outline-primary" href="{{ route('center.add') }}">
            <i class="fa fa-plus" aria-hidden="true"></i> {{__('language.Add')}}
        </a>
    @endcan
    <div class="main p-3">
        <div class="row">
            <div class="col-12 col-md-12 table-responsive no-padding">
                <table id="myTable" class="table table-hover">
                    <thead class="thead-table">
                    <tr>
                        <th class="text-center">#</th>
                        <th >{{__('language.Center_Code')}}</th>
                        <th >{{__('language.Center_Name')}}</th>
                        <th >{{__('language.Address')}}</th>
                        <th >{{__('language.Phone')}}</th>
                        <th >{{__('language.Email')}}</th>
                        @can('crud-center')
                            <th ></th>
                        @endcan
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($centers as $key=> $center)
                        <tr>
                            <td class="text-center border-top-0">{{++$key}}</td>
                            <td >{{ $center->center_code }}</td>
                            <td >{{ $center->name }}</td>
                            <td >{{$center->address}}</td>
                            <td >{{$center->phone}}</td>
                            <td >{{$center->email}}</td>
                            @can('crud-center')
                                <td class="text-center">
                                    @include('subViews.button-edit-delete', ['routeEdit'=>'center.update', 'routeDelete'=>'center.delete','id' => $center->id])
                                </td>
                            @endcan
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

