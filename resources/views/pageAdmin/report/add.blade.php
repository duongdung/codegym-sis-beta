@extends('layouts.app')

@section('head.title')
    {{__('language.create_report')}}
@endsection

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">{{__('language.Report')}} </a></li>
        <li class="breadcrumb-item active">{{__('language.Add')}} </li>
    </ol>


    <div class="main">
        <form class="form-horizontal" action="{{ route('report.store') }}"
              method="POST">
            {{ csrf_field() }}
            <div class="main-content">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="row">
                            <div class="col-12 col-md-12">
                                <div class="form-group row">
                                    <p class="col-12 text-muted p-2 text-note">
                                        {{ __('language.Fields_with') }}
                                        <span class="text-danger">*</span>
                                        {{ __('language.Are_required') }}
                                    </p>
                                    <label for="example-text-input"
                                           class="col-md-4 col-lg-3 col-form-label">{{__('language.name')}}
                                        <span class="text-danger">*</span></label>
                                    <div class="col-12 col-md-8 col-lg-9 no-padding">
                                        <input class="form-control @if($errors->has('name')) is-invalid @endif"
                                               type="text"
                                               value="{{ old('name') }}" name="name" required>
                                        @if($errors->has('name'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('name')  }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-12">
                                <div class="form-group row">
                                    <label for="example-text-input"
                                           class="col-12 col-md-4 col-lg-3 col-form-label">{{ __('language.Note') }}
                                    </label>
                                    <textarea rows="4"
                                              class="col-12 col-md-8 col-lg-9 form-control editor"
                                              type="text"
                                              name="report_description"
                                              placeholder="">
                                        </textarea>
                                </div>
                            </div>
                            <!--Button submit and back-->
                            <div class="col-10 col-md-12">
                                <div class="row justify-content-end">
                                    <button type="submit"
                                            class="btn btn-primary button-submit">{{__('language.create')}}</button>
                                    <a href="{{ route('report.list') }}">
                                        <input type="button"
                                               class=" btn btn-outline-secondary button-submit"
                                               value="{{__('language.Cancel')}}">
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection