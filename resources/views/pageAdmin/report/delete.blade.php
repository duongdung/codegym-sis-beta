@extends('layouts.app')
@section('head.title')
    {{ __('language.Delete_report') }}
@endsection
@section('content')

    <div class="container">

        <div class="row justify-content-md-center">
            <div class="col-12 col-md-8" style="height: 300px;border-radius: 5px;">

                    <div class="delete" style="padding-top: 3em" >
                        <div class="name-program text-center">
                            <p style="padding-top: 10px;"><h5 class="text-danger">{{ __('language.Do_you_deleteReport') }}:
                                <strong>{{ $report->name }}?</strong></h5></p>
                        </div>
                        <div class="form-group row justify-content-md-center">
                            <form action="{{ route('report.destroy',$report->id)}}" method="post">
                                {{ csrf_field() }}
                                <div class="col-6 col-md-3">
                                    <button type="submit" class="btn btn-danger btn-lg">{{__('language.Delete')}}</button>
                                </div>
                            </form>
                            <div class="col-6 col-md-3 ">
                                <a href="{{ route('report.list') }}"><button type="button" class="btn btn-outline-secondary btn-lg">{{__('language.Cancel')}}</button></a>
                            </div>

                        </div>
                    </div>

            </div>
        </div>
    </div>

@endsection