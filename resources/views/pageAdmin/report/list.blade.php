@extends('layouts.app')

@section('head.title')
    {{__('language.list_report')}}
@endsection

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted">{{__('language.list_report')}}</li>
    </ol>
    @include('errors.success')
    @can('crud-report')
        <a class="btn btn-outline-primary" href="{{ route('report.create') }}">
            <i class="fa fa-plus" aria-hidden="true"></i> {{__('language.Add')}}
        </a>
    @endcan
    <div class="main mt-3">
        <div class="row">
            <div class="col-12 col-md-12 table-responsive">
                <table id="myTable" class="table table-hover">
                    <thead class="thead-table">
                    <tr>
                        <th class="text-center">#</th>
                        <th>{{__('language.ex_report')}}</th>
                        <th>{{__('language.status')}}</th>
                        <th></th>
                    </tr>
                    </thead>
                    @if(count($reports)==0)
                        <tr>
                            <td class="text-center" colspan="4">{{__('language.No_Data')}}</td>
                        </tr>
                    @else
                        <tbody>
                        @foreach($reports as $key=> $report)
                            <tr>
                                <td class="text-center">{{++$key}}</td>
                                <td><a href="{{ route('report.detail',$report->id) }}">{{$report->name}}</a></td>
                                <td>{{$report->description}}</td>
                                <td class="text-center">
                                    <a href="{{route('report.delete',$report->id)}}"><i
                                                class="fa fa-times text-danger" aria-hidden="true"></i> {{__('language.delete')}}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    @endif
                </table>
            </div>

        </div>
    </div>

@endsection

