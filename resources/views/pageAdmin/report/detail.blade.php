@extends('layouts.app')

@section('head.title')
    {{__('language.Report')}}
@endsection

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted "><a href="{{ route('report.list') }}">{{__('language.Report')}}</a></li>
    </ol>

    <div class="main mt-3">
        <div class="row justify-content-center">
            <div class="col-12 col-md-12 main-content">
                <div class="col-12">
                    @include('errors.success')
                </div>
                <div class="col-12 col-md-12 detail-report border">
                    <div class="col-12 col-md-12 text-center">
                        <h3 class="mt-2 editable-input name-report" data-name="name-report" data-type="textarea" data-pk="{{$report->id}}" data-url="/reports/{{$report->id}}/update-name" data-title="{{__('language.edit_name_report')}}">{!! $report->name !!}</h3>
                        <span class="fa fa-edit editable-icon"></span>
                        <br>
                        <p class="mt-2 editable-input name-report" data-name="description-report" data-type="textarea" data-pk="{{$report->id}}" data-url="/reports/{{$report->id}}/update-description" data-title="{{__('language.edit_description_report')}}">{{$report->description}}</p>
                        <span class="fa fa-edit editable-icon"></span>
                    </div>


                    <div class="col-12 col-md-12 col-lg-12 ">
                        <div class="row justify-content-center">
                                @foreach($report->groups_criteria as $group_criteria)
                                <div class="col-12-col-md-12 col-lg-12 criteria-group border mb-3">
                                    <div class="row">
                                        <div class="col-10 col-md-10 col-lg-11">
                                            <h4 id="group-criteria-{{$group_criteria->id}}" class="mt-2 name-group editable-input no-space" data-name="name-group" data-type="textarea" data-pk="{{$group_criteria->id}}" data-url="/reports/{{$report->id}}/groups-criteria/{{$group_criteria->id}}/update-name" data-title="{{__('language.edit_name_group')}}">{{$group_criteria->name}}</h4>
                                            <span class="fa fa-edit editable-icon"></span>
                                            <br>
                                                <p class="text-muted editable-input" data-name="description-group" data-type="textarea" data-pk="{{$group_criteria->id}}" data-url="/reports/{{$report->id}}/groups-criteria/{{$group_criteria->id}}/update-description" data-title="{{__('language.edit_description_group')}}">{{$group_criteria->description}}</p>
                                            <span class="fa fa-edit editable-icon"></span>
                                        </div>
                                        <div class="col-1 col-md-2 col-lg-1">
                                            <span class="text-right border-top-0">
                                                        <i class="fa fa-times text-danger" data-toggle="modal" data-target="#deleteGroupCriteria" aria-hidden="true"></i>
                                            </span>
                                        </div>

                                        <!-- Modal delete group criteria -->
                                        <div class="modal fade" id="deleteGroupCriteria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">{{__('language.delete_group_criteria')}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        {{$group_criteria->name}}
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('language.Cancel')}}</button>
                                                        <form action="{{ route('group_criteria.destroy',[$report->id,$group_criteria->id]) }}" method="post">
                                                            {{ csrf_field() }}
                                                            <button type="submit" value="submit" class="btn btn-primary">{{__('language.detele')}}</button>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{--end--}}
                                    </div>


                                    <div class="row justify-content-center">
                                        @foreach($group_criteria->categories_criteria as $category_criteria)
                                            <div class="col-11 col-md-11 category-criteria border mb-2">
                                                <div class="row">
                                                    <div class="col-10 col-md-10 col-lg-11">
                                                        <h5 class="mt-2 name-category no-space editable-input" data-name="category-name" data-type="textarea" data-pk="{{$category_criteria->id}}" data-url="/reports/{{$report->id}}/groups-criteria/{{$group_criteria->id}}/categories-criteria/{{$category_criteria->id}}/update" data-title="{{__('language.edit_category_criteria')}}">{{ $category_criteria->name }}</h5>
                                                        <span class="fa fa-edit editable-icon"></span>
                                                    </div>
                                                    <div class="col-1 col-md-2 col-lg-1">
                                                        <span class="text-right border-top-0">
                                                                    <i class="fa fa-times text-danger" data-toggle="modal" data-target="#deleteCategoryCriteria" aria-hidden="true"></i>
                                                        </span>
                                                    </div>
                                                </div>

                                                <!-- Modal delete category criteria -->
                                                <div class="modal fade" id="deleteCategoryCriteria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">{{__('language.delete_cate_criteria')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                {{$category_criteria->name}}
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('language.Cancel')}}</button>
                                                                <form action="{{ route('category.destroy',[$report->id,$group_criteria->id,$category_criteria->id]) }}" method="post">
                                                                    {{ csrf_field() }}
                                                                    <button type="submit" value="submit" class="btn btn-primary">{{__('language.detele')}}</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{--end--}}

                                                <div class="col-12 col-md-12 border" >
                                                    <div class="row">
                                                        @foreach($category_criteria->criterias as $key=> $criteria)
                                                        <div class="col-4 border">
                                                            <p class="editable-input" data-name="name-criteria" data-type="textarea" data-pk="{{$criteria->id}}" data-url="/reports/{{$report->id}}/groups-criteria/{{$group_criteria->id}}/categories-criteria/{{$category_criteria->id}}/criterias/{{$criteria->id}}/update-name" data-title="{{__('language.edit_name_criteria')}}">{{ $criteria->name }}</p>
                                                            <span class="fa fa-edit editable-icon"></span>
                                                        </div>
                                                        <div class="col-8 border">
                                                            <div class="row">
                                                                <div class="col-10 col-md-10 col-lg-11">
                                                                    <p class="editable-input" data-name="description-criteria" data-type="textarea" data-pk="{{$criteria->id}}" data-url="/reports/{{$report->id}}/groups-criteria/{{$group_criteria->id}}/categories-criteria/{{$category_criteria->id}}/criterias/{{$criteria->id}}/update-description" data-title="{{__('language.edit_description_criteria')}}">{{$criteria->description }}   </p>
                                                                    <span class="fa fa-edit editable-icon"></span>
                                                                </div>

                                                                <div class="col-1 col-md-2 col-lg-1">
                                                                    <span class="border-top-0">
                                                                                <i class="fa fa-times text-danger" data-toggle="modal" data-target="#deleteCriteria" aria-hidden="true"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                            <!-- Modal delete criteria -->
                                                            <div class="modal fade" id="deleteCriteria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">{{__('language.delete_criteria')}}</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            {{$criteria->name}}
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('language.Cancel')}}</button>
                                                                            <form action="{{ route('criteria.destroy',[$report->id,$group_criteria->id,$category_criteria->id,$criteria->id]) }}" method="post">
                                                                                {{ csrf_field() }}
                                                                                <button type="submit" value="submit" class="btn btn-primary">{{__('language.detele')}}</button>
                                                                            </form>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {{--end--}}
                                                        @endforeach

                                                    </div>
                                                </div>
                                                <p class="text-right mt-3">
                                                    <a href="{{route('criteria.create',['report'=>$report->id,'group-criteria'=>$group_criteria->id,'category-criteria'=>$category_criteria->id])}}">{{__('language.create_criteria')}}</a>
                                                </p>
                                            </div>
                                        @endforeach
                                    </div>
                                    <p class="text-right mt-3">
                                        <a href="{{ route('category.createCategory',['report'=>$report->id, 'group-criteria'=>$group_criteria->id]) }}">{{__('language.create_category_criteria')}}</a>
                                    </p>
                                </div>
                                @endforeach
                        </div>
                        <p class="text-right mt-3">
                            <a href="{{route('group_criteria.create',$report->id)}}">{{__('language.create_group_criteria')}}</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

