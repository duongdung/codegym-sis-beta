@extends('layouts.app')

@section('head.title')
    {{ __('language.Edit_Test_Score') }}
@endsection

@section('content')
    @if(!isset($student))
        <p class="text-center mt-5">{{__('language.Empty_Data')}}</p>
    @else
        <ol class="breadcrumb">
            <li class="breadcrumb-item text-muted">{{__('language.Student')}} {{ $student->first_name .' '. $student->last_name }}</li>
            <li class="breadcrumb-item text-muted">
                <a href="{{ route('student.tab.score', $student->id) }}">{{__('language.Test_Score')}}</a>
            </li>
            <li class="breadcrumb-item text-muted">{{__('language.Edit_Test_Score')}}</li>
        </ol>
        <div class="main">
            <div class="row">
                <div class="col-12 col-md-12 ">
                    <ul class=" nav nav-tabs responsive" role="tablist">

                        <!-- Tab thong tin ca nhan -->
                        <li class="nav-item">
                            <a class="nav-link text-secondary"
                               href="{{ route('student.details', [$student->id, $student->group_id]) }}">{{__('language.Personal')}}</a>
                        </li>
                    @can('view-list-students',Session::get('centerId'))
                        <!-- Tab lop hoc -->
                            <li class="nav-item">
                                <a class="nav-link text-secondary"
                                   href="{{ route('student.group', $student->id) }}">{{__('language.Group')}}</a>
                            </li>
                    @endcan
                    @can('add-student-log', $student)
                        <!-- Tab nhat ky -->
                            <li class="nav-item">
                                <a class="nav-link text-secondary"
                                   href="{{ route('student.log', $student->id) }}">{{__('language.Logs')}}
                                </a>
                            </li>
                    @endcan
                    @can('crud-capacity', $student->group)
                        <!-- Tab bao cao -->
                            <li class="nav-item">
                                <a class="nav-link text-secondary"
                                   href="{{ route('student.capacity', [$student->id, $student->group_id]) }}"
                                >{{__('language.evaluate_capacity')}}</a>
                            </li>
                    @endcan
                    @can('view-student-score', $student)
                        <!-- Tab diem thi -->
                            <li class="nav-item">
                                <a class="nav-link text-secondary active"
                                   href="{{ route('student.tab.score', $student->id) }}"
                                >{{__('language.Test_Score')}}</a>
                            </li>
                    @endcan
                    <!-- Button edit, delete -->
                        @can('curd-student', Session::get('centerId'))
                            @include('subViews.button-edit-delete', ['routeEdit'=>'student.edit', 'routeDelete'=>'student.delete','id' => $student->id])
                        @endcan
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content" id="myTabContent">
                        <div class="border-list p-4">
                            @if(Session::has('has-exam'))
                                <p class="text-danger no-margin pt-3">
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{__('language.has_exam_1').Session::get('has-exam').__('language.has_exam_2')}}
                                </p>
                            @endif
                            <form class="form-horizontal pt-3" action="{{ route('student.update.score', [$student->id, $exam->id, $course->id]) }}" method="POST">
                                {{ csrf_field() }}
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <!--Student Name-->
                                        <tr>
                                            <th style="width: 10%" class="border-top-0">{{ __('language.Student') }}:
                                            </th>
                                            <td class="border-top-0">
                                                <input placeholder=""
                                                       class="form-control"
                                                       value="{{ $student->first_name .' '. $student->last_name }}"
                                                       name="full_name" disabled>
                                            </td>
                                        </tr>

                                        <!--Course Name-->
                                        <tr>
                                            <th style="width: 10%" class="border-top-0">{{ __('language.Course') }}:
                                            </th>
                                            <td class="border-top-0">
                                                <input placeholder=""
                                                       class="form-control"
                                                       value="{{ $course->course_name }}"
                                                       name="course_name" disabled>
                                            </td>
                                        </tr>

                                        <!--Exam date-->
                                        <tr>
                                            <th style="width: 10%" class="border-top-0">{{ __('language.Exam_Date') }}
                                                :
                                            </th>
                                            <td class="border-top-0">

                                                <input placeholder="dd-mm-yyyy"
                                                       class="datepicker form-control @if($errors->has('exam_date')) is-invalid @endif"
                                                       value="{{ $exam->date->format('d-m-Y') }}"
                                                       name="exam_date" required>

                                                @if($errors->has('exam_date'))
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('exam_date')  }}
                                                    </div>
                                                @endif
                                            </td>
                                        </tr>

                                        <!--Exam score-->
                                        <tr>
                                            <th style="width: 10%" class="border-top-0">{{ __('language.Test_Score') }}
                                                :
                                            </th>
                                            <td class="border-top-0">
                                                @foreach($exam->students as $score)
                                                    @if ($score->id === $student->id)
                                                        <input type="text"
                                                               class="form-control @if($errors->has('score')) is-invalid @endif"
                                                               placeholder=""
                                                               value="{{ $score->pivot->score }}"
                                                               name="score" required>
                                                    @endif
                                                @endforeach
                                                @if($errors->has('score'))
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('score')  }}
                                                    </div>
                                                @endif
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!--Button submit and back-->
                                <div class="row justify-content-end px-md-4">
                                    <button type="submit"
                                            class="btn btn-primary button-submit">{{__('language.Edit')}}</button>
                                    <a href="{{ route('student.details', [$student->id, $student->group_id]) }}">
                                        <input type="button"
                                               class=" btn btn-outline-secondary button-submit"
                                               value="{{__('language.Cancel')}}">
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

