@extends('layouts.app')
@section('head.title')
    {{ __('language.Remove_Test_Score') }}
@endsection
@section('content')

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-12 col-md-8" style="height: 300px;border-radius: 5px;">
                <div class="delete" style="padding-top: 3em" >
                    <div class="name-program" style="text-align: center;">
                        <h6 class="text-danger">{{ __('language.do_you_want_delete_score')}}
                        <strong>{{ $course->course_name }}</strong>{{ __('language.of_student') }}
                        <strong>{{ $student->first_name .' '. $student->last_name  }}</strong>?
                        </h6>
                    </div>
                    <div class="form-group row justify-content-md-center mt-3">
                        <form action="{{ route('student.destroy.score',[$student->id, $exam->id])}}" method="post">
                            {{ csrf_field() }}
                            <div class="col-6 col-md-3">
                                <button type="submit" class="btn btn-danger">{{__('language.Delete')}}</button>
                            </div>
                        </form>
                        <div class="col-6 col-md-3 ">
                            <a href="{{ route('student.details',[$student->id, $student->group_id]) }}"><button type="button" class="btn btn-outline-secondary">{{__('language.Cancel')}}</button></a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection