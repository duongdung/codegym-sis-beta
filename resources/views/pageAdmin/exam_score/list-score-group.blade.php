@extends('layouts.app')

@section('head.title')
    {{ __('language.Group_detail') }}
@endsection

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('group.list') }}">{{ __('language.Group') }} </a></li>
        <li class="breadcrumb-item text-muted">{{ $group->group_id }}</li>
        <li class="breadcrumb-item text-muted">{{ __('language.View_Test_Score') }}</li>
    </ol>

    <div class="main">
        <div class="row">
            <div class="col-12 col-md-12 ">
                <ul class="nav nav-tabs" id="myTab" role="tablist">

                    <!-- Tab info -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-home-tab"
                           href="{{ route('group.detail',$group->id) }}">
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                            {{ __('language.Group_detail') }}
                        </a>
                    </li>

                    <!-- Tab student -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                           href="{{ route('group.student',$group->id) }}">
                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                            {{ __('language.Student') }}
                        </a>
                    </li>

                    <!-- Tab Log -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                           href="{{ route('group.log', $group->id) }}">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                            {{ __('language.Logs') }}
                        </a>
                    </li>

                    <!-- Tab Attendance-->
                    @can('create-edit-view-attendance', $group)
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                               href="{{ route('attendance.create', $group->id) }}">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                {{ __('language.Attendance') }}
                            </a>
                        </li>
                    @endcan

                <!-- Tab exam score -->
                    @can('view-group-score', $group)
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary active" id="nav-profile-tab"
                               href="{{ route('group.show.score', $group->id) }}">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                {{ __('language.Test_Score') }}
                            </a>
                        </li>
                    @endcan

                <!-- Button edit, delete -->
                    @can('crud-group', Session::get('centerId'))
                        @include('subViews.button-edit-delete', ['routeEdit'=>'group.edit', 'routeDelete'=>'group.destroy','id' => $group->id])
                    @endcan
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="border-list p-4">
                        @can('crud-group-score', Session::get('centerId'))
                        <a href="{{ route('group.tab.score', $group->id) }}">
                            <i class="fa fa-plus"
                               aria-hidden="true"></i> {{__('language.Create_Test_Score')}}
                        </a>
                        @endcan
                        @if(Session::has('add_score_success'))
                            <p class="text-success no-margin">
                                <i class="fa fa-check" aria-hidden="true"></i>
                                {{Session::get('add_score_success')}}
                            </p>
                        @endif
                        @if(Session::has('edit_score_success'))
                            <p class="text-success no-margin">
                                <i class="fa fa-check" aria-hidden="true"></i>
                                {{Session::get('edit_score_success')}}
                            </p>
                        @endif
                        @if(Session::has('delete_score_success'))
                            <p class="text-success no-margin">
                                <i class="fa fa-check" aria-hidden="true"></i>
                                {{Session::get('delete_score_success')}}
                            </p>
                        @endif
                        <form action="{{ route('group.show.score', $group->id) }}"
                              method="get"
                              class="pt-3">
                            <!--course-->
                            <div class="form-group">
                                <label class="col-form-label">{{__('language.Choose_Course')}}</label>
                                @if(isset($courses))
                                    <select class="js-example-basic-single" style="width: 100%" name="selectCourse"
                                            onchange="this.form.submit()">
                                        <option value="">{{__('language.Choose_Course')}}</option>
                                        @foreach($courses as $key=>$course)
                                            @if (isset($selectCourse))
                                                <option value="{{ $course->id }}" {{ ($course->id === $selectCourse->id) ? 'selected' : '' }} >{{ $course->course_name }}</option>
                                            @else
                                                <option value="{{ $course->id }}">{{ $course->course_name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                @else
                                    <p class="text-danger">{{__('language.Empty_Data')}}</p>
                                @endif
                            </div>
                        </form>
                        @if (isset($exams))
                            <div class="table-responsive scroll-table">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th style="width: 5%" class="border-top-0 text-secondary text-center">#</th>
                                        <th style="width: 15%" class="border-top-0">{{__('language.Student_Id')}}</th>
                                        <th style="width: 20%" class="border-top-0">{{__('language.Full_Name')}}</th>
                                        @foreach($exams as $key=>$exam)
                                            <th class="border-top-0">{{ ($exam->date) ? $exam->date->format('d/m/Y') : '' }}</th>
                                        @endforeach
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($group->students as $key => $studentGroup)
                                        <tr>
                                            <td class="border-top-0">{{++$key}}
                                                <input type="hidden" placeholder="" value="{{ $studentGroup->id }}"
                                                       name="studentId[]">
                                            </td>
                                            <td class="border-top-0">
                                                <a class="p-0"
                                                   href="{{route('student.details',[$studentGroup->id, $studentGroup->group_id])}}">{{$studentGroup->student_id}}</a>
                                            </td>
                                            <td class="border-top-0">
                                                <a class="p-0"
                                                   href="{{route('student.details',[$studentGroup->id, $studentGroup->group_id])}}">{{$studentGroup->first_name .' '.$studentGroup->last_name}}</a>
                                            </td>
                                            @foreach($exams as $key=>$exam)
                                                <td class="border-top-0">
                                                    @foreach($exam->students as $score)
                                                        @if ($score->id === $studentGroup->id)
                                                            {{ $score->pivot->score }}
                                                        @endif
                                                    @endforeach
                                                </td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <tr>
                                <td class="h6 text-muted">
                                    <i class="fa fa-info-circle"></i>
                                    {{__('language.Empty_Data')}}
                                </td>
                            </tr>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection