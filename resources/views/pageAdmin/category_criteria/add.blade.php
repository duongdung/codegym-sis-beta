@extends('layouts.app')

@section('head.title')
    {{__('language.create_category_criteria')}}
@endsection

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted">{{__('language.create_category_criteria')}}</li>
    </ol>
    <div class="Category">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <form class="form-horizontal" action="{{ route('criteria.storeCategory',['reportId'=>$groups_criteria->report_id,'group-criteria'=>$groups_criteria->id]) }}"
                      method="POST">
                    {{ csrf_field() }}
                    <div class="col-12 col-ms-12 col-md-12 col-lg-12  user-personal-content">
                        <div class="row">
                            <div class="col-12 col-md-12 col-lg-12 ">
                                <p class="col-12 col-md-11 col-lg-12 text-muted p-2 pr-5">
                                    {{ __('language.Fields_with') }}
                                    <span class="text-danger">*</span>
                                    {{ __('language.Are_required') }}
                                </p>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-12 col-md-4 col-lg-2 col-form-label">{{__('language.name')}} <span class="text-danger">*</span></label>
                                    <div class="col-12 col-md-8 col-lg-10 no-padding">
                                        <input class="form-control @if($errors->has('name')) is-invalid @endif" type="text" value="{{ old('name') }}" name="name" required>
                                        @if($errors->has('name'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('name')  }}
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputHousehold"
                                               class=" col-12 col-md-4 col-lg-2 col-form-label">{{__('language.group_criteria')}}
                                            <span class="align-middle text-danger">*</span>
                                    </label>
                                    <div class="col-12 col-md-8 col-lg-10 pt-2">
                                            <lablel>{{ $groups_criteria->name }}</lablel>
                                    </div>
                                </div>

                                <div class="col-10 col-md-12 col-lg-11">
                                    <div class="row justify-content-end">
                                        <button type="submit"
                                                class="btn btn-primary button-submit">{{__('language.create')}}</button>
                                        <a href="{{ route('report.detail',$reportId) }}">
                                            <input type="button"
                                                   class=" btn btn-outline-secondary button-submit"
                                                   value="{{__('language.Cancel')}}">
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div> <!-- Hết personal -->

                </form>

            </div>

        </div>

    </div>

@endsection