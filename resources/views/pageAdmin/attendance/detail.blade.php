@extends('layouts.app')
@section('head.title')
    {{__('language.attendance_report')}}
@endsection

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item "><a href="{{ route('group.list') }}">{{__('language.Group')}}</a></li>
        <li class="breadcrumb-item text-muted">{{ $attendance->group->group_id }}</li>
        <li class="breadcrumb-item text-muted">{{__('language.attendance_report') .': '.$attendance->created_at->format('d/m/Y')}}</li>
    </ol>
    <div class="main">
        <div class="row">
            <div class="col-12 col-md-12 ">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <!-- Tab info -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-home-tab"
                           href="{{ route('group.detail',$attendance->group->id) }}">
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                            {{ __('language.Group_detail') }}
                        </a>
                    </li>

                    <!-- Tab student -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                           href="{{ route('group.student',$attendance->group->id) }}">
                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                            {{ __('language.Student') }}
                        </a>
                    </li>

                    <!-- Tab Log -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                           href="{{ route('group.log', $attendance->group->id) }}">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                            {{ __('language.Logs') }}
                        </a>
                    </li>

                <!-- Tab Attendance-->
                    @can('create-edit-view-attendance', $attendance->group)
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary active" id="nav-profile-tab"
                               href="{{ route('attendance.create', $attendance->group->id) }}">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                {{ __('language.Attendance') }}
                            </a>
                        </li>
                    @endcan

                <!-- Tab exam score -->
                    @can('view-group-score', $group)
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                               href="{{ route('group.show.score',$attendance->group->id) }}">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                {{ __('language.Test_Score') }}
                            </a>
                        </li>
                    @endcan
                </ul>
                <div class="border-list p-4">
                    @include('errors.success')
                    <div class="edit-attendance text-right">
                        @can('create-edit-view-attendance', $attendance->group)
                            <a class="pr-3" href="{{ route('attendance.update',['groupId'=>$attendance->group->id,'attendance'=>$attendance->id]) }}">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        @endcan

                        @can('delete-attendance', Session::get('centerId'))
                            <a href="{{ route('attendance.delete',['groupId'=>$attendance->group->id,'attendance'=>$attendance->id]) }}">
                                <i class="fa fa-times text-danger" aria-hidden="true"></i>
                            </a>
                        @endcan
                    </div>

                    <table id="myTable" class="table table-hover">
                        <thead>
                        <tr>
                            <th class="border-top-0">#</th>
                            <th class="border-top-0">{{__('language.ID')}}</th>
                            <th class="border-top-0">{{__('language.full_name')}}</th>
                            <th class="border-top-0">{{__('language.status')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($attendance->attendancedetails as $key => $item )
                            <tr>
                                <td class="border-top-0">{{ ++$key }}</td>
                                <td class="border-top-0"><a
                                            href="{{ route('student.details',[$item->student->id, $item->student->group_id])}}">{{$item->student->student_id}}</a>
                                </td>
                                <td class="border-top-0">{{$item->student->firt_name}} {{$item->student->last_name}}</td>
                                <td class="border-top-0">
                                    @if($item->attendance == 1)
                                        <div class="text-danger">{{ __('language.absent') }}</div>
                                    @elseif($item->attendance == 2)
                                        <div class="text-warning">{{ __('language.leave') }}</div>
                                    @elseif($item->attendance == 3)
                                        {{ __('language.late') }}
                                    @else
                                        <div class="text-success">{{ __('language.present') }}</div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection