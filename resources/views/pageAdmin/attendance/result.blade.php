@extends('layouts.app')
@section('head.title')
    {{__('language.Attendancedetail')}}
@endsection

@section('content')
    <ol class="breadcrumb">

        <li class="breadcrumb-item text-muted"><a href="{{ route('group.list') }}">{{ __('language.Group') }}</a></li>
        <li class="breadcrumb-item text-muted">{{ $group->group_id }}</li>
    </ol>
    <div class="main">
        <div class="row">
            <div class="col-12 col-md-12 ">
                <ul class="nav nav-tabs" id="myTab" role="tablist">

                    <!-- Tab info -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-home-tab" href="{{ route('group.detail',$group->id) }}">
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                            {{ __('language.Group_detail') }}
                        </a>
                    </li>

                    <!-- Tab student -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                           href="{{ route('group.student',$group->id) }}">
                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                            {{ __('language.Student') }}
                        </a>
                    </li>

                    <!-- Tab Log -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                           href="{{ route('group.log', $group->id) }}">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                            {{ __('language.Logs') }}
                        </a>
                    </li>

                <!-- Tab Attendance-->
                    @can('create-edit-view-attendance', $group)
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary active" id="nav-profile-tab"
                               href="{{ route('attendance.create', $group->id) }}">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                {{ __('language.Attendance') }}
                            </a>
                        </li>
                    @endcan

                <!-- Tab exam score -->
                    @can('view-group-score', $group)
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                               href="{{ route('group.show.score',$group->id) }}">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                {{ __('language.Test_Score') }}
                            </a>
                        </li>
                    @endcan
                </ul>
                @if(count($attendances) == 0)
                    <div class="col-md-12">
                        <p class="text-danger">{{ __('language.No_Data') }}</p>
                    </div>
                @else
                    <div class="border-list p-4">
                        <div class=" table-responsive" style="overflow-y: auto">
                            <table id="myTable" class="table table-hover">
                                <thead>
                                <tr>
                                    <th class="border-top-0">#</th>
                                    <th class="border-top-0">{{__('language.ID')}}</th>
                                    <th class="border-top-0">{{__('language.full_name')}}</th>
                                    @foreach($attendances as $attendance)
                                        <th class="border-top-0">{{$attendance->created_at->format('d-m-Y')}}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($students as $index => $student)
                                    <tr>
                                        <td class="border-top-0">{{$index + 1}}</td>
                                        <td class="border-top-0"><a
                                                    href="{{route('student.details',[$student->id, $student->group_id])}}">{{$student->student_id}}</a>
                                        </td>
                                        <td class="border-top-0">{{ $student->firt_name }} {{$student->last_name}}</td>
                                        @foreach($student->attendances as $attendance)

                                            @if($attendance->pivot->attendance == 1)
                                                <td class="border-top-0"><p
                                                            class="text-danger">{{__('language.absent')  }}</p>
                                                </td>
                                            @elseif($attendance->pivot->attendance == 2)
                                                <td class="border-top-0"><p
                                                            class="text-capitalize">{{__('language.leave')  }}</p>
                                                </td>
                                            @elseif($attendance->pivot->attendance == 3)
                                                <td class="border-top-0"><p
                                                            class="text-primary">{{__('language.late')}}</p>
                                                </td>
                                            @else
                                                <td class="border-top-0"><p
                                                            class="text-success">{{__('language.present')  }}</p>
                                                </td>
                                            @endif
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{--@endforeach--}}
                        </div>
                    </div>
            </div>
        </div>
        @endif
    </div>
@endsection

