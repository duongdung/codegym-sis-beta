@extends('layouts.app')
@section('head.title')
    {{__('language.Attendancedetail')}}
@endsection
@section('content')
    @if(count($attendances) == 0)
        <p class="text-danger">{{ __('language.No_Data') }}</p>
    @else
        <ol class="breadcrumb">
            <li class="breadcrumb-item text-muted"><a href="{{ route('group.list') }}">{{__('language.Group')}}</a></li>
            <li class="breadcrumb-item text-muted">{{$group->group_id }}</li>
            <li class="breadcrumb-item text-muted"> {{__('language.Attendancedetail')}}</li>
        </ol>
        <div class="main">
            <div class="row">
                <div class="col-12 col-md-12 ">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <!-- Tab info -->
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary" id="nav-home-tab"
                               href="{{ route('group.detail',$group->id) }}">
                                <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                {{ __('language.Group_detail') }}
                            </a>
                        </li>

                        <!-- Tab student -->
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                               href="{{ route('group.student',$group->id) }}">
                                <i class="fa fa-user-circle" aria-hidden="true"></i>
                                {{ __('language.Student') }}
                            </a>
                        </li>

                        <!-- Tab Log -->
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                               href="{{ route('group.log', $group->id) }}">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                {{ __('language.Logs') }}
                            </a>
                        </li>

                    <!-- Tab Attendance-->
                        @can('create-edit-view-attendance', $group)
                            <li class="nav-item">
                                <a class="nav-item nav-link text-secondary active" id="nav-profile-tab"
                                   href="{{ route('attendance.create', $group->id) }}">
                                    <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                    {{ __('language.Attendance') }}
                                </a>
                            </li>
                        @endcan

                    <!-- Tab exam score -->
                        @can('view-group-score', $group)
                            <li class="nav-item">
                                <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                                   href="{{ route('group.show.score',$group->id) }}">
                                    <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                    {{ __('language.Test_Score') }}
                                </a>
                            </li>
                        @endcan
                    </ul>
                    <div class="border-list p-4">
                        @include('errors.success')
                        <div class="title">
                            <ul>
                                <li>
                                    <a href="{{ route('attendancedetail.showfive',['groupId'=>$group->id]) }}">{{__('language.view-5')}}</a>
                                </li>
                                <li>{{__('language.Attendancedetail')}} :</li>
                            </ul>
                        </div>
                        <div class="attend-date">
                            <form action="{{ route('attendancedetail.date',$group->id) }}"
                                  method="post">
                                {{ csrf_field() }}
                                <div class="data col-12 col-md-12">
                                    <div class="row">
                                        <div class="form-group row col-12 col-md-4">
                                            <label for="example-date-input"
                                                   class="col-2 col-form-label">{{__('language.from')}}</label>
                                            <div class="col-10 no-padding">
                                                <input class="form-control @if($errors->has('date_to')) is-invalid @endif"
                                                       type="date" id="example-date-input" name="date_to">
                                                @if($errors->has('date_to'))
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('date_to')  }}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row col-12 col-md-4">
                                            <label for="example-date-input"
                                                   class="col-2 col-md-2 col-form-label">{{__('language.to')}}</label>
                                            <div class="col-10 no-padding">
                                                <input class="form-control @if($errors->has('date_from')) is-invalid @endif"
                                                       type="date" value="{{ date("Y-m-d") }}" id="example-date-input"
                                                       name="date_from">
                                            </div>
                                            @if($errors->has('date_from'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('date_from')  }}
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-12 col-md-1 pl-2 text-center">
                                            <button class="btn btn-primary" type="submit"
                                                    value="submit">{{__('language.See')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <div class="row">
                                <div class="col-12 no-padding text-center">
                                    <table id="myTable" class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th class="border-top-0 text-center">#</th>
                                            <th class="border-top-0 text-center">{{__('language.date')}}</th>
                                            <th class="border-top-0 text-center">{{__('language.teacher')}}</th>
                                            <th class="border-top-0 text-center">{{__('language.absent') }}</th>
                                            <th class="border-top-0 text-center">{{__('language.leave') }}</th>
                                            <th class="border-top-0 text-center">{{__('language.late') }}</th>
                                            <th class="border-top-0 text-center">{{__('language.present') }}</th>
                                            <th class="border-top-0 text-center">{{__('language.total_student')}}</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($attendances as $key => $attendance )
                                            <tr>
                                                <td class="border-top-0">{{ ++$key }}</td>
                                                <td class="border-top-0"><a
                                                            href="{{ route('attendance.detail',['groupId'=>$group->id,'attendance'=>$attendance->id])}}"> {{$attendance->created_at->format('d/m/Y')}}</a>
                                                </td>
                                                <td class="border-top-0"><a href="{{ route('user.detail',$attendance->user->id) }}">{{ $attendance->user->first_name .' '.$attendance->user->last_name }}</a></td>
                                                <td class="border-top-0">{{$attendance->absent_count  }}</td>
                                                <td class="border-top-0">{{$attendance->leave_count  }}</td>
                                                <td class="border-top-0">{{$attendance->late_count  }}</td>
                                                <td class="border-top-0">{{$attendance->present_count  }}</td>
                                                <td class="border-top-0">{{$attendance->group->students->count()}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="pagination" style="margin-top: 10px;float: right">
                                {{ $attendances->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!--Hết -->
@endsection