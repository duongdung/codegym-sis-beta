@extends('layouts.app')

@section('head.title')
    {{ __('language.Attendance') }}
@endsection

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('group.list') }}">{{ __('language.Group') }}</a></li>
        <li class="breadcrumb-item text-muted">{{ $group->group_id }}</li>
    </ol>

    <div class="main">
        <div class="row">
            <div class="col-12 col-md-12 ">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <!-- Tab info -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-home-tab" href="{{ route('group.detail',$group->id) }}">
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                            {{ __('language.Group_detail') }}
                        </a>
                    </li>

                    <!-- Tab student -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                           href="{{ route('group.student',$group->id) }}">
                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                            {{ __('language.Student') }}
                        </a>
                    </li>

                    <!-- Tab Log -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                           href="{{ route('group.log', $group->id) }}">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                            {{ __('language.Logs') }}
                        </a>
                    </li>

                    <!-- Tab Attendance-->
                    @can('create-edit-view-attendance', $group)
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary active" id="nav-profile-tab"
                               href="{{ route('attendance.create', $group->id) }}">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                {{ __('language.Attendance') }}
                            </a>
                        </li>
                    @endcan

                <!-- Tab exam score -->
                    @can('view-group-score', $group)
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                               href="{{ route('group.show.score',$group->id) }}">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                {{ __('language.Test_Score') }}
                            </a>
                        </li>
                    @endcan
                </ul>
                <div class="border-list p-4">
                    <a href="{{ route('attendance.list', $group->id) }}">
                        <i class="fa fa-file-text-o" aria-hidden="true"></i>
                        {{__('language.View_Attendance')}}
                    </a>
                    <form action="{{ route('attendance.store',$group->id) }}" class="mt-4" method="post">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label for="example-date-input"
                                   class="col-3 col-md-1 col-form-label">{{__('language.date')}}:</label>
                            <div class="col-9 col-md-3 no-padding">
                                <input class="form-control @if($errors->has('date_attedance')) is-invalid @endif"
                                       type="date" value="{{ date("Y-m-d") }}" id="example-date-input"
                                       name="date_attedance">
                                @if($errors->has('date_attedance'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('date_attedance')  }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-12 col-md-12 table-responsive text-center">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th class="text-center border-top-0">#</th>
                                    <th class="text-center border-top-0">{{__('language.ID')}}</th>
                                    <th class="text-center border-top-0">{{__('language.full_name')}}</th>
                                    <th class="text-center border-top-0">{{__('language.absent') }}</th>
                                    <th class="text-center border-top-0">{{__('language.leave') }}</th>
                                    <th class="text-center border-top-0">{{__('language.late') }}</th>
                                    <th class="text-center border-top-0">{{__('language.present') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($group->students as $key => $student)
                                    <tr>
                                        <td class="border-top-0">{{++$key}}</td>
                                        <td class="border-top-0"><a
                                                    href="{{ route('student.details', [$student->id, $student->group_id]) }}">{{ $student->student_id }}</a>
                                        </td>
                                        <td class="border-top-0">{{ $student->firt_name }} {{$student->last_name}}</td>
                                        <td class="border-top-0">
                                            <label class="custom-control custom-radio mr-1 ">
                                                <input id="radio1" name="attend[{{$student->id}}]" type="radio"
                                                       class="custom-control-input" value="1" checked>
                                                <span class="custom-control-indicator"></span>
                                            </label>

                                        </td>
                                        <td class="border-top-0">
                                            <label class="custom-control custom-radio mr-1 ">
                                                <input id="radio1" name="attend[{{$student->id}}]" type="radio"
                                                       class="custom-control-input" value="2">
                                                <span class="custom-control-indicator"></span>
                                            </label>

                                        </td>
                                        <td class="border-top-0">
                                            <label class="custom-control custom-radio mr-1 ">
                                                <input id="radio1" name="attend[{{$student->id}}]" type="radio"
                                                       class="custom-control-input" value="3">
                                                <span class="custom-control-indicator"></span>
                                            </label>

                                        </td>
                                        <td class="border-top-0">
                                            <label class="custom-control custom-radio mr-1">
                                                <input id="radio1" name="attend[{{$student->id}}]" type="radio"
                                                       class="custom-control-input" value="4">
                                                <span class="custom-control-indicator"></span>
                                            </label>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12 justify-content-center pb-4" align="center">
                            <button type="submit"
                                    class="btn btn-primary button-submit">{{__('language.Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


