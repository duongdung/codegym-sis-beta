@extends('layouts.app')
@section('head.title')
    {{ __('language.Delete_Attendance') }}
@endsection
@section('content')

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-12 col-md-8" style="height: 300px;border-radius: 5px;">

                    <div class="delete" style="padding-top: 3em" >
                        <div class="name-program text-center">
                            <p class="pt-10"><h5 class="text-danger">{{__('language.Do_you_deleteAttendance')}}:
                                <strong>{{ $attendance->group->group_id .'('.$attendance->created_at->format('d/m/Y').')'}}?</strong></h5></p>
                        </div>

                        <div class="form-group row justify-content-md-center">
                            <form action="{{ route('attendance.destroy',['groupId'=>$attendance->group->id,'attendance'=>$attendance->id]) }}" method="post">
                                {{ csrf_field() }}
                                <div class="col-6 col-md-3">
                                    <button type="submit" class="btn btn-danger btn-lg">{{__('language.Delete')}}</button>
                                </div>
                            </form>
                            <div class="col-6 col-md-3 ">
                                <a href="{{ route('attendance.detail',['groupId'=>$attendance->group->id,'attendance'=>$attendance->id]) }}"><button type="button" class="btn btn-outline-secondary btn-lg">{{__('language.Cancel')}}</button></a>
                            </div>

                        </div>
                    </div>

            </div>
        </div>
    </div>

@endsection