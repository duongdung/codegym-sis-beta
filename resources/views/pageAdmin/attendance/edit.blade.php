@extends('layouts.app')

@section('head.title')
    {{ __('language.Edit_Attendance') }}
@endsection

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted"><a href="{{ route('group.list') }}">{{ __('language.Group') }}</a></li>
        <li class="breadcrumb-item text-muted">{{ $attendance->group->group_id }}</li>
        <li class="breadcrumb-item text-muted">{{ __('language.Edit_Attendance').': '.$attendance->created_at->format('d/m/Y') }}</li>
    </ol>

    <div class="main">
        <div class="row">
            <div class="col-12 col-md-12 ">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <!-- Tab info -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-home-tab"
                           href="{{ route('group.detail',$attendance->group->id) }}">
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                            {{ __('language.Group_detail') }}
                        </a>
                    </li>

                    <!-- Tab student -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                           href="{{ route('group.student',$attendance->group->id) }}">
                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                            {{ __('language.Student') }}
                        </a>
                    </li>

                    <!-- Tab Log -->
                    <li class="nav-item">
                        <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                           href="{{ route('group.log', $attendance->group->id) }}">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                            {{ __('language.Logs') }}
                        </a>
                    </li>

                <!-- Tab Attendance-->
                    @can('create-edit-view-attendance', $attendance->group)
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary active" id="nav-profile-tab"
                               href="{{ route('attendance.create', $attendance->id) }}">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                {{ __('language.Attendance') }}
                            </a>
                        </li>
                    @endcan

                <!-- Tab exam score -->
                    @can('view-group-score', $group)
                        <li class="nav-item">
                            <a class="nav-item nav-link text-secondary" id="nav-profile-tab"
                               href="{{ route('group.show.score',$attendance->group->id) }}">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                {{ __('language.Test_Score') }}
                            </a>
                        </li>
                    @endcan
                </ul>
                <div class="border-list p-4">
                    <form action="{{ route('attendance.edit',['groupId'=>$attendance->group->id,'attendance'=>$attendance->id])}}" method="post">
                        {{ csrf_field() }}
                        <div class="col-12 col-md-12 table-responsive text-center">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th class="text-center border-top-0">#</th>
                                    <th class="text-center border-top-0">{{__('language.ID')}}</th>
                                    <th class="text-center border-top-0">{{__('language.full_name')}}</th>
                                    <th class="text-center border-top-0">{{__('language.absent') }}</th>
                                    <th class="text-center border-top-0">{{__('language.leave') }}</th>
                                    <th class="text-center border-top-0">{{__('language.late') }}</th>
                                    <th class="text-center border-top-0">{{__('language.present') }}</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($attendance->attendancedetails as $key => $attendancedetail)

                                    <tr>
                                        <td class="border-top-0">{{ ++$key }}</td>
                                        <td class="border-top-0"><a
                                                    href="">{{ $attendancedetail->student->student_id }}</a>
                                        </td>
                                        <td class="border-top-0">{{$attendancedetail->student->first_name.' '.$attendancedetail->student->last_name}}</td>
                                        <td class="border-top-0">
                                            <label class="custom-control custom-radio mr-1 ">
                                                <input id="radio1" name="attend[{{$attendancedetail->student_id}}]"
                                                       type="radio" @if($attendancedetail->attendance == 1) checked
                                                       @else @endif class="custom-control-input" value="1">
                                                <span class="custom-control-indicator"></span>
                                            </label>

                                        </td>
                                        <td class="border-top-0">
                                            <label class="custom-control custom-radio mr-1 ">
                                                <input id="radio1" name="attend[{{$attendancedetail->student_id}}]"
                                                       type="radio" @if($attendancedetail->attendance == 2) checked
                                                       @else @endif class="custom-control-input" value="2">
                                                <span class="custom-control-indicator"></span>
                                            </label>

                                        </td>
                                        <td class="border-top-0">
                                            <label class="custom-control custom-radio mr-1 ">
                                                <input id="radio1" name="attend[{{$attendancedetail->student_id}}]"
                                                       type="radio" @if($attendancedetail->attendance == 3) checked
                                                       @else @endif class="custom-control-input" value="3">
                                                <span class="custom-control-indicator"></span>
                                            </label>

                                        </td>
                                        <td class="border-top-0">
                                            <label class="custom-control custom-radio mr-1">
                                                <input id="radio1" name="attend[{{$attendancedetail->student_id}}]"
                                                       type="radio" @if($attendancedetail->attendance == 4) checked
                                                       @else @endif class="custom-control-input" value="4">
                                                <span class="custom-control-indicator"></span>
                                            </label>

                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="col-10 col-md-12">
                            <div class="row justify-content-end">
                                <button type="submit"
                                        class="btn btn-primary button-submit">{{__('language.Save')}}</button>
                                <a href="{{ route('attendance.detail',['groupId'=>$attendance->group->id,'attendance'=>$attendance->id])}}">
                                    <input type="button"
                                           class=" btn btn-outline-secondary button-submit"
                                           value="{{__('language.Cancel')}}">
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


