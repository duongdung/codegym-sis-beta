<form class="col-12 col-sm-8 col-md-6 col-lg-5  form-inline navbar-right" action="" method="GET">
    {{ csrf_field() }}
    <input class="form-control mr-sm-2" type="text" name="searchStudent" placeholder="{{__('language.search')}}"
           aria-label="Search">
    <button class="btn btn-primary my-2 my-sm-0"
            type="submit" value="searchStudent">{{ __('language.search') }}</button>
</form> <!-- Hết search bar -->