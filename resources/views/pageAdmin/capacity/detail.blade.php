@extends('layouts.app')
@section('head.title')
    {{__('language.evaluator_details')}}
@endsection

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted">{{__('language.Student')}} {{ $evaluate->student->first_name .' '. $evaluate->student->last_name }}</li>
        <li class="breadcrumb-item text-muted">{{__('language.evaluate_capacity')}}</li>
        <li class="breadcrumb-item text-muted">{{__('language.evaluator_details')}}</li>
    </ol>
    <div class="main">
        <div class="row">
            <div class="col-12 col-md-12 ">
                <ul class=" nav nav-tabs responsive" role="tablist">

                    <!-- Tab thong tin ca nhan -->
                    <li class="nav-item">
                        <a class="nav-link text-secondary"
                           href="{{ route('student.details', [$evaluate->student->id, $evaluate->student->group_id]) }}">{{__('language.Personal')}}</a>
                    </li>
                @can('view-list-students',Session::get('centerId'))
                    <!-- Tab lop hoc -->
                        <li class="nav-item">
                            <a class="nav-link text-secondary"
                               href="{{ route('student.group', $evaluate->student->id) }}">{{__('language.Group')}}</a>
                        </li>
                @endcan
                @can('add-student-log', $evaluate->student)
                    <!-- Tab nhat ky -->
                        <li class="nav-item">
                            <a class="nav-link text-secondary"
                               href="{{ route('student.log', $evaluate->student->id) }}">{{__('language.Logs')}}
                            </a>
                        </li>
                @endcan
                @can('crud-capacity',$group)
                    <!-- Tab bao cao -->
                        <li class="nav-item">
                            <a class="nav-link text-secondary active"
                               href="{{ route('student.capacity', [$evaluate->student->id,$group->id]) }}"
                            >{{__('language.evaluate_capacity')}}</a>
                        </li>
                @endcan
                @can('crud-score',$evaluate->student)
                    <!-- Tab diem thi -->
                        <li class="nav-item">
                            <a class="nav-link text-secondary active"
                               href="{{ route('student.tab.score', $evaluate->student->id) }}"
                            >{{__('language.Test_Score')}}</a>
                        </li>
                @endcan
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="border-list p-4">

                        <div class="table-responsive">
                            <div class="delete-capacity text-right">
                                <a class="pr-3" href="{{route('evaluate.update',[$evaluate->id,'student'=>$evaluate->student_id,'group'=>$group->id]) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="{{route('evaluate.delete',[$evaluate->id,'student'=>$evaluate->student_id,'group'=>$group->id])}}"> <i class="fa fa-times text-danger" aria-hidden="true"></i></a>
                            </div>
                            <table id="myTable" class="table table-hover">
                                <thead>
                                <tr>
                                    <th style="width: 5%" class="border-top-0">#</th>
                                    <th style="width: 40%" class="border-top-0">{{ __('language.criteria') }}</th>
                                    <th class="border-top-0">{{__('language.status')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($evaluate->criterias as $key => $criteria)
                                    <tr>
                                        <td class="border-top-0">{{++$key}}</td>
                                        <td class="border-top-0">{{$criteria->name}}</td>
                                        <td class="border-top-0">
                                            @if($criteria->pivot->capacity == 0)
                                                <div class="text-danger">{{'N/A'}}</div>
                                            @elseif($criteria->pivot->capacity == 1)
                                                <div class="text-warning">{{__('language.cant_obtain')}}</div>
                                            @elseif($criteria->pivot->capacity == 2)
                                                {{__('language.obtain')}}
                                            @elseif($criteria->pivot->capacity == 3)
                                                <div class="text-success">{{__('language.good')}}</div>
                                            @else
                                                {{__('language.excellent')}}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection