@extends('layouts.app')
@section('head.title')
    {{ __('language.Edit') }}
@endsection

@section('content')

    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted">{{__('language.Student')}} {{ $evaluate->student->first_name .' '. $evaluate->student->last_name }}</li>
        <li class="breadcrumb-item active">{{__('language.Edit')}} </li>
    </ol>
    <div class="main">
        <div class="row">
            <div class="col-12 col-md-12 ">
                <ul class=" nav nav-tabs responsive" role="tablist">

                    <!-- Tab thong tin ca nhan -->
                    <li class="nav-item">
                        <a class="nav-link text-secondary"
                           href="{{ route('student.details', [$evaluate->student->id, $evaluate->student->group_id]) }}">{{__('language.Personal')}}</a>
                    </li>
                @can('view-list-students',Session::get('centerId'))
                    <!-- Tab lop hoc -->
                        <li class="nav-item">
                            <a class="nav-link text-secondary"
                               href="{{ route('student.group', $evaluate->student->id) }}">{{__('language.Group')}}</a>
                        </li>
                @endcan
                @can('add-student-log', $evaluate->student)
                    <!-- Tab nhat ky -->
                        <li class="nav-item">
                            <a class="nav-link text-secondary"
                               href="{{ route('student.log', $evaluate->student->id) }}">{{__('language.Logs')}}
                            </a>
                        </li>
                @endcan
                @can('crud-capacity',$group)
                    <!-- Tab bao cao -->
                        <li class="nav-item">
                            <a class="nav-link text-secondary active"
                               href="{{ route('student.capacity', [$evaluate->student->id,$group->id]) }}"
                            >{{__('language.evaluate_capacity')}}</a>
                        </li>
                @endcan
                @can('view-student-score', $student)
                    <!-- Tab diem thi -->
                        <li class="nav-item">
                            <a class="nav-link text-secondary active"
                               href="{{ route('student.tab.score', $evaluate->student->id) }}"
                            >{{__('language.Test_Score')}}</a>
                        </li>
                @endcan
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="border-list p-4">
                        <div class="table-responsive">
                            <form action="" method="post">
                                {{ csrf_field() }}
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th class="border-top-0">{{ __('language.criteria') }}</th>
                                        <th class="border-top-0">N/A</th>
                                        <th class="border-top-0">{{__('language.cant_obtain')}}</th>
                                        <th class="border-top-0">{{__('language.obtain')}}</th>
                                        <th class="border-top-0">{{__('language.good')}}</th>
                                        <th class="border-top-0">{{__('language.excellent')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($evaluate->criterias as $criteria)
                                        <tr>
                                            <td class="border-top-0">
                                                {{$criteria->name}}
                                            </td>
                                            <td class="border-top-0">
                                                <label class="custom-control custom-radio mr-1 ">
                                                    <input id="radio1" name="status[{{$criteria->id}}]" type="radio"
                                                           class="custom-control-input"
                                                           @if($criteria->pivot->capacity == 0) checked
                                                           @endif  value="0">
                                                    <span class="custom-control-indicator"></span>
                                                </label>
                                            </td>
                                            <td class="border-top-0">
                                                <label class="custom-control custom-radio mr-1 ">
                                                    <input id="radio1" name="status[{{$criteria->id}}]" type="radio"
                                                           class="custom-control-input"
                                                           @if($criteria->pivot->capacity == 1) checked
                                                           @endif value="1">
                                                    <span class="custom-control-indicator"></span>
                                                </label>
                                            </td>
                                            <td class="border-top-0">
                                                <label class="custom-control custom-radio mr-1 ">
                                                    <input id="radio1" name="status[{{$criteria->id}}]" type="radio"
                                                           class="custom-control-input"
                                                           @if($criteria->pivot->capacity == 2) checked
                                                           @endif value="2">
                                                    <span class="custom-control-indicator"></span>
                                                </label>
                                            </td>
                                            <td class="border-top-0">
                                                <label class="custom-control custom-radio mr-1 ">
                                                    <input id="radio1" name="status[{{$criteria->id}}]" type="radio"
                                                           class="custom-control-input"
                                                           @if($criteria->pivot->capacity == 3) checked
                                                           @endif value="3">
                                                    <span class="custom-control-indicator"></span>
                                                </label>
                                            </td>
                                            <td class="border-top-0">
                                                <label class="custom-control custom-radio mr-1 ">
                                                    <input id="radio1" name="status[{{$criteria->id}}]" type="radio"
                                                           class="custom-control-input"
                                                           @if($criteria->pivot->capacity == 4) checked
                                                           @endif value="4">
                                                    <span class="custom-control-indicator"></span>
                                                </label>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                                <div class="col-10 col-md-12">
                                    <div class="row justify-content-end">
                                        <button type="submit"
                                                class="btn btn-primary button-submit">{{__('language.Edit')}}</button>
                                        <a href="{{ route('evaluate.detail',[$evaluate->id,'student'=>$evaluate->student_id,'group'=>$group->id]) }}">
                                            <input type="button"
                                                   class=" btn btn-outline-secondary button-submit"
                                                   value="{{__('language.Cancel')}}">
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection