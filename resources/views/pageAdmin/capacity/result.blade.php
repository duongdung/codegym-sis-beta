@extends('layouts.app')

@section('head.title')
    {{ __('language.result_evaluate') }}
@endsection
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted"><a href="{{ route('student.details',[$student->id,$student->group_id]) }}">{{__('language.Student')}} {{ $student->first_name .' '. $student->last_name }}</a></li>
        <li class="breadcrumb-item text-muted">{{__('language.result_evaluate')}}</li>
    </ol>
    <div class="main" >
        <div class="col-12">
            <div class="row">
                <div class="col-12 report-name text-center">
                    <h4><a href="">{{$evaluations->first()->report->name}}</a></h4>
                </div>
                <div class="col-12">
                    <div class="col-12 evaluate-detail table-responsive">
                        <table class="table table-bordered"  id="tableID">

                            <tbody>
                            <tr>
                                <td rowspan="2" style="vertical-align: inherit">#</td>
                                <td rowspan="2" style="vertical-align: inherit">Năng Lực</td>
                                <td colspan="{{ $evaluation_count }}" class="text-center">Tiến độ/Đánh giá</td>
                            </tr>
                            <tr>
                                @foreach($evaluations as $key=> $evaluation)
                                <td>Kỳ {{++$key}}</td>
                                @endforeach
                            </tr>

                            @foreach($criterias as $key=> $criteria)
                            <tr>
                                <th scope="row">{{++$key}}</th>
                                <td>{{$criteria->name}}</td>
                                @foreach($criteria->evaluates as $key=> $evaluate)
                                    @if($evaluate->pivot->capacity == 0)
                                        <td class="border-top-0"><p
                                                    class="text-danger">N/A</p>
                                        </td>
                                    @elseif($evaluate->pivot->capacity == 1)
                                        <td class="border-top-0"><p
                                                    class="text-capitalize">{{__('language.cant_obtain')}}</p>
                                        </td>
                                    @elseif($evaluate->pivot->capacity == 2)
                                        <td class="border-top-0"><p
                                                    class="text-primary">{{__('language.obtain')}}</p>
                                        </td>
                                    @elseif($evaluate->pivot->capacity == 3)
                                        <td class="border-top-0"><p
                                                    class="text-primary">{{__('language.good')}}</p>
                                        </td>
                                    @else
                                        <td class="border-top-0"><p
                                                    class="text-success">{{__('language.excellent')}}</p>
                                        </td>
                                    @endif
                                @endforeach
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
                @can('export-capacity',Session::get('centerId'))
                <div class="col-12 download">
                    <a href="{{route('exportData',[$student->id,$group->id])}}"><i class="fa fa-download" aria-hidden="true"></i> Download</a>
                </div>
                @endcan

            </div>

        </div>
    </div>

@endsection


