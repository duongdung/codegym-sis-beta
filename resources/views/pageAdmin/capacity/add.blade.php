@extends('layouts.app')

@section('head.title')
    {{ __('language.create_evaluate') }}
@endsection

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted">{{__('language.Student')}} {{ $student->first_name .' '. $student->last_name }}</li>
        <li class="breadcrumb-item text-muted">{{__('language.evaluate_capacity')}}</li>
        <li class="breadcrumb-item text-muted">{{__('language.create_evaluate')}}</li>
    </ol>
    <div class="main">
        <div class="row">
            <div class="col-12 col-md-12 ">
                <ul class=" nav nav-tabs responsive" role="tablist">

                    <!-- Tab thong tin ca nhan -->
                    <li class="nav-item">
                        <a class="nav-link text-secondary"
                           href="{{ route('student.details', [$student->id,$student->group_id]) }}">{{__('language.Personal')}}</a>
                    </li>
                @can('view-list-students',Session::get('centerId'))
                    <!-- Tab lop hoc -->
                        <li class="nav-item">
                            <a class="nav-link text-secondary"
                               href="{{ route('student.group', $student->id) }}">{{__('language.Group')}}</a>
                        </li>
                @endcan
                @can('add-student-log', $student)
                    <!-- Tab nhat ky -->
                        <li class="nav-item">
                            <a class="nav-link text-secondary"
                               href="{{ route('student.log', $student->id) }}">{{__('language.Logs')}}
                            </a>
                        </li>
                @endcan
                @can('crud-capacity',$group)
                    <!-- Tab bao cao -->
                        <li class="nav-item">
                            <a class="nav-link text-secondary active"
                               href="{{ route('student.capacity', [$student->id,$group->id]) }}"
                            >{{__('language.evaluate_capacity')}}</a>
                        </li>
                @endcan
                @can('view-student-score', $student)
                    <!-- Tab diem thi -->
                        <li class="nav-item">
                            <a class="nav-link text-secondary active"
                               href="{{ route('student.tab.score', $student->id) }}"
                            >{{__('language.Test_Score')}}</a>
                        </li>
                    @endcan
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="border-list p-4">
                        <div class="form-group row mt-3">
                            <label for="inputHousehold"
                                   class="col-12 col-md-2 col-form-label">{{__('language.Report')}}:
                            </label>

                            <form action="{{route('postReport')}}" class="form-report col-12 col-md-10" method="post">
                                {{ csrf_field() }}
                                @if(count($evaluates) > 0)
                                    <h4>
                                        <a href="{{ route('report.detail',$evaluates->first()->report->id)}}">{{$evaluates->first()->report->name}}</a>
                                    </h4>
                                @else
                                    <select class="col-md-8 col-lg-9 form-control" id="exampleSelect1"
                                            name="reportId" onchange='this.form.submit();'>
                                        @foreach($reports as $report)
                                            @if($report->id == Session::get('reportId'))
                                                <option selected value=" {{$report->id}}">{{$report->name}} <i
                                                            class="fa fa-angle-down" aria-hidden="true"></i>
                                                </option>
                                            @else
                                                <option value=" {{$report->id}}">{{$report->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                @endif
                            </form>
                        </div>
                        <div class="table-responsive mt-5">
                            <form action="{{ route('evaluate.store',['student'=>$student->id,'group'=>$groupId,'report'=>(count($evaluates) > 0) ? $evaluates->first()->report->id : Session::get('reportId')]) }}"
                                  method="post">
                                {{ csrf_field() }}
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>{{ __('language.criteria') }}</th>
                                        <th>N/A</th>
                                        <th>{{__('language.cant_obtain')}}</th>
                                        <th>{{__('language.obtain')}}</th>
                                        <th>{{__('language.good')}}</th>
                                        <th>{{__('language.excellent')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($groupsCriteria) == 0)
                                        <tr>
                                            <td colspan="6" class="text-center">{{__('language.Empty_Data')}}</td>
                                        </tr>
                                    @endif
                                    @foreach($groupsCriteria as $groupCriteria)
                                        @foreach($groupCriteria->categories_criteria as $categoryCriteria)
                                            @foreach($categoryCriteria->criterias as $key =>$criteria)
                                                <tr>
                                                    <td>
                                                        {{$criteria->name}}
                                                    </td>
                                                    <td>
                                                        <label class="custom-control custom-radio mr-1 ">
                                                            <input id="radio1" name="status[{{$criteria->id}}]"
                                                                   type="radio"
                                                                   class="custom-control-input" value="0" checked>
                                                            <span class="custom-control-indicator"></span>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label class="custom-control custom-radio mr-1 ">
                                                            <input id="radio1" name="status[{{$criteria->id}}]"
                                                                   type="radio"
                                                                   class="custom-control-input" value="1">
                                                            <span class="custom-control-indicator"></span>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label class="custom-control custom-radio mr-1 ">
                                                            <input id="radio1" name="status[{{$criteria->id}}]"
                                                                   type="radio"
                                                                   class="custom-control-input" value="2">
                                                            <span class="custom-control-indicator"></span>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label class="custom-control custom-radio mr-1 ">
                                                            <input id="radio1" name="status[{{$criteria->id}}]"
                                                                   type="radio"
                                                                   class="custom-control-input" value="3">
                                                            <span class="custom-control-indicator"></span>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label class="custom-control custom-radio mr-1 ">
                                                            <input id="radio1" name="status[{{$criteria->id}}]"
                                                                   type="radio"
                                                                   class="custom-control-input" value="4">
                                                            <span class="custom-control-indicator"></span>
                                                        </label>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endforeach
                                    @endforeach
                                    </tbody>
                                </table>
                                @if(count($groupsCriteria) > 0)
                                    <div class="col-10 col-md-12">
                                        <div class="row justify-content-end">
                                            <button type="submit"
                                                    class="btn btn-primary button-submit">{{__('language.create')}}</button>
                                            <a href="{{ route('student.capacity', [$student->id,$group->id]) }}">
                                                <input type="button"
                                                       class=" btn btn-outline-secondary button-submit"
                                                       value="{{__('language.Cancel')}}">
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection