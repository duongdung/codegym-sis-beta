@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-2">
            <div id="app">
                <passport-clients></passport-clients>
                <hr>
                <passport-authorized-clients></passport-authorized-clients>
                <hr>
                <passport-personal-access-tokens></passport-personal-access-tokens>
            </div>
        </div>
    </div>
</div>
@endsection

