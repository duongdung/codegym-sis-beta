@extends('layouts.app')
@section('head.title')
    {{ __('language.changepasss') }}
@endsection
@section('content')
  <div class="col-12 col-md-12 change-pass">
      <div class="card">
          <div class="card-header">
              {{__('language.changepasss')}}
          </div>
          <div class="card-block">
              <div class="col-md-12">
                  @include('errors.success')
              </div>
              <form action="{{ route('changePass.edit')}}" method="post">
                  {{ csrf_field() }}
                  <div class="row justify-content-center">
                      <div class="col-12 col-md-8 col-lg-7">
                          <div class="col-12 col-sm-12 col-md-12 pt-3">
                              <div class="form-group row">
                                  <label for="example-text-input" class="col-12 col-sm-4 col-md-5 col-lg-4 col-form-label">{{__('language.password_old')}} <span class="text-danger">*</span></label>
                                  <div class="col-12 col-sm-8 col-md-7 col-lg-8">
                                      <input class="form-control @if($errors->has('pass_old') ) is-invalid @endif" type="password"  id="example-text-input" name="pass_old" required>
                                      @if($errors->has('pass_old'))
                                          <div class="invalid-feedback">
                                              {{ $errors->first('pass_old')  }}
                                          </div>
                                      @endif
                                  </div>
                              </div>
                          </div>
                          <div class="col-12 col-md-12 pt-3">
                              <div class="form-group row">
                                  <label for="example-text-input" class="col-12 col-sm-4 col-md-5 col-lg-4 col-form-label">{{__('language.password_new')}} <span class="text-danger">*</span></label>
                                  <div class="col-12 col-md-7 col-lg-8">
                                      <input class="form-control @if($errors->has('pass_new')) is-invalid @endif" type="password"  id="example-text-input" name="pass_new">
                                      @if($errors->has('pass_new'))
                                          <div class="invalid-feedback">
                                              {{ $errors->first('pass_new')}}
                                          </div>
                                      @endif
                                  </div>
                              </div>
                          </div>
                          <div class="col-12 col-md-12 pt-3">
                              <div class="form-group row">
                                  <label for="example-text-input" class="col-12 col-md-5 col-lg-4 col-form-label">{{__('language.password_confirm')}} <span class="text-danger">*</span></label>
                                  <div class="col-12 col-md-7 col-lg-8">
                                      <input class="form-control @if($errors->has('pass_confim')) is-invalid @endif" type="password"  id="example-text-input" name="pass_confim">
                                      @if($errors->has('pass_confim'))
                                          <div class="invalid-feedback">
                                              {{ $errors->first('pass_confim')  }}
                                          </div>
                                      @endif
                                  </div>
                              </div>
                          </div>
                      </div>

                  </div>
                  <div class="col-12 col-md-12 text-center pb-2">
                      <button class="btn btn-primary button-submit" type="submit">{{__('language.changepasss')}}</button>
                  </div>

              </form>

          </div>
      </div>
  </div>

@endsection