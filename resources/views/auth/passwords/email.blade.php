@extends('layouts.reset-pass')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8 offset-md-2 pt-5">
            <div class="card">
                <div class="card-header">Reset Password</div>

                <div class="card-block pt-3">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="col-12 col-md-12">
                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group row {{ $errors->has('email') ? 'errors' : '' }}">
                            <label for="email" class="col-12 col-md-4 control-label"><h5 class="card-title">E-Mail Address</h5></label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row ">
                            <div class="col-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
