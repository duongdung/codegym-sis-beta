<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet">
    <link href="{{ asset('css/layout.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-te-1.4.0.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-datepicker.standalone.min.css') }}" rel="stylesheet">

    <!-- Page CSS -->
    <link href="{{ asset('css/tip-yellowsimple.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-editable.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="//codeorigin.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css"/>
    <title>@yield('head.title')</title>
    <link rel="icon" href="{{ asset('codegym-icon.png') }}">
</head>
@if (Auth::user())
<body class="fixed-nav sticky-footer" id="page-top">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top main-menu" id="mainNav">
        <a class="navbar-brand logo ml-5" href="{{ route('home') }}">
            <img src="{{ asset('logo-beta.png') }}" alt="">
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
        data-target="#navbarResponsive"
        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Main menu -->
    <div class="collapse navbar-collapse justify-content-end" id="navbarResponsive">

        {{--{{ $users }}--}}

        <!-- Dashboard menu -->
        <ul class="navbar-nav navbar-sidenav menu-left px-3 px-md-0" id="exampleAccordion">

            <!-- Menu Center -->
            @can('view-center')
            <li class="nav-item mb-md-3" data-toggle="tooltip" data-placement="right" title="Tables">
                <div class="row justify-content-between pl-3">
                    <a class="nav-link col-7 col-md-9 pl-3" href="{{ route('center.list') }}">
                        <i class="fa fa-university" aria-hidden="true"></i>
                        <span class="nav-link-text pl-2">{{__("language.Center")}}</span>
                    </a>
                    <a class="nav-link collapsed col-5 col-md-3 text-right text-md-left pr-5 pl-md-3 pr-md-0"
                    data-toggle="collapse"
                    href="#collapseCenter"
                    data-parent="#exampleAccordion">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </a>
            </div>
            <ul class="sidenav-second-level collapse {{ (Request::path() == 'centers/add-new' || Request::path() == 'centers') ? 'show' : '' }}"
            id="collapseCenter">
            @can('crud-center')
            <li class="menu-item {{ Request::path() == 'centers/add-new' ? 'active' : '' }}">
                <a href="{{ route('center.add') }}">{{__("language.Center_Add")}}</a>
            </li>
            @endcan
            <li class="menu-item {{ Request::path() == 'centers' ? 'active' : '' }}">
                <a href="{{ route('center.list') }}">{{__("language.List_Center")}}</a>
            </li>
        </ul>
    </li>
    @endcan

    <!--Menu Employee -->
    @can('crud-user', Session::get('centerId'))
    <li class="nav-item mb-md-3" data-toggle="tooltip" data-placement="right" title="Example Pages">
        <div class="row justify-content-between pl-3">
            <a class="nav-link col-7 col-md-9 pl-3" href="{{ route('user.list') }}">
                <i class="fa fa-user-circle" aria-hidden="true"></i>
                <span class="nav-link-text pl-2">
                    {{__("language.Employee")}}</span>
                </a>
                <a class="nav-link collapsed col-5 col-md-3 text-right text-md-left pr-5 pl-md-3 pr-md-0"
                data-toggle="collapse"
                href="#collapseEmployee"
                data-parent="#exampleAccordion">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </a>
        </div>
        <ul class="sidenav-second-level collapse {{ (Request::path() == 'users/add-new' || Request::path() == 'users')  ? 'show' : '' }}"
        id="collapseEmployee">
        <li class="{{ Request::path() == 'users/add-new' ? 'active' : '' }}">
            <a href="{{ route('user.create')}}">{{__("language.Add_Employees")}}</a>
        </li>
        <li class="{{ Request::path() == 'users' ? 'active' : '' }}">
            <a href="{{ route('user.list') }}">{{__("language.List_Employees")}}</a>
        </li>
    </ul>
</li>
@endcan

<!-- Menu Student -->
@can('view-list-students', Session::get('centerId'))
<li class="nav-item mb-md-3" data-toggle="tooltip" data-placement="right" title="Components">
    <div class="row justify-content-between pl-3">
        <a class="nav-link col-7 col-md-9 pl-3" href="{{ route('student.list') }}">
            <i class="fa fa-users" aria-hidden="true"></i>
            <span class="nav-link-text pl-2">
                {{__("language.Student")}}</span>
            </a>
            <a class="nav-link collapsed col-5 col-md-3 text-right text-md-left pr-5 pl-md-3 pr-md-0"
            data-toggle="collapse"
            href="#collapseComponents"
            data-parent="#exampleAccordion">
            <i class="fa fa-angle-right" aria-hidden="true"></i>
        </a>
    </div>
    <ul class="sidenav-second-level collapse {{ (Request::path() == 'students/add-new' || Request::path() == 'students') ? 'show' : '' }}"
    id="collapseComponents">
    @can('curd-student', Session::get('centerId'))
    <li class="{{ Request::path() == 'students/add-new' ? 'active' : '' }}">
        <a href="{{ route('student.create')}}">{{__("language.Student_Add")}}</a>
    </li>
    @endcan
    <li class="{{ Request::path() == 'students' ? 'active' : '' }}">
        <a href="{{ route('student.list')}}">{{__("language.Student_List")}}</a>
    </li>
</ul>
</li>
@endcan
@can('students', Session::get('centerId'))
<li class="nav-item mb-md-3" data-toggle="tooltip" data-placement="right" title="Components">
    <div class="row justify-content-between pl-3">
        <a class="nav-link col-7 col-md-9 pl-3" href="/home">
            <i class="fa fa-users" aria-hidden="true"></i>
            <span class="nav-link-text pl-2">
                {{__("language.Student_info")}}</span>
            </a>
            <a class="nav-link collapsed col-5 col-md-3 text-right text-md-left pr-5 pl-md-3 pr-md-0"
            data-parent="#exampleAccordion">
            <i class="fa fa-angle-right" aria-hidden="true"></i>
        </a>
    </div>
</li>
@endcan

@can('view-groups', Session::get('centerId'))
<!--Menu Group -->
<li class="nav-item mb-md-3" data-toggle="tooltip" data-placement="right" title="Menu Levels">
    <div class="row justify-content-between pl-3">
        <a class="nav-link col-7 col-md-9 pl-3" href="{{ route('group.list') }}">
            <i class="fa fa-fw fa-sitemap"></i>
            <span class="nav-link-text pl-2">
                {{__("language.Group")}}</span>
            </a>
            <a class="nav-link collapsed col-5 col-md-3 text-right text-md-left pr-5 pl-md-3 pr-md-0"
            data-toggle="collapse"
            href="#collapseGroup"
            data-parent="#exampleAccordion">
            <i class="fa fa-angle-right" aria-hidden="true"></i>
        </a>
    </div>
    <ul class="sidenav-second-level collapse {{ (Request::path() == 'groups/add-new' || Request::path() == 'groups') ? 'show' : '' }}"
    id="collapseGroup">
    @can('crud-group', Session::get('centerId'))
    <li class="{{ Request::path() == 'groups/add-new' ? 'active' : '' }}">
        <a href="{{ route('group.create')}}">{{__("language.Group_Add")}}</a>
    </li>
    @endcan
    @can('view-groups')
    <li class="{{ Request::path() == 'groups' ? 'active' : '' }}">
        <a href="{{ route('group.list') }}">{{__("language.Group_List")}}</a>
    </li>
    @endcan
</ul>
</li>
@endcan

<!--Menu Program-->
@can('manager-program')
<li class="nav-item mb-md-3" data-toggle="tooltip" data-placement="right" title="Menu Levels">
    <div class="row justify-content-between pl-3">
        <a class="nav-link col-7 col-md-9 pl-3" href="{{ route('program.list') }}">
            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
            <span class="nav-link-text pl-2">
                {{__("language.Study_Program")}}</span>
            </a>
            <a class="nav-link collapsed col-5 col-md-3 text-right text-md-left pr-5 pl-md-3 pr-md-0"
            data-toggle="collapse"
            href="#collapseProgram"
            data-parent="#exampleAccordion">
            <i class="fa fa-angle-right" aria-hidden="true"></i>
        </a>
    </div>
    <ul class="sidenav-second-level collapse {{ (Request::path() == 'programs/add-new' || Request::path() == 'programs') ? 'show' : '' }}"
    id="collapseProgram">
    <li class="{{ Request::path() == 'programs/add-new' ? 'active' : '' }}">
        <a href="{{route('program.add')}}">{{__("language.Study_Program_Add")}}</a>
    </li>
    <li class="{{ Request::path() == 'programs' ? 'active' : '' }}">
        <a href="{{route('program.list')}}">{{__("language.Study_Program_List")}}</a>
    </li>
</ul>
</li>
@endcan

<!--Menu Course-->
@can('crud-course',Session::get('centerId'))
<li class="nav-item mb-md-3" data-toggle="tooltip" data-placement="right" title="Menu Levels">
    <div class="row justify-content-between pl-3">
        <a class="nav-link col-7 col-md-9 pl-3" href="{{ route('course.show') }}">
            <i class="fa fa-book" aria-hidden="true"></i>
            <span class="nav-link-text pl-2">{{__("language.Course")}}</span>
        </a>
        <a class="nav-link collapsed col-5 col-md-3 text-right text-md-left pr-5 pl-md-3 pr-md-0"
        data-toggle="collapse"
        href="#collapseCourse"
        data-parent="#exampleAccordion">
        <i class="fa fa-angle-right" aria-hidden="true"></i></a>
    </div>
    <ul class="sidenav-second-level collapse {{ (Request::path() == 'courses/add-new' || Request::path() == 'courses') ? 'show' : '' }}" id="collapseCourse">
        <li class="{{ Request::path() == 'courses/add-new' ? 'active' : '' }}">
            <a href="{{route('course.create')}}">{{__("language.Course_Add")}}</a>
        </li>
        <li class="{{ Request::path() == 'courses' ? 'active' : '' }}">
            <a href="{{route('course.show')}}">{{__("language.Course_List")}}</a>
        </li>
    </ul>
</li>
@endcan
<!--Menu report-->
@can('crud-report')
<li class="nav-item mb-md-3" data-toggle="tooltip" data-placement="right" title="Menu Levels">

    <div class="row justify-content-between pl-3">
        <a class="nav-link collapsed col-7 col-md-9 pl-3"
        href="{{route('report.list')}}">
        <i class="fa fa-bar-chart" aria-hidden="true"></i>
        <span class="nav-link-text pl-2">
            {{__("language.Report")}}</span>
        </a>
        <a class="nav-link collapsed col-5 col-md-3 text-right text-md-left pr-5 pl-md-3 pr-md-0"
        data-toggle="collapse"
        href="#collapseReport"
        data-parent="#exampleAccordion">
        <i class="fa fa-angle-right" aria-hidden="true"></i></a>
    </div>
    <ul class="sidenav-second-level collapse {{ (Request::path() == 'reports/add-new' || Request::path() == 'reports') ? 'show' : '' }}" id="collapseReport">
        <li class="{{ Request::path() == 'reports/add-new' ? 'active' : '' }}">
            <a href="{{ route('report.create') }}">{{ __('language.create_report') }}</a>
        </li>
        <li class="{{ Request::path() == 'reports' ? 'active' : '' }}">
            <a href="{{ route('report.list') }}">{{__('language.list_report')}}</a>
        </li>
    </ul>
</li>
@endcan
<!-- Menu Oauth2 -->
@can('oauth2')
<li class="nav-item mb-md-3" data-toggle="tooltip" data-placement="right" title="Menu Levels">
    <div class="row justify-content-between pl-3">
    <a class="nav-link col-7 col-md-9 pl-3" href="{{ route('oauth2.show') }}">
            <i class="fa fa-book" aria-hidden="true"></i>
            <span class="nav-link-text pl-2">Oauth2</span>
        </a>
        <a class="nav-link collapsed col-5 col-md-3 text-right text-md-left pr-5 pl-md-3 pr-md-0"
        data-toggle="collapse"
        href="#collapseCourse"
        data-parent="#exampleAccordion">
        <i class="fa fa-angle-right" aria-hidden="true"></i>
    </a>
</div>
</li>
@endcan
<li class="nav-item menu-user-left" data-toggle="tooltip" data-placement="right" title="Menu Levels">
    <a class="nav-link nav-link-collapse collapsed pl-3" style="padding-right: 2em"
    data-toggle="collapse" href="#collapseUser"
    data-parent="#exampleAccordion">
    <i class="fa fa-user-o" aria-hidden="true"></i>
    <span class="nav-link-text pl-2">
        {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</span>
    </a>
    <ul class="sidenav-second-level collapse" id="collapseUser">
        <li>
            <a href="{{ route('me.profile')}}">
                {{__('language.profile')}}
            </a>
        </li>
        <li>
            <a href="{{route('me.update',Auth::user()->id)}}">
                {{__('language.change_info')}}
            </a>
        </li>
        <li>
            <a href="{{route('changePass.update')}}">
                {{__('language.changepasss')}}
            </a>
        </li>
        <li>
            <a href="{{route('changePass.update')}}"
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            {{__('language.logout')}}
        </a>
    </li>
    <li>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </li>
</ul>
</li>
</ul>

<!-- Button bottom dashboard -->
<ul class="navbar-nav sidenav-toggler">
    <li class="nav-item">
        <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
        </a>
    </li>
</ul>

<!-- Menu top -->
<ul class="navbar-nav">
    <!-- Menu Center -->
    <li class="nav-item menu-select">
        @can('view-center')
        @include('pageAdmin.center.selectCenter')
        @endcan
    </li>

    <!-- Menu language -->
    <li class="nav-item menu-select">
        <form action="{{route("postLang")}}" class="form-lang" method="post">
            {{ csrf_field() }}
            <select class="custom-select w-100" name="locale" onchange='this.form.submit();'>
                <option selected value="vi">VI</option>
                <option value="en" {{ Lang::locale() === 'en' ? 'selected' : '' }} >EN</option>
            </select>
        </form>
    </li>

    <!-- Menu User -->
    <li class="nav-item dropdown menu-user-top">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">
        {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="{{ route('me.profile')}}"><i
            class="fa fa-address-card" aria-hidden="true"></i>
            {{__('language.profile')}}</a>
            <a class="dropdown-item" href="{{route('me.update')}}"><i
                class="fa fa-pencil-square-o" aria-hidden="true"></i>
                {{__('language.change_info')}}</a>
                <a class="dropdown-item" href="{{route('changePass.update')}}"> <i class="fa fa-key"
                 aria-hidden="true"></i>
                 {{__('language.changepasss')}}</a>
                 <a class="dropdown-item" href="{{route('changePass.update')}}"
                 onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
                 class="fa fa-fw fa-sign-out"></i>
                 {{__('language.logout')}}</a>
                 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </li>
    </ul>
</div>

</nav>

<div class="content-wrapper bg-light">
    <div class="container-fluid">
        @yield('content')
    </div>
</div>

<footer class="sticky-footer">
    <div class="container">
        <div class="text-center">
            <small>Copyright &copy; 2017</small>
        </div>
    </div>
</footer>

<!-- Scroll to Top Button -->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>

<!-- Logout Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Select "Logout" below if you are ready to end your current session.
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> {{--
            <a class="btn btn-primary" href="{{ route('logout') }}">Logout</a>--}}
            <a class="btn btn-primary" href="{{ route('logout') }}"
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            {{__('language.logout')}}
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
</div>
</div>
</div>
@endif
<script src="{{ asset('js/jquery/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/boostrap/popper.min.js') }}"></script>
<script src="{{ asset('js/boostrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery-easing/jquery.easing.min.js') }}"></script>
<script src="{{ asset('js/jquery-te-1.4.0.min.js') }}"></script>
<script src="{{ asset('js/jquery.poshytip.min.js') }}"></script>
<script src="{{ asset('js/jquery-editable-poshytip.min.js') }}"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/index.js') }}"></script>
<script src="{{ asset('js/sb-admin.js') }}"></script>
@yield('javascript')
</body>
</html>