@extends('layouts.app')

@section('head.title')
{{__('language.Home')}}
@endsection
@section('content')
<div id="dashboard">
    <div class="container-fluid">
        <div class="row bg-white has-shadow mt-3">
            @if (isset($statistics))
            @can('view-statistics-all')
            @include('subViews.statistic-all', ['count'=>$statistics['countCenter'],'col'=>'col-6 col-md-4' , 'route'=>'center.list', 'name'=>__('language.Centers'), 'icon' => 'fa-university'])
            @include('subViews.statistic-all', ['count'=>$statistics['countGroup'],'col'=>'col-6 col-md-4' ,  'route'=>'group.list', 'name'=>__('language.Groups'),  'icon' => 'fa-users'])
            @include('subViews.statistic-all', ['count'=>$statistics['countStudent'],'col'=>'col-6 col-md-4' , 'route'=>'student.list', 'name'=>__('language.Students'),'icon' => 'fa-graduation-cap'])
            @include('subViews.statistic-all', ['count'=>$statistics['countCourse'],'col'=>'col-6 col-md-4' , 'route'=>'course.show', 'name'=>__('language.Courses'), 'icon' => 'fa-book'])
            @include('subViews.statistic-all', ['count'=>$statistics['countTeacher'],'col'=>'col-6 col-md-4' , 'route'=>'user.list', 'name'=>__('language.Teacher'), 'icon' => 'fa-user-circle'])
            @include('subViews.statistic-all', ['count'=>$statistics['countUser'],'col'=>'col-6 col-md-4' , 'route'=>'user.list', 'name'=>__('language.Users'),   'icon' => 'fa-user-o'])
            @endcan
            @can('view-statistics-center', Session::get('centerId'))
            @include('subViews.statistic-all', ['count'=>$statistics['countGroup'],'col'=>'col-6 col-md-3' ,  'route'=>'group.list', 'name'=>__('language.Groups'),  'icon' => 'fa-users'])
            @include('subViews.statistic-all', ['count'=>$statistics['countStudent'],'col'=>'col-6 col-md-3' , 'route'=>'student.list', 'name'=>__('language.Students'),'icon' => 'fa-graduation-cap'])
            @include('subViews.statistic-all', ['count'=>$statistics['countCourse'],'col'=>'col-6 col-md-3' , 'route'=>'course.show', 'name'=>__('language.Courses'), 'icon' => 'fa-book'])
            @include('subViews.statistic-all', ['count'=>$statistics['countTeacher'],'col'=>'col-6 col-md-3' , 'route'=>'user.list', 'name'=>__('language.Teacher'), 'icon' => 'fa-user-circle'])
            @include('subViews.statistic-all', ['count'=>$statistics['countUser'],'col'=>'col-6 col-md-6' , 'route'=>'user.list', 'name'=>__('language.Users'),   'icon' => 'fa-user-o'])
            @endcan
            @can('view-statistics-for-teacher', Session::get('centerId'))
            @include('subViews.statistic-all', ['count'=>$statistics['countGroup'],'col'=>'col-6 col-md-6' ,  'route'=>'group.list', 'name'=>__('language.Groups'),  'icon' => 'fa-users'])
            @include('subViews.statistic-all', ['count'=>$statistics['countCourse'],'col'=>'col-6 col-md-6' , 'route'=>'course.show', 'name'=>__('language.Courses'), 'icon' => 'fa-book'])
            @endcan
            @else
            <div class="col-12">
                <p class="text-center">{{ __('language.No_Data') }}</p>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
