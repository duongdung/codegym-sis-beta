<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Những chứng chỉ này không khớp với hồ sơ của chúng tôi.',
    'throttle' => 'Bạn đăng nhập quá số lần quy định. Vui lòng thử lại trong :seconds giây.',

];
