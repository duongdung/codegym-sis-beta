<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAttributesOfFieldsInStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('students')) {
            Schema::table('students', function (Blueprint $table) {
                if (Schema::hasColumn('students', 'birthday')) {
                    $table->string('birthday')->nullable()->change();
                }
                if (Schema::hasColumn('students', 'identify_card')) {
                    $table->string('identify_card')->nullable()->change();
                }
                if (Schema::hasColumn('students', 'startdate_id')) {
                    $table->string('startdate_id')->nullable()->change();
                }
                if (Schema::hasColumn('students', 'address_id')) {
                    $table->string('address_id')->nullable()->change();
                }
                if (Schema::hasColumn('students', 'start_school')) {
                    $table->string('start_school')->nullable()->change();
                }
                if (Schema::hasColumn('students', 'expected_end_date')) {
                    $table->string('expected_end_date')->nullable()->change();
                }
                if (Schema::hasColumn('students', 'address_id')) {
                    $table->string('address_id')->nullable()->change();
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            //
        });
    }
}
