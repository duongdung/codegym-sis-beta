<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategroriesCriteriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories_criteria', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('group_criteria_id')->unsigned();
            $table->foreign('group_criteria_id')->references('id')->on('groups_criteria')->onDelete('cascade');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories_criteria');
    }
}
