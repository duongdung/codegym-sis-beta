<?php

use App\User;
use Illuminate\Database\Seeder;
use App\Group;
class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teacher1    = User::where('id', 6)->first();
        $teacher2    = User::where('id', 7)->first();
        $teacher3    = User::where('id', 8)->first();
        $tutor1      = User::where('id', 9)->first();

        $group = new Group();
        $group->group_id                = "CG001";
        $group->group_description       ="Lớp CG001 ";
        $group->startdate_group         ="2017-09-05";
        $group->enddate_group           ="2017-11-05";
        $group->expected_end_date_group ="2017-11-10";
        $group->program_id              ="1";
        $group->teacher_id              ="5";
        $group->center_id               = 1;
        $group->save();
        $group->users()->attach($teacher1,['role'=>'1']);
        $group->users()->attach($teacher2,['role'=>'1']);

        $group = new Group();
        $group->group_id                = "CG002";
        $group->group_description       ="Lớp CG002 ";
        $group->startdate_group         ="2017-08-05";
        $group->enddate_group           ="2017-09-05";
        $group->expected_end_date_group ="2017-10-10";
        $group->program_id              ="2";
        $group->teacher_id              ="6";
        $group->center_id               = 1;
        $group->save();
        $group->users()->attach($teacher3,['role'=>'1']);
        $group->users()->attach($tutor1,['role'=>'2']);

        $group = new Group();
        $group->group_id                = "CGDN003";
        $group->group_description       ="Lớp CGDN003 ";
        $group->startdate_group         ="2017-09-05";
        $group->enddate_group           ="2017-11-05";
        $group->expected_end_date_group ="2017-11-10";
        $group->program_id              ="2";
        $group->teacher_id              ="7";
        $group->center_id         = 2;
        $group->save();
        $group->users()->attach($teacher2,['role'=>'1']);
        $group->users()->attach($tutor1,['role'=>'2']);

        $group = new Group();
        $group->group_id                = "CGDN004";
        $group->group_description       ="Lớp CGDN004 ";
        $group->startdate_group         ="2017-09-05";
        $group->enddate_group           ="2017-11-05";
        $group->expected_end_date_group ="2017-11-10";
        $group->program_id              ="2";
        $group->teacher_id              ="5";
        $group->center_id               = 2;
        $group->save();
        $group->users()->attach($teacher3,['role'=>'1']);
        $group->users()->attach($tutor1,['role'=>'2']);
    }
}
