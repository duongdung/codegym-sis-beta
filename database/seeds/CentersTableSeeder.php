<?php

use App\Center;
use Illuminate\Database\Seeder;

class CentersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $center = new Center();
        $center->id             = 1;
        $center->name           =   "CodeGym Hà Nội";
        $center->address        =   "Hà Nội";
        $center->center_code    =   "CGHN";
        $center->phone          =   "043888888";
        $center->email          =   "cogegym.hn@gmail.com";
        $center->save();

        $center = new Center();
        $center->id             = 2;
        $center->name           =   "CodeGym Đà Nẵng";
        $center->address        =   "Đà Nẵng";
        $center->center_code   =   "CGDN";
        $center->phone          =   "083888888";
        $center->email          =   "cogegym.dn@gmail.com";
        $center->save();
    }
}

