<?php

use App\Exam;
use App\Student;
use Illuminate\Database\Seeder;

class ExamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $student1    = Student::where('id', 1)->first();
        $student2    = Student::where('id', 2)->first();
        $student3    = Student::where('id', 3)->first();

        $student4    = Student::where('id', 4)->first();
        $student5    = Student::where('id', 5)->first();

        $exam = new Exam();
        $exam->course_id    =   1;
        $exam->date         =   "2017-10-10";
        $exam->save();
        $exam->students()->attach($student1,['score'=>'100']);
        $exam->students()->attach($student2,['score'=>'01']);
        $exam->students()->attach($student3,['score'=>'02']);
        $exam->students()->attach($student4,['score'=>'80']);
        $exam->students()->attach($student5,['score'=>'22']);

        $exam = new Exam();
        $exam->course_id    =   1;
        $exam->date         =   "2017-10-15";
        $exam->save();
        $exam->students()->attach($student2,['score'=>'98']);
        $exam->students()->attach($student3,['score'=>'89']);

        $exam = new Exam();
        $exam->course_id    =   1;
        $exam->date         =   "2017-10-15";
        $exam->save();
        $exam->students()->attach($student5,['score'=>'89']);

        $exam = new Exam();
        $exam->course_id    =   2;
        $exam->date         =   "2017-10-10";
        $exam->save();
        $exam->students()->attach($student1,['score'=>'99']);
        $exam->students()->attach($student2,['score'=>'88']);
        $exam->students()->attach($student3,['score'=>'77']);
        $exam->students()->attach($student4,['score'=>'89']);
        $exam->students()->attach($student5,['score'=>'67']);

        $exam = new Exam();
        $exam->course_id    =   3;
        $exam->date         =   "2017-10-10";
        $exam->save();
        $exam->students()->attach($student1,['score'=>'66']);
        $exam->students()->attach($student2,['score'=>'67']);
        $exam->students()->attach($student3,['score'=>'68']);
        $exam->students()->attach($student4,['score'=>'80']);
        $exam->students()->attach($student5,['score'=>'89']);

    }
}
