<?php

use App\Http\Controllers\RoleConstants;
use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->id      =  RoleConstants::ROLE_ADMIN;
        $role->name      =  "Admin";
        $role->role_code =  "admin";
        $role->description  =  "Admin";
        $role->save();

        $role = new Role();
        $role->id      =  RoleConstants::ROLE_HQMANAGER;
        $role->name      =  "Headquarter Manager";
        $role->role_code =  "hq-manager";
        $role->description  =  "Quản lý toàn bộ hệ thống";
        $role->save();

        $role = new Role();
        $role->id      =  RoleConstants::ROLE_HQAAO;
        $role->name      =  "Headquarter AAO";
        $role->role_code =  "hq-aao";
        $role->description  =  "Giáo vụ toàn bộ hệ thống";
        $role->save();

        $role = new Role();
        $role->id      =  RoleConstants::ROLE_CENTERMANAGER;
        $role->name      =  "Center Manager";
        $role->role_code =  "center-manager";
        $role->description  =  "Quản lý trung tâm";
        $role->save();

        $role = new Role();
        $role->id      =  RoleConstants::ROLE_AAO;
        $role->name      =  "AAO";
        $role->role_code =  "aao";
        $role->description  =  "Giáo vụ của trung tâm";
        $role->save();

        $role = new Role();
        $role->id      =  RoleConstants::ROLE_COACH;
        $role->name      =  "Coach";
        $role->role_code =  "coach";
        $role->description  =  "Huấn luyện viên của trung tâm";
        $role->save();

        $role = new Role();
        $role->id      =  RoleConstants::ROLE_TUTOR;
        $role->name      =  "Tutor";
        $role->role_code =  "tutor";
        $role->description  =  "Trợ giảng của trung tâm";
        $role->save();

        $role = new Role();
        $role->id      =  RoleConstants::ROLE_STUDENT;
        $role->name      =  "Student";
        $role->role_code =  "student";
        $role->description  =  "Học viên của trung tâm";
        $role->save();
    }
}
