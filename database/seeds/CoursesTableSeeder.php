<?php

use App\Course;
use App\Student;
use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $student1    = Student::where('id', 1)->first();
        $student2    = Student::where('id', 2)->first();
        $student3    = Student::where('id', 3)->first();
        $student4    = Student::where('id', 4)->first();
        $student5    = Student::where('id', 5)->first();

        $course = new Course();
        $course->id             =   1;
        $course->course_code    =   "WF001";
        $course->course_name    =   "Web Fundamentals";
        $course->description    =   "";
        $course->save();
        $course->students()->attach($student1,['test_score'=>'10','group'=>'1']);
        $course->students()->attach($student2,['test_score'=>'9','group'=>'1']);
        $course->students()->attach($student3,['test_score'=>'8','group'=>'1']);

        $course->students()->attach($student4,['test_score'=>'8','group'=>'3']);
        $course->students()->attach($student5,['test_score'=>'8','group'=>'3']);



        $course = new Course();
        $course->id             =   2;
        $course->course_code    =   "KB01";
        $course->course_name    =   "Personal Kanban";
        $course->description    =   "";
        $course->save();
        $course->students()->attach($student1,['test_score'=>'6','group'=>'1']);
        $course->students()->attach($student2,['test_score'=>'6','group'=>'1']);
        $course->students()->attach($student3,['test_score'=>'7','group'=>'1']);

        $course->students()->attach($student4,['test_score'=>'3','group'=>'3']);
        $course->students()->attach($student5,['test_score'=>'3','group'=>'3']);

        $course = new Course();
        $course->id             =   3;
        $course->course_code    =   "Lamp";
        $course->course_name    =   "LAMP";
        $course->description    =   "";
        $course->save();
    }
}
