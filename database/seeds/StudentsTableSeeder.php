<?php


use Illuminate\Database\Seeder;
use App\Student;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $student = new Student();
        $student->student_id        = 'CGHN001';
        $student->first_name        = 'Phan';
        $student->last_name         = 'Luân';
        $student->birthday          = '1995-11-01';
        $student->identify_card     = '091511111';
        $student->startdate_id      = '2015-08-18';
        $student->address_id        = 'Hà Nội';
        $student->household         = 'Nguyễn Cơ Thạch, Hà Nội';
        $student->phone             = '0988888888';
        $student->email             = 'phanluan@gmail.com';
        $student->company           = 'Agilead Global';
        $student->link_github       = 'https://github.com/phanluan';
        $student->link_blog         = '';
        $student->note              = '';
        $student->image             = 'images/avatars/phanluan.jpg';
        $student->start_school      = '2017-09-01';
        $student->expected_end_date = '2017-11-15';
        $student->actual_end_date   = '2017-11-15';
        $student->group_id          = '1';
        $student->password = bcrypt("student");
        $student->center_id         = 1;
        $student->save();

        $student = new Student();
        $student->student_id        = 'CGHN002';
        $student->first_name        = 'Duy';
        $student->last_name         = 'Đức';
        $student->birthday          = '1995-06-11';
        $student->identify_card     = '091511112';
        $student->startdate_id      = '2015-08-17';
        $student->address_id        = 'Hà Nội';
        $student->household         = 'Lê Đức Thọ, Hà Nội';
        $student->phone             = '0988888889';
        $student->email             = 'duyduc@gmail.com';
        $student->company           = 'Agilead Global';
        $student->link_github       = 'https://github.com/duyduc';
        $student->link_blog         = '';
        $student->note              = '';
        $student->image             = 'images/avatars/duyduc.jpg';
        $student->start_school      = '2017-09-01';
        $student->expected_end_date = '2017-11-15';
        $student->actual_end_date   = '2017-11-15';
        $student->group_id          = '1';
        $student->center_id         = 1;
        $student->password          = bcrypt("student");
        $student->save();

        $student = new Student();
        $student->student_id        = 'CGHN003';
        $student->first_name        = 'Bình';
        $student->last_name         = 'Sơn';
        $student->birthday          = '1988-06-11';
        $student->identify_card     = '091511113';
        $student->startdate_id      = '2015-08-17';
        $student->address_id        = 'Hà Nội';
        $student->household         = 'Lê Đức Thọ, Hà Nội';
        $student->phone             = '0988888887';
        $student->email             = 'binhson@gmail.com';
        $student->company           = 'Agilead Global';
        $student->link_github       = 'https://github.com/binhson';
        $student->link_blog         = '';
        $student->note              = '';
        $student->image             = 'images/avatars/binhson.jpg';
        $student->start_school      = '2017-09-01';
        $student->expected_end_date = '2017-11-15';
        $student->actual_end_date   = '2017-11-15';
        $student->group_id          = '1';
        $student->center_id         = 1;
        $student->password          = bcrypt("student");
        $student->save();

        $student = new Student();
        $student->student_id        = 'CGDN001';
        $student->first_name        = 'Dương';
        $student->last_name         = 'Dũng';
        $student->birthday          = '1990-07-11';
        $student->identify_card     = '091511126';
        $student->startdate_id      = '2015-08-17';
        $student->address_id        = 'Hà Nội';
        $student->household         = 'Đào Tấn, Hà Nội';
        $student->phone             = '0988888982';
        $student->email             = 'duongdung@gmail.com';
        $student->company           = 'Agilead Global';
        $student->link_github       = 'https://github.com/duongdung';
        $student->link_blog         = '';
        $student->note              = '';
        $student->image             = 'images/avatars/duongdung.jpg';
        $student->start_school      = '2017-09-01';
        $student->expected_end_date = '2017-11-11';
        $student->actual_end_date   = '2017-11-11';
        $student->group_id          = '3';
        $student->password          = bcrypt("student");
        $student->center_id         = 2;
        $student->save();

        $student = new Student();
        $student->student_id        = 'CGHN005';
        $student->first_name        = 'Thị';
        $student->last_name         = 'Trang';
        $student->birthday          = '1995-07-11';
        $student->identify_card     = '091511119';
        $student->startdate_id      = '2015-08-17';
        $student->address_id        = 'Hà Nội';
        $student->household         = 'Đào Tấn, Hà Nội';
        $student->phone             = '0988888882';
        $student->email             = 'letrang@gmail.com';
        $student->company           = 'Agilead Global';
        $student->link_github       = 'https://github.com/letrang';
        $student->link_blog         = '';
        $student->note              = '';
        $student->image             = 'images/avatars/letrang.jpg';
        $student->start_school      = '2017-09-01';
        $student->expected_end_date = '2017-11-11';
        $student->actual_end_date   = '2017-11-11';
        $student->group_id          = '3';
        $student->password          = bcrypt("student");
        $student->center_id         = 2;
        $student->save();
    }
}
