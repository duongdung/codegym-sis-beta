<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CentersTableSeeder::class);
        $this->call(ProgramsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(GroupsTableSeeder::class);
        $this->call(StudentsTableSeeder::class);
        $this->call(CoursesTableSeeder::class);
        $this->call(ReportsTableSeeder::class);
        $this->call(GroupsCriteriaTableSeeder::class);
        $this->call(CategoriesCriteriesTableSeeder::class);
        $this->call(CriteriasTableSeeder::class);
        $this->call(ExamsTableSeeder::class);

    }
}
