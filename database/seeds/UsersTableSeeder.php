<?php
use App\Group;
use App\User;
use App\Role;
use Illuminate\Database\Seeder;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin      = Role::where('role_code', 'admin')->first();
        $role_hqmanager  = Role::where('role_code', 'hq-manager')->first();
        $role_hqaao      = Role::where('role_code', 'hq-aao')->first();
        $role_aao        = Role::where('role_code', 'aao')->first();
        $role_coach      = Role::where('role_code', 'coach')->first();
        $role_tutor      = Role::where('role_code', 'tutor')->first();
        $role_student    = Role::where('role_code', 'tutor')->first();

        $user = new User();
        $user->email = "super.admin@codegym.vn";
        $user->first_name = "Nguyễn";
        $user->last_name = "Ninh";
        $user->password = bcrypt("superadmin");
        $user->save();
        $user->roles()->attach($role_admin);
        $user->roles()->attach($role_hqmanager);
        $user->roles()->attach($role_hqaao );
        $user->roles()->attach($role_aao);
        $user->roles()->attach($role_coach);
        $user->roles()->attach($role_tutor);

        $user = new User();
        $user->email = "hq.aao@codegym.vn";
        $user->first_name = "Phan";
        $user->last_name = "Luân";
        $user->password = bcrypt("hqaao");
        $user->save();
        $user->roles()->attach($role_hqaao );

        $user = new User();
        $user->email = "khacnhat@codegym.vn";
        $user->first_name = "Nguyễn Khắc";
        $user->last_name = "Nhật";
        $user->password = bcrypt("admin");
        $user->save();
        $user->roles()->attach($role_admin);

        $user = new User();
        $user->email = "hq.manager@codegym.vn";
        $user->first_name = "Phan";
        $user->last_name = "Hai";
        $user->password = bcrypt("hqmanager");
        $user->save();
        $user->roles()->attach($role_hqmanager);

        $user = new User();
        $user->email = "aao@codegym.vn";
        $user->first_name = "Chương";
        $user->last_name = "Hải";
        $user->password = bcrypt("aao");
        $user->center_id = 1;
        $user->save();
        $user->roles()->attach($role_aao);

        $user = new User();
        $user->email = "teacher1@codegym.vn";
        $user->first_name = "Phạm";
        $user->last_name = "Sơn";
        $user->password = bcrypt("teacher");
        $user->center_id = 1;
        $user->save();
        $user->roles()->attach($role_coach);

        $user = new User();
        $user->email = "teacher2@codegym.vn";
        $user->first_name = "Nguyễn";
        $user->last_name = "Dương";
        $user->password = bcrypt("teacher");
        $user->center_id = 1;
        $user->save();
        $user->roles()->attach($role_coach);

        $user = new User();
        $user->email = "teacher3@codegym.vn";
        $user->first_name = "Phan";
        $user->last_name = "Thành";
        $user->password = bcrypt("teacher");
        $user->center_id = 1;
        $user->save();
        $user->roles()->attach($role_coach);

        $user = new User();
        $user->email = "tutor@codegym.vn";
        $user->first_name = "Phan";
        $user->last_name = "Quân";
        $user->password = bcrypt("tutor");
        $user->center_id = 1;
        $user->save();
        $user->roles()->attach($role_tutor);

        $user = new User();
        $user->email = 'phanluan@gmail.com';
        $user->first_name = "Phan";
        $user->last_name = "Luan";
        $user->password = bcrypt("phanluan");
        $user->center_id = 2;
        $user->save();
        $user->roles()->attach($role_student);

        $user = new User();
        $user->email = 'duyduc@gmail.com';
        $user->first_name = "Duy";
        $user->last_name = 'Đức';
        $user->password = bcrypt("duyduc");
        $user->center_id = 2;
        $user->save();
        $user->roles()->attach($role_student);

        $user = new User();
        $user->email = 'binhson@gmail.com';
        $user->first_name = 'Bình';
        $user->last_name = 'Sơn';
        $user->password = bcrypt("binhson");
        $user->center_id = 1;
        $user->save();
        $user->roles()->attach($role_student);

        $user = new User();
        $user->email = 'duongdung@gmail.com';
        $user->first_name = 'Dương';
        $user->last_name = 'Dũng';
        $user->password = bcrypt("duongdung");
        $user->center_id = 1;
        $user->save();
        $user->roles()->attach($role_student);

        $user = new User();
        $user->email = 'letrang@gmail.com';
        $user->first_name = 'Thị';
        $user->last_name = 'Trang';
        $user->password = bcrypt("letrang");
        $user->center_id = 1;
        $user->save();
        $user->roles()->attach($role_student);

    }
}

