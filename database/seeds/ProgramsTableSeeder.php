<?php

use App\Program;
use Illuminate\Database\Seeder;

class ProgramsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $programs = new Program();
        $programs->name_program        = '6M';
        $programs->program_description = 'Chương trình đào tạo lập trình viên full-stask';
        $programs->length_of_program         = '6 tháng';
        $programs->save();

        $programs = new Program();
        $programs->name_program        = '2.5M';
        $programs->program_description = 'Chương trình đào tạo nhanh để cập nhật công nghệ mới';
        $programs->length_of_program         = '2 tháng';
        $programs->save();
    }
}
