<?php

use Illuminate\Database\Seeder;

use App\Criteria;

class CriteriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $criteria = new Criteria();
        $criteria->name = 'Tự kiểm soát và kỷ luật với tư cách là người đi làm tập sự';
        $criteria->category_criteria_id = 1;
        $criteria->description = 'Đạt: Thường xuyên nghỉ học và không xin phép.Cơ bản là không chuẩn bị bài trước các buổi đào tạo.Thường xuyên không hoàn thành công việc và hay trễ deadline';
        $criteria->save();


        $criteria = new Criteria();
        $criteria->name = 'Lập kế hoạch ';
        $criteria->category_criteria_id = 2;
        $criteria->description = 'Không đạt: Thỉnh thoảng hoặc không lập kế hoạch hàng ngày.Không biết việc gì cần làm trong ngày';
        $criteria->save();

    }
}
