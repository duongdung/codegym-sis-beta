<?php

use Illuminate\Database\Seeder;
use App\CategoryCriteria;

class CategoriesCriteriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryCriteria = new CategoryCriteria();
        $categoryCriteria->name = 'How to learn';
        $categoryCriteria->group_criteria_id = 1;
        $categoryCriteria->save();

        $categoryCriteria = new CategoryCriteria();
        $categoryCriteria->name = 'How to get things done';
        $categoryCriteria->group_criteria_id = 1;
        $categoryCriteria->save();
    }
}
