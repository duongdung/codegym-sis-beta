<?php

use Illuminate\Database\Seeder;
use App\GroupCriteria;
class GroupsCriteriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $criteria = new GroupCriteria();
        $criteria->name = 'KỸ NĂNG THẾ KỶ 21';
        $criteria->report_id = '1';
        $criteria->description = 'Tổng hợp các kỹ năng về học tập như ký năng ghi chép , kỹ năng tự kiểm soát cá nhân';
        $criteria->save();


        $criteria = new GroupCriteria();
        $criteria->name = 'KỸ NĂNG NGHỀ NGHIỆP';
        $criteria->report_id = '1';
        $criteria->description = 'Tổng hợp các kỹ năng về lập trình .';
        $criteria->save();
    }
}
