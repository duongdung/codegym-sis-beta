<?php

use Illuminate\Database\Seeder;
use App\Report;

class ReportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $report = new Report();
        $report->name = 'HƯỚNG DẪN ĐÁNH GIÁ CHO PERFORMANCE REPORT';
        $report->description = 'Hướng dẫn đánh giá học viên';
        $report->save();

    }
}
