var baseUrl = window.location.protocol + '//' + window.location.host + '/';
$(document).ready(function () {
    $(".editor").jqte({
        color: true,
        fsizes: ['10', '12', '16', '18', '20', '24', '28'],
        sub: false,
        sup: false,
        format: false
    });

    $('.js-example-basic-single').select2();

    $('#avatar').change(function (event) {
        var output = $('#img-preview');
        output.attr('src', URL.createObjectURL(event.target.files[0]));
    });

    var searchStudent = $('#search-student'),
        eventStudent = $('#input-student'),
        valueStudent = $(searchStudent).val(),

        groupId = $('#search-teacher').val(),
        eventTeacher = $('#input-teacher');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy'
    });

    // ---------------------Sinh vien------------------
    // Tìm kiếm student
    $(eventStudent).keyup(function () {

        var keyword = $('#input-student').val();

        // Kiểm tra từ khóa nếu rỗng thì hiển thị thông báo
        if (!keyword) {

            html = '<div class="alert alert-danger" role="alert">';
            html += 'Hãy nhập từ khóa để tìm kiếm học viên.';
            html += '</div>';

            // Gán kết quả vào div#list-student
            $('#list-student').html(html);

        } else {

            // Hiện danh sách sinh viên tìm kiếm được
            ShowStudent(keyword);

        }

    });

    // Show student
    function ShowStudent(keyword) {

        count = 0;
        // ajax tìm kiếm sinh viên
        $.get(baseUrl + 'groups/search-student/' + keyword, function (data) {

            var html = '<table class="table table-search ">';
            html += '<tbody>';

            $.each(data['data'], function (index, item) {

                // Nếu sinh viên chưa có trong danh sách lớp
                if (item.group_id != valueStudent) {

                    html = StudentNotInGroup(html, item);
                    count++;

                    // Nếu sinh viên đã có trong danh sách lớp
                } else {

                    html = StudentInGroup(html, item);
                    count++;

                }
            });

            // Đếm số lượng học viên tìm thấy
            if (count > 0) {

                html += '<div id="add-success">';
                html += '<p class="text-success">Tìm thấy ' + count + ' học viên</p>';
                html += '</div>';

            } else {

                html += '<p class="text-danger">Không tìm thấy học viên nào</p>';

            }
            html += '</tbody>';
            html += '</table>';

            // Gán kết quả vào div#list-student
            $('#list-student').html(html);

            // ajax thêm sinh viên vào lớp
            AddStudent();

        })
    }

    // Hoc vien co trong lop
    function StudentNotInGroup(html, item) {
        html += '<input type="hidden" id="studentId" value="' + item.id + '" name="studentId">';

        html += '<tr class="row m-0">';

        html += '<td class="col-2">';
        html += '<img style="width: 50%;" src="http://localhost:8000/storage/' + item.image + '" alt="" class="img-responsive rounded mx-auto d-block">';
        html += '</td>';

        html += '<td class="col-4">';
        html += '<p class="h5 m-0 p-0">' + item.first_name + ' ' + item.last_name + '</p>';
        html += '<p class="text-secondary m-0 p-0">' + item.email + '</p>';
        html += '</td>';

        html += '<td class="col-3">';
        html += '<p class="text-secondary mt-2 mb-0 p-0">Đang thuộc lớp ' + item.group.group_id + '</p>';
        html += '</td>';

        html += '<td class="col-3 ml-auto">';
        html += '<div id="button-add-student" class="mt-1 float-right">';
        html += '<button type="button" value="' + item.id + '" class=" btn btn-outline-secondary add-student-group">';
        html += '<i class="fa fa-pencil" aria-hidden="true"></i> Ghi Danh';
        html += '</button>';
        html += '</div>';
        html += '</td>';

        html += '</tr>';

        return html;
    }

    // Hoc vien khong co trong lop
    function StudentInGroup(html, item) {
        html += '<tr class="row m-0">';

        html += '<td class="col-2">';
        html += '<img style="width: 50%;" src="http://localhost:8000/storage/' + item.image + '" alt="" class="img-responsive rounded mx-auto d-block">';
        html += '</td>';

        html += '<td class="col-5">';
        html += '<p class="h5 m-0 p-0">' + item.first_name + ' ' + item.last_name + '</p>';
        html += '<p class="text-secondary">' + item.email + '</p>';
        html += '</td>';

        html += '<td class="col-2">';
        html += '<p class="text-secondary mt-2 mb-0 p-0">' + item.group.group_description + '</p>';
        html += '</td>';

        html += '<td class="col-3 ml-auto">';
        html += '<div id="button-add-student" class="mt-1 float-right">';
        html += '<button type="button" value="' + item.id + '" class=" btn btn-outline-secondary border-none add-student-group" disabled>Đã ghi Danh </button>';
        html += '</div>';
        html += '</td>';

        html += '</tr>';

        return html;
    }

    // Them sinh vien vao lop
    function AddStudent() {

        // Nhấn nút GHI DANH
        $('.add-student-group').click(function () {

            $idStudent = $(this).val();

            $.ajax({
                type: "GET",
                url: baseUrl + 'groups/add-student/' + valueStudent + '/' + $idStudent,
                success: function () {
                    var addSuccess = ' <p class="text-success">Ghi danh sinh viên thành công</p>';
                    $('#add-success').html(addSuccess);
                }
            });

            // Ẩn nút ghi danh
            $(this).html('Đã ghi danh');
            $(this).attr('disabled', 'true');
            $(this).addClass('border-none');
        });
    }

    // ----------------------Giao vien--------------------
    // Tìm kiếm student
    $(eventTeacher).keyup(function () {

        var keyword = $('#input-teacher').val();

        // Kiểm tra từ khóa nếu rỗng thì hiển thị thông báo
        if (!keyword) {

            html = '<p class="text-danger" role="">';
            html += '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Hãy nhập từ khóa để tìm kiếm giáo viên.';
            html += '</p>';

            // Gán kết quả vào div#list-student
            $('#list-teacher').html(html);

        } else {
            // Hiện danh sách sinh viên tìm kiếm được
            ShowTeacher(keyword);
        }

    });

    // Show student
    function ShowTeacher(keyword) {

        count = 0;
        // ajax tìm kiếm giao vien
        $.get(baseUrl + 'users/search-teacher/' + keyword, function (data) {

            var html = '<table class="table table-search ">';
            html += '<tbody>';

            $.each(data.teacher, function (index, item) {

                var teacherId = $('.teacher-id');
                var value = '';

                html += '<input type="hidden" id="studentId" value="' + item.id + '" name="studentId">';
                html += '<tr class="row m-0">';
                html += '<td class="col-5">';
                html += '<p class="h5 m-0 p-0">' + item.first_name + ' ' + item.last_name + '</p>';
                html += '<p class="text-secondary m-0 p-0">' + item.email + '</p>';
                html += '</td>';

                html += '<td class="col-4">';
                if (item.roles.length === 1) {
                    $.each(item.roles, function (index1, role) {
                        if (role.id === 6) {
                            html += '<p class="pl-3">Giáo viên</p>';
                            html += '<input type="hidden" class="role-select' + index + ' role" value="6"/>';
                        } else if (role.id === 7) {
                            html += '<p class="pl-3">Trợ giảng</p>';
                            html += '<input type="hidden" class="role-select' + index + ' role" value="7"/>';
                        }
                    });
                } else {
                    html += '<select class="custom-select role-select role-option' + index + '">'
                    $.each(item.roles, function (index1, role) {
                        html += '<option value=' + role.id + '>'
                        if (role.id === 6) {
                            html += '<p>Giáo viên</p>';
                        } else if (role.id === 7) {
                            html += '<p>Trợ giảng</p>';
                        }
                        html += '</option>'
                    });
                    html += '</select>'
                }
                html += '</td>';
                html += '<td class="col-3 ml-auto">';
                html += '<div id="button-add-teacher" class="mt-1 float-right">';

                flag = true;
                for (var i = 0; i < teacherId.length; i++) {
                    value = $(teacherId[i]).html();

                    // Nếu giáo viên đã có trong danh sách lớp
                    if (item.id == value) {
                        html += '<button type="button" value="' + item.id + '" class=" btn btn-outline-secondary border-none add-student-group" disabled>Đã trong lớp';
                        flag = false;
                    }
                }
                // Nếu giáo viên chưa có trong danh sách lớp
                if (flag) {
                    html += '<button type="button" data-content="' + item.id + '" value="' + index + '" class=" btn btn-outline-secondary add-teacher-group">';
                    html += '<i class="fa fa-pencil" aria-hidden="true"></i> Ghi Danh';
                }
                html += '</button>';
                html += '</div>';
                html += '</td>';
                html += '</tr>';

            });

            count = data.teacher.length;

            // Đếm số lượng giao vien tìm thấy
            if (count > 0) {

                html += '<div id="add-success">';
                html += '<p class="text-success">Tìm thấy ' + count + ' giáo viên</p>';
                html += '</div>';

            } else {

                html += '<p class="text-danger">Không tìm thấy giáo viên nào</p>';

            }
            html += '</tbody>';
            html += '</table>';

            // Gán kết quả vào div#list-student
            $('#list-teacher').html(html);

            // ajax thêm sinh viên vào lớp
            AddTeacher();
        })
    }

    // Them sinh vien vao lop
    function AddTeacher() {

        // Nhấn nút GHI DANH
        $('.add-teacher-group').click(function () {
            var index = $(this).val();
            var roleSelect = $('.role-select' + index).hasClass('role');
            var idTeacher = $(this).data('content');
            if (!roleSelect) {
                var role = $('.role-option' + index + ' option:selected').val();
            } else {
                var role = $('.role').val();
            }
            $.ajax({
                type: "GET",
                url: baseUrl + 'groups/add-teacher/' + groupId + '/' + idTeacher + '/' + role,
                success: function () {
                    var addSuccess = ' <p class="text-success">Ghi danh giáo viên thành công</p>';
                    $('#add-success').html(addSuccess);

                },
                error: function (data) {
                    console.log('error !');
                }
            });
            // Ẩn nút ghi danh
            $(this).html('Đã ghi danh');
            $(this).attr('disabled', 'true');
            $(this).addClass('border-none');
        });
    }

    // ----------------------Search ajax course--------------------

    $('.editable-input').editable();

    function activeTabs() {
        var url = document.location.toString();
        if (url.match('#')) {
            $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
        }

        $('.nav-tabs a').on('shown.bs.tab', function (e) {
            window.location.hash = e.target.hash;
        })
    }

    activeTabs();

})
;

function deleteGroupLog(group_id, log_id) {
    if (confirm('Bạn có chắc chắn không?')) {
        $.ajax({
            type: "POST",
            url: baseUrl + 'groups/' + group_id + '/logs/' + log_id + '/delete',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function () {
                $('#group-log-' + log_id).remove();
            },
            error: function (data) {
                alert('Không xóa được nhật ký của lớp');
            }
        });
    }
}
function deleteStudentLog(student_id, log_id) {
    if (confirm('Bạn có chắc chắn không?')) {
        $.ajax({
            type: "POST",
            url: baseUrl + 'students/' + student_id + '/logs/' + log_id + '/delete',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function () {
                $('#student-log-' + log_id).remove();
            },
            error: function (data) {
                alert('Không xóa được nhật ký của học viên');
            }
        });
    }
}

